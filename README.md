# __OVAS__ #

Open-source Variant Analysis Suite, built for the secure and transparent analysis of sequence variants.

[**Click here for ISO**](https://drive.google.com/open?id=0Bychzay3EgcwUV9IR25JLTdpM0E)

  - __NO CLOUD__ - All data is processed in-house and never leaves the client machine. Subsequent processing is under the sole ownership of the researcher.
  - __OFFLINE__ - An internet connection is not required for all general processing, or for the pulling of any static data. Genetic maps may require a one-time connection to generate, but only for extremely irregular custom user-settings.
  - __OPEN SOURCE__ - Transparent, modular, modifiable. GPLv3.
  - __DISEASE CONTEXT__ - A Mendelian inheritance context is adopted to shape an analysis of related/un-related individuals, for autosomal/X-linked dominant/recessive disease models. Non-Mendelian models such as Mosaicism are also supported.
  - __SELF-CONTAINED ENVIRONMENT__ - OVAS runs by default off a bootable medium (DVD, USB) without the need for prior setup.
  - __INTERACTIVE__ - OVAS is accessed through a web-browser interface which provides configurable run paramenters, real-time progress updates, and interactive variant selection for completed analyses.


![fig2.png](https://bitbucket.org/repo/oX8Kq8/images/771327415-fig2.png)
![fig3.png](https://bitbucket.org/repo/oX8Kq8/images/2610892568-fig3.png)

![fig1.png](https://bitbucket.org/repo/oX8Kq8/images/2823741586-fig1.png)
  
### Installation and Setup
  - Download and extract the Live ISO image (at the top of the page)

  - Burn to a DVD or USB stick
     - In Linux, this is trivially
     `sudo sh -c "dd if=ovas-*.iso of=/dev/sdm"`
 where `sdm` is the whole block device for the writeable medium. 
(Use with caution).

  - Once complete, reboot your machine with the bootable medium inserted.
  - Make sure you select your bootable device as your first bootable medium from the boot menu in your BIOS settings if nothing appears.
  - Wait 30 seconds for OVAS to boot and for the web-interface to appear.

### Custom Installation to an existing Linux platform  
##### (skip this stage if using bootable medium method above)

   - Clone the source code from this repository
   - In your existing system, create the following directory:
   `/extra/static_data/FASTA/`
   - Download FASTA for your desired Human Genome version(s) from:
   `http://hgdownload.cse.ucsc.edu/downloads.html#human`
    and place it in this directory, subfoldered to `hg19` or `hg38` or which ever build you choose.
   - Install a http server and point it to the cloned directory. If using a `httpd`-based server, you can create a virtual host and create a file at `/etc/httpd/conf/vhosts/ovas.dom`, containing:

```xml
  <VirtualHost localhost:1234>
  ServerAdmin webmaster@domainname1.dom
  DocumentRoot "/path/to/ovas-pipeline"
  ServerName ovas.dom
  ServerAlias ovas.dom
  ErrorLog "/var/log/httpd/ovas.dom-error_log"
  CustomLog "/var/log/httpd/ovas.dom-access_log" common
  
  <Directory "/path/to/ovas-pipeline/" >
      Require all granted
  </Directory>
</VirtualHost>
```

replacing `/path/to/ovas-pipeline` and `1234` for your deployment and preferred local port. Then modify the config file at `/etc/httpd/conf/httpd.conf` and add the line:

 `Listen 1234`  (or your desired port)

 then towards the end of the file, add:

```
Include conf/extra/php7_module.conf
Include conf/vhosts/ovas.dom
```

* Finally:
    * Install all dependencies from `/path/to/ovas-pipeline/private/working_minimal.txt`
    * Set permissions:
        - `sudo chmod 644 /path/to/ovas-pipeline -R`
        - `sudo chmod 755 /path/to/ovas-pipeline/server/runs -R` 
    * Restart your web-server.


## Usage

#### Initial Startup

 1. Insert the medium for your VCF files, either by another USB stick or CD/DVD drive.
 2. Upload your pedigree file of related and unrelated individuals into the web-interface. Wait for the pedigrees to be rendered. If an error occurs, check your pedigree and try again.
 3. Once rendered, upload the VCF files for each of the new file upload slots that have appeared in each of the case and control individuals.

#### Pre-Run configuration
 1. __Core Annotation__:
     * Configure your Genemap settings (genome version, inclusion of splice/UTR/intron sites).
     * Decide whether you want to use isoforms.
     * Set the heterozygousity threshold (0.65 by default)
 2. __Optional Modules__:
     * Enable the modules that you want to use by ticking the boxes in the corner. The modules expand to display sub-options which can also be enabled/overriden.
3. __Inheritance Model__:
    * Select the inheritance model that best describes the disease. Different models have different options which may also require adjustment.
4. __Commonality__:
    * Depending on your pedigree, enable to produce only variants shared at the same locus, or those that fall within the same gene. Or ignore, if pedigree is complex.
5. __Additional Annotation__:
    * Enable the other annotations that you would like to see (unique gene identifiers for isoforms, UniProt context, Gene expression filtering (with seperate thresholds for each GEXPR experiment column)).
 
#### Runtime 

 - Once options are set, press the 'Submit' button and wait for the page to update the with a running progress box of what is being processed. 
 - Core annotation typically takes a while ~5-15 mins, but all other stages should be reasonably quick.
 - To cancel an ongoing analysis, simply click the 'Cancel' button which has now replaced the 'Submit' one.

#### Re-Running the same analysis
 - If user configuration wasn't desirably set the first time around, then premature termination of the pipeline might be required.
 - Upon cancelling a job, all previously uploaded data still remains in the working directory, including core annotations (if the pipeline got that far).
 - Thus, re-running the same analysis will simply resume from where the analysis left off.
 - If the run configurations are tweaked in the second re-run, then the pipeline will resume from the first point of run modification. For modifications after the core annotation step, this is very quick.


#### Post-analysis

 - After the pipeline has finished, a 'Summary' tab will appear at the bottom of the page which when ticked will open up a new tab containing the filtered variants for all individuals in the analysis.
 - The data is presented in a table, with table headers pertaining to individual IDs which can be clicked as links to the final output VCFs produced by the analysis.
 - The table can be modified to be more/less verbose, to filter for specific mutation types (synonymous, missense, non-sense) and for variant proximity (UTR/Splice/Intron/Exon).