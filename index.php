<!DOCTYPE html>
<html>
    <head>
        <link href="web/css/main.css" rel="stylesheet" type="text/css">
        <link href="web/css/checkbox.css" rel="stylesheet" type="text/css">
        <link href="web/css/upload.css" rel="stylesheet" type="text/css">   
        <link href="web/css/slider.css" rel="stylesheet" type="text/css">
        <link href="web/css/position.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="./icon.png?v=2" >
        <title>OVAS Pipeline</title>
    </head>
    <body>

        <script src='web/js/combo_box_setter.js'></script>

        <!--- FLOATING HEADER -->
        <div id="float_header" name="float_header">
            <div id='float_header_div' >
                <div id="logo" name="logo">OVAS</div>
                <div id="logo_title" >Open-source Variant Analysis Pipeline</div>
                <div id="logo_buttons">
                    <input id="form_submit" name="form_submit" onclick="generateConfig()" type="button" value="Submit"/>
                    <input id="kill_button" onclick="killpid()" type="button" />
                </div>
            </div>
        </div>

        <!-- Error box -->
        <div id='error_message'></div>

        <?php include "web/php/load_previous_settings.php" ?>

        <!-- Begin Form -->
        <form action="index.php" enctype="multipart/form-data" id="form-wrapper" method="post" name="daform">
            <!-- Core (Mandatory) stages -->
            <div class="superclass" id="core_steps" name="core_steps">
                Core Modules
                <!-- Pedigree upload button -->
                <br>
                <input id="hidden_post" name="hidden_post" style="display:none;">
                <input name="hidden_submit_var" style="display:none;" type="checkbox" checked="" >
                <span>
                    Use files from previous run
                    <input <?php if (isset($_POST["use_previous_files"]) && $_POST["use_previous_files"]=="on") {echo "checked";} else { echo "";}?> dd="" id="use_previous_files" name="use_previous_files" onchange="togglePedUpload();" type="checkbox"/>
                </span>
                <input type='button' id='deleteall' value='Delete all files on server' onclick="deleteAllFiles()" />
                <br/>
                <div class="superclass" id="pedigree" name="pedigree">
                    <div class="fileUploadPed">
                        <input class="fileplaceholderPed" id="pedigreeplaceholder_upload" placeholder="+ Upload Pedigree" disabled="disabled" />
                        <input class="uploadbuttonPed" id="pedigree_upload" name="pedfile" onchange="uploadPedigree();" type="file" accept="text/plain,.pro,.ped,.pedfile,.pedigree" />
                    </div>
                    <div class="fileUploadPed" id='multiple_vcf_upload_container' >
                        <input class="fileplaceholderPed" id="multiple_vcf_upload_placeholder" placeholder="+ Upload Multiple VCFs" disabled="disabled" />
                        <input class="uploadbuttonPed" id="multiple_vcf_upload" name="multiple_files[]" onchange="uploadMultipleVCF();" type="file" multiple accept=".vcf"  />
                        <p>or specify manually for each below.</p>
                    </div>
                    
                    <table class="options" id="case_control_table" name="case_control_table">
                        <tr>
                            <th>Family</th>
                            <th>Cases</th>
                            <th>Controls</th>
                        </tr>
                        <!-- Data dynamically added from pedigree_parser.js -->
                    </table>
                </div>
                <div class="step" id="core_addgenes" name="core_addgenes">
                    Adding Genes
                    <input checked="" disabled="" type="checkbox">
                    <br>
                    <div class="expander">
                        <input type="checkbox">
                        <h3>
                            Genemap Options
                        </h3>
                        <div class="options">
                            <table>
                                <tr>
                                    <td>human genome version:</td>
                                    <td>
                                        <select id="hg_ver" name="hg_ver">
                                            <option value="hg38">
                                                GRCh38 (hg38)
                                            </option>
                                            <option value="hg19">
                                                GRCh37 (hg19)
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="row_splice">
                                    <td>
                                        include splice?
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["include_splice"]) && $_POST["include_splice"]=="on") {echo "checked";} else { echo "";}?> dd="" checked="" id="include_splice" name="include_splice" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr class="splice" id="row_spliceonly">
                                    <td>
                                        splice only
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["splice_only"]) && $_POST["splice_only"]=="on") {echo "checked";} else { echo "";}?> dd="" id="splice_only" name="splice_only" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr class="splice" id="row_spliceonlyacceptor">
                                    <td>
                                        no acceptor?
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["splice_ona"]) && $_POST["splice_ona"]=="on") {echo "checked";} else { echo "";}?> dd="" id="splice_ona" name="splice_ona" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr class="splice" id="row_spliceonlydonor">
                                    <td>
                                        no donor?
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["splice_ond"]) && $_POST["splice_ond"]=="on") {echo "checked";} else { echo "";}?> dd="" id="splice_ond" name="splice_ond" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr id="row_splicemargin">
                                    <td>
                                        splice bp margin:
                                    </td>
                                    <td>
                                        <input id="splice_margin" min="0" name="splice_margin" type="number" value='<?php if (isset($_POST["splice_margin"])){echo $_POST["splice_margin"];}else{ echo 5;}; ?>'/>
                                    </td>
                                </tr>
                                <tr id="row_utr">
                                    <td>
                                        include utr?
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["include_utr"]) && $_POST["include_utr"]=="on") {echo "checked";} else { echo "";}?> dd="" checked="" id="include_utr" name="include_utr" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr id="row_utronly">
                                    <td>
                                        include utr only?
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["utr_only"]) && $_POST["utr_only"]=="on") {echo "checked";} else { echo "";}?> dd="" id="utr_only" name="utr_only" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr id="row_promoter">
                                    <td>
                                        promoter upstream/downstream
                                    </td>
                                    <td>
                                        <input id="promoter_upstream" min="0" name="promoter_upstream" type="number" value='<?php if (isset($_POST["promoter_upstream"])){echo $_POST["promoter_upstream"];}else{ echo 500;}; ?>' />
                                        <input id="promoter_dnstream" min="0" name="promoter_dnstream" type="number" value='<?php if (isset($_POST["promoter_dnstream"])){echo $_POST["promoter_dnstream"];}else{ echo 500;}; ?>' />
                                    </td>
                                </tr>
                                <tr id="row_intergenic">
                                    <td>
                                        include intergenic?
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["include_intergenic"]) && $_POST["include_intergenic"]=="on") {echo "checked";} else { echo "";}?> dd="" id="include_intergenic" name="include_intergenic" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr id="row_intron">
                                    <td>
                                        include introns?
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["include_introns"]) && $_POST["include_introns"]=="on") {echo "checked";} else { echo "";}?> dd="" id="include_introns" name="include_introns" type="checkbox"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br />
                </div>
                <div class="step" id="core_addfunc" name="core_addfunc">
                    Adding Function
                    <input checked="" disabled="" type="checkbox">
                    <br />
                    <div class="options">
                        <table>
                            <tr>
                                <td>
                                    keep isoforms?
                                </td>
                                <td>
                                    <input <?php if (isset($_POST["keepisoforms"]) && $_POST["keepisoforms"]=="on") {echo "checked";} else { echo "";}?> dd="" id="keepisoforms" name="keepisoforms" type="checkbox"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
                <div class="step" id="core_addzygo" name="core_addzygo">
                    Adding Zygosity
                    <input checked="" disabled="" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <td>
                                    heterozygous &#8804;
                                </td>
                                <td>
                                    <input id="zygosity_threshold" max="99" min="1" name="zygosity_threshold" type="number" value='<?php if (isset($_POST["zygosity_threshold"])){echo $_POST["zygosity_threshold"];}else{ echo 75;}; ?>'>
                                    %
                                </td>
                                <td>
                                    &lt; homozygous
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br/>
                </div>
                <br/>
            </div>
            <!--- Optional Steps below -->
            <div class="superclass" id="optional_steps" name="optional_steps">
                Optional Modules
                <div class="step optional" id="opt_locfilter" name="opt_locfilter">
                    Physical Location Filter
                    <input <?php if (isset($_POST["location_filter_checkbox"]) && $_POST["location_filter_checkbox"]=="on") {echo "checked";} else { echo "";}?> dd="" id="location_filter_checkbox" name="location_filter_checkbox" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <th class="tooltip">
                                    locations must be in BED format
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <div id="textareawrap" name="textareawrap">
                                        <textarea id="location_filter_regions" name="location_filter_regions"><?php if (isset($_POST["location_filter_regions"])){ echo $_POST['location_filter_regions'];}?></textarea>
                                    </div>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
                <div class="step optional" id="opt_readdepth" name="opt_readdepth">
                    Read Depth Filter
                    <input <?php if (isset($_POST["read_depth_checkbox"]) && $_POST["read_depth_checkbox"]=="on") {echo "checked";} else { echo "";}?> dd="" id="read_depth_checkbox" name="read_depth_checkbox" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <td>
                                    read depth &gt;
                                </td>
                                <td>
                                    <input id="read_depth" name="read_depth" type="number" value='<?php if (isset($_POST["read_depth"])){echo $_POST["read_depth"];}else{ echo 0;}; ?>'/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
                <div class="step optional" id="opt_callqual" name="opt_callqual">
                    Call Quality Filter
                    <input <?php if (isset($_POST["call_quality_checkbox"]) && $_POST["call_quality_checkbox"]=="on") {echo "checked";} else { echo "";}?> dd="" id="call_quality_checkbox" name="call_quality_checkbox" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <td>
                                    call quality &gt;
                                </td>
                                <td>
                                    <input id="call_quality" name="call_quality" type="number" value='<?php if (isset($_POST["call_quality"])){echo $_POST["call_quality"];}else{ echo 0;}; ?>'/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
                <div class="step optional" id="opt_aaf" name="opt_aaf">
                    Alternative Allele Frequency Filter
                    <input <?php if (isset($_POST["aaf_checkbox"]) && $_POST["aaf_checkbox"]=="on") {echo "checked";} else { echo "";}?> dd="" id="aaf_checkbox" name="aaf_checkbox" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <td>
                                    AAF &nbsp &lt;
                                </td>
                                <td>
                                    <input id="aaf_perc" max="100" min="0" name="aaf_perc" type="number" value='<?php if (isset($_POST["aaf_perc"])){echo $_POST["aaf_perc"];}else{ echo 100;}; ?>'>
                                    %
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
                <div class="step optional" id="opt_mutetype" name="opt_mutetype">
                    Mutation Type Filter
                    <input <?php if (isset($_POST["mut_type_checkbox"]) && $_POST["mut_type_checkbox"]=="on") {echo "checked";} else { echo "";}?> dd="" id="mut_type_checkbox" name="mut_type_checkbox" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <th>
                                    keep only:
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    missense
                                </td>
                                <td>
                                    <input <?php if (isset($_POST["mtf_missense_box"]) && $_POST["mtf_missense_box"]=="on") {echo "checked";} else { echo "";}?> dd="" id="mtf_missense_box" name="mtf_missense_box" type="checkbox"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    nonsense
                                </td>
                                <td>
                                    <input <?php if (isset($_POST["mtf_nonsense_box"]) && $_POST["mtf_nonsense_box"]=="on") {echo "checked";} else { echo "";}?> dd="" id="mtf_nonsense_box" name="mtf_nonsense_box" type="checkbox"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    synonymous
                                </td>
                                <td>
                                    <input <?php if (isset($_POST["mtf_synonymous_box"]) && $_POST["mtf_synonymous_box"]=="on") {echo "checked";} else { echo "";}?> dd="" id="mtf_synonymous_box" name="mtf_synonymous_box" type="checkbox"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    noncoding
                                </td>
                                <td>
                                    <input <?php if (isset($_POST["mtf_noncoding_box"]) && $_POST["mtf_noncoding_box"]=="on") {echo "checked";} else { echo "";}?> dd="" id="mtf_noncoding_box" name="mtf_noncoding_box" type="checkbox"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
                <div class="step optional" id="opt_novel" name="opt_novel">
                    Novel Variants Filter
                    <input <?php if (isset($_POST["novel_varaints_checkbox"]) && $_POST["novel_varaints_checkbox"]=="on") {echo "checked";} else { echo "";}?> dd="" id="novel_varaints_checkbox" name="novel_varaints_checkbox" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <td>
                                    keep variants which are not present in database (dbSNP142)
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
            </div>
            <!-- Begin Modes of Inheritance -->
            <div class="superclass" id="modes_of_inheritance" name="modes_of_inheritance">
                Inheritance Model:
                <select id="trait_model" name="trait_model" onchange="traitOptions()">
                    <option value="">
                        [Unknown]
                    </option>
                    <option value="trait_ad">
                        Autosomal Dominant
                    </option>
                    <option value="trait_ar">
                        Autosomal Recessive
                    </option>
                    <option value="trait_xd">
                        X-Linked Dominant
                    </option>
                    <option value="trait_xr">
                        X-Linked Recessive
                    </option>
                    <option value="trait_mo">
                        Mosaic
                    </option>
                </select>
                <!-- These steps are hidden depending upon the combobox above -->
                <div id="autohider" name="autohider">
                    <!-- AUTOSOMAL DOMINANT -->
                    <div class="step" id="trait_ad" name="trait_ad">
                        <!-- not needed
                             <div class="options">
                             <table>
                             <tr>
                             <th>Filter</th>
                             </tr>
                             <tr>
                             <td>if unaffected control present</td>
                             <td><input type='checkbox' id='AD_filter_unaffecteds_box' name='AD_filter_unaffecteds_box' checked></td>
                             </tr>
                             </table>
                             </div>
                           -->
                    </div>
                    <!-- AUTOSOMAL RECESSIVE -->
                    <div class="step" id="trait_ar" name="trait_ar">
                        <div class="options">
                            <table>
                                <tr>
                                    <th>
                                        Filter
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        if one or both parents present
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["AR_filter_parents_box"]) && $_POST["AR_filter_parents_box"]=="on") {echo "checked";} else { echo "";}?> dd="" checked="" id="AR_filter_parents_box" name="AR_filter_parents_box" type="checkbox"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        if one or more unaffected siblings present
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["AR_filter_unaff_sibs_box"]) && $_POST["AR_filter_unaff_sibs_box"]=="on") {echo "checked";} else { echo "";}?> dd="" checked="" id="AR_filter_unaff_sibs_box" name="AR_filter_unaff_sibs_box" type="checkbox"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- X DOMINANT -->
                    <div class="step" id="trait_xd" name="trait_xd">
                        <div class="options">
                            <!-- keep blank -->
                        </div>
                    </div>
                    <!-- X RECESSIVE -->
                    <div class="step" id="trait_xr" name="trait_xr">
                        <div class="options">
                            <table>
                                <tr>
                                    <th>
                                        Filter
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        if unaffected control present
                                    </td>
                                    <td>
                                        <input <?php if (isset($_POST["XR_filter_unaff_box"]) && $_POST["XR_filter_unaff_box"]=="on") {echo "checked";} else { echo "";}?> dd="" checked="" id="XR_filter_unaff_box" name="XR_filter_unaff_box" type="checkbox"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- MOSAIC -->
                    <div class="step" id="trait_mo" name="trait_mo">
                        <div class="options">
                            <table>
                                <tr>
                                    <td>
                                        upper threshold
                                    </td>
                                    <td>
                                        <input id="mo_uthresh" max="99" min="1" name="mo_uthresh" type="number" value='<?php if (isset($_POST["mo_uthresh"])){echo $_POST["mo_uthresh"];}else{ echo 35;}; ?>'/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        lower threshold
                                    </td>
                                    <td>
                                        <input id="mo_lthresh" max="99" min="1" name="mo_lthresh" type="number" value='<?php if (isset($_POST["mo_lthresh"])){echo $_POST["mo_lthresh"];}else{ echo 15;}; ?>'/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Common Filter -->
            <div class="superclass">
                <div class="step optional" id="opt_venn" name="opt_venn">
                    Commonality Filter
                    <input <?php if (isset($_POST["common_filter_checkbox"]) && $_POST["common_filter_checkbox"]=="on") {echo "checked";} else { echo "";}?> dd="" id="common_filter_checkbox" name="common_filter_checkbox" type="checkbox">
                    <br>
                    <div class="options">
                        <table>
                            <tr>
                                <td>
                                    Filter at same
                                </td>
                                <td>
                                    <select id="common_filter_select" name="common_filter_select">
                                        <option value="level_variant">
                                            Variant
                                        </option>
                                        <option value="level_gene">
                                            Gene
                                        </option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </div>
            </div>
            <!-- Additional Annotations -->
            <div class="superclass" id="step name='step' annotations">
                Additional Annotations
                <div class="step" id="annot_NM_ids" name="annot_NM_ids">
                    Append NM IDs to gene names
                    <input <?php if (isset($_POST["annot_nm_id_box"]) && $_POST["annot_nm_id_box"]=="on") {echo "checked";} else { echo "";}?> dd="" id="annot_nm_id_box" name="annot_nm_id_box" type="checkbox">
                </div>
                <div class="step" id="annot_uniprot" name="annot_uniprot">
                    Append UniProt annotations
                    <input <?php if (isset($_POST["annot_uniprot_box"]) && $_POST["annot_uniprot_box"]=="on") {echo "checked";} else { echo "";}?> dd="" id="annot_uniprot_box" name="annot_uniprot_box" type="checkbox">
                </div>
                <div class="step" id="annot_genexpr" name="annot_genexpr">
                    Append Gene expression
                    <input <?php if (isset($_POST["annot_genexpr_box"]) && $_POST["annot_genexpr_box"]=="on") {echo "checked";} else { echo "";}?> dd="" id="annot_genexpr_box" name="annot_genexpr_box" type="checkbox">
                    <input id="hidden_post_expr" name="hidden_post_expr" style="display:none;">
                    <!-- tissue values from table are placed here before sending -->
                    <br>
                    <div class="options">
                        <div id="table-wrapper" name="table-wrapper">
                            <div id="table-scroll" name="table-scroll">
                                <table id="tissue_table" name="tissue_table">
                                    <thead>
                                        <tr>
                                            <th colspan="3">
                                                Filter Range
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                Tissues
                                            </th>
                                            <th colspan="3">
                                                Above Threshold Value
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- rows beyond this point are loaded from tissues.txt file
                                             please add extra data there -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </form>
        <!-- end form-wrapper -->
        <script src="web/js/window_onload.js" ></script>
        <script src="web/js/process_killer.js"></script>
        <!-- needs to be above init.php, but below results_view -->
        <script src="web/js/abs_positioning.js" ></script>
        <script src="web/js/ajax_pedigree.js"></script>

        
        <div class="superclass" id="side_view" name="side_view">
	    <div id="side_view_sub" >
                <div class="step" id="pedigree_view">
		    Pedigrees
		    <input checked="" id="pedigree_checkbox" type="checkbox">
		    <br />
		    <div class="options">
                        <input id="pedigree_zoom" type="range" />
                        <div id="pedigree_imgs"><!-- generated pedigree PNGs go here --></div>
		    </div>
		    <br />
                </div>

                <div class="step" id="results_view" name="results_view">
		    Running Processes
		    <input checked="" id="results_checkbox" type="checkbox"  />
		    <br />
		    <div class="options" id="results_output">
                        <!-- results go here -->
                        <?php include "web/php/init.php"; ?>
		    </div>
		    <br />
                </div>
                
                <div class="step" id="summary_view">
		    Summary
		    <input id="summary_checkbox" type="checkbox">
		    <br />
		    <div class="options" id="summary_window">
                        <!-- interactive summary goes here -->
                        <?php include "web/php/interactive_iframe.php"; ?>
		    </div>
		    <br />
                </div>
	    </div>
        </div>

        <script src="web/js/ajax_form.js"></script>
        <script src="web/js/handle_inheritance_combobox.js"></script>
        <script src="web/js/read_and_display_tissues.js"></script>
        <script src="web/js/pedigree_parser.js"></script>
        <script src="web/js/genemap_table_interactivity.js"></script>
        <script src="web/js/submit_button.js"></script>
        <script src="web/js/expand_summary.js"></script>
    </body>
</html>

