<?php

$debug = false;

// static_vars.php dictates where the working_dir

function moveFileToServer( $file, $dest, $log )
{
    return moveSingleFileToServer( $file['tmp_name'], $file['error'], $dest, $log);
}


function moveSingleFileToServer( $file_tmp, $file_err, $dest, $log){
    $message = 'Error uploading file';

    switch( $file_err ) {
        case UPLOAD_ERR_OK:
            $message = false;
            break;
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            $message .= ' - file too large (limit of bytes).';
            break;
        case UPLOAD_ERR_PARTIAL:
            $message .= ' - file upload was not completed.';
            break;
        case UPLOAD_ERR_NO_FILE:
            $message .= ' - zero-length file uploaded.';
            break;
        default:
            $message .= ' - internal error #' . $file_err;
            break;
    }
    
    if( !$message ) {
        if( !is_uploaded_file($file_tmp) ) {
            $message = 'Error uploading file - unknown error.';
        } else {
            // Let's see if we can move the file...
            if( !move_uploaded_file($file_tmp, $dest) ) {
                // No error supporession so we can see the underlying error.
                $message = 'Error uploading file - could not save upload (this will probably be a permissions problem in '.$dest.')';
            } else {
                //                rename($dest/$file
                $message = 'File uploaded okay.';
                return true;
            }
        }
    }
    fputs($log, $message);
    return false;
}    


function retrieveFileFromMultiples( $id ){
    $matching_ids_array = [];

    $arr = $_FILES['multiple_files']['name'];
    
    foreach( $arr as $a){
        $match_id = (array_filter( preg_split( "/[^0-9A-Za-z]/", $a), function($x){
            return preg_replace("/[^0-9]/", "", $x ) !== "" ;
        }))[0];

        if ($match_id === $id){
            $matching_ids_array[] = $a;  // push
        }
    }
       
    if (COUNT($matching_ids_array) !== 1){
        echo "<script>alert('multiple Ids detected for "  . $id . " " . print_r($matching_ids_array) . "')</script>";
        return 0;
    }

    // Got key, now retrieve index and pass to [tmp_name] to find filename
    $key = $matching_ids_array[0];
    $index = array_search( $key, $arr );

    $file = $_FILES['multiple_files']['name'][$index];
    $tmp  = $_FILES['multiple_files']['tmp_name'][$index];
    $err  = $_FILES['multiple_files']['error'][$index];

    $retar= [ $file, $tmp, $err ];
    return $retar;
}

function console_log( $data ){
  echo '<script>';
  echo 'console.log('. json_encode( $data ) .')';
  echo '</script>';
}

function performMove( $id, $num_multiples, $logfile, $dir ){
    $key = "fileperson_" . $id;
    $ticked = ($_POST["tickperson_" . $id] === "on");

    if ($id and $ticked){
        console_log("id and ticked:" . $id );
        $tmp_name = $_FILES[$key]["tmp_name"];
        $fil_name = $_FILES[$key]["name"];
        $err_name = $_FILES[$key]["error"];
        
        // Search existing file uploads near tickboxes
        if ($tmp_name === ""){
            // Otherwise if there's no match, use one from the multiples bin
            console_log("not a single, checking multiples : " . $num_multiples );
            if ($num_multiples > 0 ){
                $new_arr  = retrieveFileFromMultiples( $id );
                $tmp_name = $new_arr[1];
                $fil_name = $new_arr[0];
                $err_name = $new_arr[2];
                console_log("From multiples: " . $tmp_name . ", " . $fil_name . ", " . $err_name );
            }
        }

        if ($tmp_name === ""){
            console_log( "File for " . $id . " could not be found" );
            return 0;
        }      
        fputs($logfile, $fil_name . "-->" . $id . "  ");
        $new_name = $dir . '/' . $id . ".vcf";
        moveSingleFileToServer( $tmp_name, $err_name, $new_name, $logfile);
    }
}



// Move each uploaded file to the working directory within the right
// family and case/control folder
if ($_POST)
{
    if ($_POST["hidden_post"])
    {
        $mapline = $_POST["hidden_post"];
        
        if ($debug){ echo "<h2>" . $mapline . "</h2><br />";}
        
        $fam_tokens = explode(" || ", $mapline);

        $logfile = fopen($working_dir . "/upload.log", 'w') or die("could not open log file");
        //system("rm -rf $init_dir $init_dir/../0*");

        $num_multiples = 5;
        $deb = print_r( $_FILES['multiple_files'], true );
        fputs($logfile, $deb );
        
        for($f = 0; $f < count($fam_tokens); $f++){
            
            $famstring = $fam_tokens[$f];

            $fam_casecontrols = explode( " : ", $famstring );
            $fid = $fam_casecontrols[0];
          
            $case_control = explode(" # ", $fam_casecontrols[1] );

            $caseids = explode( " ", $case_control[0] );
            $contids = explode( " ", $case_control[1] );

            // Mkdir
            $fam_dir = $init_dir . "/family_" . $fid;
            $cas_dir = $fam_dir . "/cases";
            $con_dir = $fam_dir . "/controls";
            fputs($logfile, PHP_EOL . $fam_dir . "   " );
            mkdir($fam_dir, 0777, true); // default perm with wide access, and yes recurse
            mkdir($cas_dir, 0777, true);
            mkdir($con_dir, 0777, true);

            // Handle cases          
            fputs($logfile, " " . $fid . ":::" . PHP_EOL . "    cases:    [ ");
	    
            for ($c=0; $c < count($caseids); $c++){
                $id = $caseids[$c];
                performMove( $id, $num_multiples, $logfile, $cas_dir );
            }

            // Handle controls
            fputs($logfile, " ]," . PHP_EOL . "    controls: [ ");
            
            for ($c=0; $c < count($contids); $c++){
                $id = $contids[$c];
                performMove( $id, $num_multiples, $logfile, $con_dir );
            }

            // Move pedfile too
            moveFileToServer( $_FILES["pedfile"], $working_dir . "/pedfile.pro", $logfile);
            fputs($logfile, " ] " . PHP_EOL);            
        }
        fclose($logfile);

        if ($debug){
            echo "<textarea style='height:50%;' >";
            echo file_get_contents( $working_dir . "/upload.log");
            echo "</textarea>";
        }
    }
}

?>
