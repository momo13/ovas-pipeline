<?php

if ($_POST){
	
    //	echo "Running pipe: <br />";

	// Echo in real time
	//Tell PHP to flush stdout
    ob_implicit_flush(true);
    ob_end_flush();

    $descriptorspec = array(
        0 => array("pipe", "r"),       // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),       // stdout is a pipe that the child will write to
        2 => array("pipe", "w")        // stderr is a pipe that the child will read from
    );

     
//    $cmd = "screen -mS HSAP bash -c './server/HSAP_PIPELINE/general_start.sh; read x;'";
    //$cmd = "count=20; while [ \$count -gt 0 ]; do count=\$(( \$count - 1 )); echo \$count; sleep 0.1; done";
    $cmd = "bash -c './general_start.sh 2>&1'";

    flush();
    $process = proc_open($cmd, $descriptorspec, $pipes, './server/HSAP_PIPELINE/', array());
    
    echo "<pre>";

    if (is_resource($process)) {
        while ($s = fgets($pipes[1]))
        {
			$data = explode("\r", $s);
			$size = count($data);
			if ( $size > 3 ){
				//print "$size" . " -- " . "$data[0]" . end($data);
                print "$data[0]" . end($data);
			} else {
				print $s;
			}
//			fwrite(STDOUT, "\r$s");
			flush();
		}

        if( count($pipes[2])>1 ){
            echo "Error:" . '<br />';
            while ($s = fgets($pipes[2])) {
                print $s;
                flush();
            }
        }
    }
    echo "</pre><p>Done.</p>";
    proc_close($process);
}

?>
