<?php

// Take input names from web form and append the values to the 
// associated shell variable names, as given by the map file

if ($_POST) {
	
	$mapfile = fopen('web/data/jname_to_shell.map','r') or  die("Couldn't find map file to read");
	$confile = openWriteFile($working_dir . "/pipeline.config") or  die("Couldn't open config file to write");
	
	$sdate = date(DATE_RFC2822);
	$header = "#!/bin/bash" . PHP_EOL . PHP_EOL
	 . "# config file for HSAP Pipeline" . PHP_EOL . PHP_EOL
	 . "# generated via web at : " . PHP_EOL
	 . "#    " . $sdate . PHP_EOL . PHP_EOL;
	
	fwrite( $confile, $header );
	
	while(!feof($mapfile)){
		$line = trim( fgets($mapfile) );

                if ( strlen($line) < 2 ){ continue; }
                if ( strpos($line, "#") === 0 ){ continue; }
	
		$tokens = preg_split("/\s+/", $line);
		
		$shell_name = $tokens[0];
		$javas_name = $tokens[1];
		
		$value = $_POST[$javas_name];
		
		// Handle tissues file
		if ($shell_name == "append_gexpr" ){
			if ($_POST[$javas_name] == "on" ){
				$value = '.' . explode( $working_dir, $tissue_file)[1];
			}
			else {
				$value = "N";
			}
			$outline = "export " . $shell_name . '="' . $value . '";' . PHP_EOL;
			fwrite( $confile, $outline);
			continue;
		}

		if (!$value){
			if ($value == 0){
				$value = 0;
			}
			else {
				$value = "N";
			}
		}
		else if ($value == "on" ){
			$value = "Y";
		}
		
		// Trait prefixes
		else if ( substr_compare( $value, "trait_", 0, 6) === 0){
			$value = preg_split("/trait_/", $value)[1];
			$value = strtoupper($value);
		}
		
//		echo $javas_name . ' = ' . $shell_name . ' [' . $value . '] <br />';
		
		$outline = "export " . $shell_name . '="' . $value . '";' . PHP_EOL;
        //
        // export so that unsourced sub-scripts can see it
        //
		fwrite( $confile, $outline);
	}
	
	fclose($mapfile);
	fclose($confile);

    //	echo "Written Config." . PHP_EOL;
}




?>
