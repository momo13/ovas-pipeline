<?php

$debug = false;

if ($_POST)
{
	
	if ($_POST["annot_genexpr_box"] == "on" ){
		
		$tiss_file = openWriteFile($tissue_file) or  die("Couldn't open " . $tissue_file . "file to write");
		
		// Write headers
		fwrite($tiss_file, "#TISSUES\tRATIO\tRATIO2\nSELECT_ALL_TISSUES\t0\t0\n");
		
		$outline = $_POST["hidden_post_expr"];		
		$tissues = preg_split("/\s*\|\s*/", $outline);
		
		if ($debug){echo "<pre>";}
		
		for ($i = 1; $i < count($tissues); $i++){
			
			//print("$i");
			$values = preg_split("/_/", $tissues[$i]);
			
			//print_r($values);
			
			$name = $values[0];
			$val1 = $values[1];
			$val2 = $values[2];
						
			if ($debug){echo "$name : { $val1, $val2 }\n";}
			fwrite($tiss_file, "$name\t$val1\t$val2\n");
		}
		
		fclose($tiss_file);
		
		if ($debug){echo "</pre>";}
		
		echo "Written Tissues." . PHP_EOL;
		
	}
}

?>
