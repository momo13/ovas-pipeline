<?php

include "web/php/static_vars.php";

function loadPrevPipelineSettings(){

   // Map out variable map file
   $pipe_map = array();
   $mapname = 'web/data/jname_to_shell.map';
   $exists = file_exists($mapname);

   if ($exists !== 0){
       echo "<script>console.log('Couldnt find map file to read -- ". $exists . "')</script>";
       return -1;
   }

   $mapfile = fopen( $mapname, 'r');


   while(!feof($mapfile)){
        $line = trim( fgets($mapfile) );
                
        // Skip blank lines and hashes
        if ( strlen($line) < 2 ){ continue; }
        if ( strpos($line, "#") === 0 ){ continue; }                
        $tokens = preg_split("/\s+/", $line);
                
        $shell_name = $tokens[0];
        $javas_name = $tokens[1];
                
        $pipe_map[$shell_name] = $javas_name;
   }
   fclose($mapfile);

   //echo "<script> console.log('" . print_r($pipe_map) . "')</script>";
   // Now read in the pipeline config and set $_POST variables for index.php to use
   //
   $confile = fopen($working_dir . "/pipeline.config",'r') or die("<script> alert('Couldnt open config file ". $working_dir . " to parse')</script>");

   while(!feof($confile)){
        $line = trim( fgets($confile) );

        // Skip blank lines and hashes
        if ( strlen($line) < 2 ){ continue; }
        if ( strpos($line, "#") === 0 ){ continue; }
        
        $tokens = preg_split("/\s+/", $line);
        $tokens = preg_split("/=/", $tokens[1]);

        $shell_name = trim( $tokens[0] );
        $shell_valu = trim( preg_split("/;/", $tokens[1])[0] );

        // Strip quotes
        $shell_name = str_replace('"', "", $shell_name);
        $shell_valu = str_replace('"', "", $shell_valu);

        // Handle tissues -- this is a file, cannot parse, skip
        if ($shell_name === "append_gexpr" ) {
           continue;
        }

        echo "<script> console.log('" . $shell_name . " : " . $shell_valu . " , " . $javas_name . " : " . $javas_valu . "'); </script>";
        
        // Handle trait prefixes -- TODO: needs to call a JS method to set the correct combobox option
        if ($shell_name === "ih_model" ){
           $option = "trait_" . strtolower($shell_valu); // e.g. "AR" --> "trait_ar"
           echo '<script> setInheritanceBox("' . $option . '") </script>';
           continue;
        }
        else if ($shell_name === "bedtarget_dbver" ){
           echo "<script> setHGBox('" . $shell_valu . "')</script>";
        }

        // Assert shell_name must be in map
        $javas_name = $pipe_map[$shell_name];
        $javas_valu = 0;

        // Convert shell value to php
        //        echo "<script> console.log('" . $shell_valu . " : " . strcmp($shell_valu,Y)  . "'); </script>";
        if ($shell_valu === "Y"){
            $javas_valu = "on";
            $_POST[$javas_name] = $javas_valu;
        }
    }
   fclose($confile);
}

loadPrevPipelineSettings();

?>