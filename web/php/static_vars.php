<?php

$working_dir='./server/runs/working_dir/';
$hsap_dir = './server/HSAP_PIPELINE/';

$init_dir=$working_dir . "/00_input";
$tissue_file = $working_dir . '/tissue_expression.list';

if (!file_exists($init_dir)){
    mkdir($init_dir, 0777, true);
}

chmod($init_dir, 0777);
chmod($working_dir, 0777);

//chgrp($init_dir, "tetris");
//chgrp($working_dir, "tetris");

function openWriteFile($path){
	touch($path);
	chmod($path, 0777);
    //	chgrp($path, "tetris");
	// local users should be part of hsap group too
	return fopen($path, 'w');
}

if ($_POST)
{
    // Clear existing tissue file every run
    unlink($tissue_file);

}
//else {
//    system("rm -rf $init_dir"); // For now...
//}
?>
