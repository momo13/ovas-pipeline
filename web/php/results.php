<?php

ob_implicit_flush(true);
ob_end_flush();        //Tell PHP to flush stdout
    
$cmd = "ping 127.0.0.1 -c 5";

$descriptorspec = array(
    0 => array("pipe", "r"),        // stdin is a pipe that the child will read from
    1 => array("pipe", "w"),       // stdout is a pipe that the child will write to
    2 => array("pipe", "w")                  // stderr is a pipe that the child will read from
);

ob_flush();
$process = proc_open($cmd, $descriptorspec, $pipes, './public/php/', array());
echo "<pre>";

if (is_resource($process)) {
    while ($s = fgets($pipes[1])) {
        print $s;
        ob_flush();
    }

    if( count($pipes[2])>1 ){
        echo "Error:" . '<br />';
        while ($s = fgets($pipes[2])) {
            print $s;
            ob_flush();
        }
    }
}
echo "</pre>";
proc_close($process);

?>
