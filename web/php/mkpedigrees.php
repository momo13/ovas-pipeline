<?php

include "static_vars.php";

$q = $_REQUEST["q"];
$j = $_REQUEST["j"]; // yes, no

//Hack to pass newlines through GET
$q = str_replace("ZIZIZ", PHP_EOL, $q);

// Current script is two directories in, needs to be referenced.
$mkped_loc = "../../$hsap_dir/helpers/mkpedigree/";

$ped_folder = "../../$working_dir/pedigrees/";
if (!file_exists($ped_folder)){
    mkdir($ped_folder, 0777, true);
}

$pedloc = $ped_folder . "mkpedfile.tmp";

// Move old pedfile to eski, compared in mkpedigrees.sh
if (file_exists( $pedloc ) ){
    rename( $pedloc, $pedloc. ".eski" );
}

// Only write if data not null (otherwise the page is being refreshed)
if (strlen($q)>5){
    $pedtmp = openWriteFile( $pedloc ) or die("No valid pedigree placement");
    fwrite($pedtmp, $q);
    fclose($pedtmp);
}

system("$mkped_loc/mkpedigrees.sh $pedloc $mkped_loc/HaploPainter1.043.pl $ped_folder $j 2>&1");


?>
