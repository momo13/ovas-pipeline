var total_img_widths = -1,
    resize_timer = null,
    ped_scale_slider = null,
    pedview_imgs = null,
    num_ped_imgs = 0;
    

addLoadEvent(function(){
    ped_scale_slider =  document.getElementById('pedigree_zoom');
    pedview_imgs = document.getElementById('pedigree_imgs');

    ped_scale_slider.oninput = function setPedScale(){
        var percent_value = ped_scale_slider.value;
        resizePedigreeView(scaling_factor = (percent_value / 100) );
    };
});



function resizePedigreeView(scaling_factor = -1)
{
    if (pedview_imgs == null){
        pedview_imgs = document.getElementById('pedigree_imgs');
    }

    var imgs = pedview_imgs.getElementsByTagName("img"),    
        num_imgs = imgs.length;

    if (num_imgs !== num_ped_imgs){ 
        console.log("polling, num_imgs", num_imgs);
        return 0;
    }

    clearTimeout(resize_timer); 
    ped_view.style.display = "block";


    if (scaling_factor === -1){
    
        // calculate total image width (if not done)
        if (total_img_widths === -1){
            for (var i=0; i < num_imgs; i++){
		total_img_widths += imgs[i].naturalWidth;
	    }
        }

        var side_width = side_view.style.width.split('px')[0];

        // Set limits on scaling
        var min_sf = 0.2,
            max_sf = 0.3;

        var min_padding = 20;

        var min_widths = min_sf * total_img_widths,
            max_widths = max_sf * total_img_widths;
            
        var padding = min_padding;

        if (side_width > max_widths){
            padding = (side_width - (max_widths+min_padding) )/(2*num_imgs); // /2 for left and right
        }

        if (padding < min_padding){ padding = min_padding;}


        // Actually calculate scale with useable space
        var useable_width = side_width - (padding * num_imgs * 2);

        scaling_factor = useable_width / total_img_widths;
        
        if (scaling_factor < min_sf) {scaling_factor = min_sf;}
        else if (scaling_factor > max_sf) {scaling_factor = max_sf;}
    }

    for (var i=0; i < num_imgs; i++)
    {
        var img = imgs[i];
        
        var new_width = img.naturalWidth * scaling_factor;
        
        var padsides = parseInt(padding/2);
        img.style.marginLeft = padsides + 'px'; //dynamic when out of sf limits
        img.style.marginRight = padsides + 'px'; //dynamic when out of sf limits
        img.style.width = new_width + 'px';
    }
    
    pedview_imgs.style.display = 'block';
    side_view.style.width = (side_view.style.width.split('px')[0] - 30)+'px';
}



function ajaxPedigreeRequest(name, filedata, generate="yes", callback=0){
    //filedata = can be blank if not generating
    //generate = no, then existing images on server are simply returned.

    var xhr = new XMLHttpRequest(),
        res = null;
        
    //Hide pedigree view till processing is complete
    if (pedview_imgs == null){
        pedview_imgs = document.getElementById('pedigree_imgs');
    }
    
    pedview_imgs.style.display = 'none';
    //resizeAll();


    xhr.onreadystatechange = function()
    {
        if (xhr.readyState == 4 && xhr.status == 200)
        {
            var ped_width= pedview_imgs.offsetWidth/3;
            
            res = xhr.responseText;
//            console.log("res", res)
           
            if ( (res.indexOf("NONE")!==-1) ) {
                //                pedview_imgs.innerHTML = res.split('\n').join('<br />');
                pedview_imgs.innerHTML = "No data to reuse";
                console.log("no pedigrees...");
                if (callback!=0){
                    callback();
                }
            }
            else {
                var images = res.split("ZIZIZ");  // ZI delim
                var num_imgs = images.length - 1; // last is blank, skip
                
                side_view.style.display = 'block'; 
                //resizeAll();
                
                pedview_imgs.innerHTML = "";
                
                for (var i=0; i < num_imgs; i++){
                    pedview_imgs.innerHTML += images[i] //.replace("alt=","style='margin:"+padding+"px;width:"+im_w+";' alt=")
                }

                // Polls until number of recieved images are the same as
                // those displayed
                num_ped_imgs = num_imgs;
                
                resize_timer = setInterval(function()
                {
                    console.log("num_ped_imgs", num_ped_imgs);
                    resizePedigreeView();
                }, 100);
            }
        }
    };
    
    // Hack to send newlines over GET
    filedata = filedata.split('\t').join(' ');
    filedata = filedata.split('\n').join("ZIZIZ");
    
    xhr.open('get', "web/php/mkpedigrees.php?j="+generate+"&q="+ filedata, true);
    xhr.send();
}
