// Global Funcs
function toInt(arg){            //For some reason parseInt wont work properly
    return parseInt(arg);   // as a lambda function arg.... wtf?
}

var error_message = null;

function notify_user(message, obj, callback=0)
{
    //Focus
    var form_scoll_pos = document.getElementById('form-wrapper').scrollTop,
        window_height = window.innerHeight,
        obj_pos = obj.offsetTop + daform.clientTop,
        obj_xpos = obj.offsetLeft + daform.clientLeft,
        obj_height = obj.clientHeight;

//    console.log(obj_pos, obj_xpos, obj_height);

    // Out of view in two scenarios:
    //       obj_pos > form_scroll_pos + window_height
    //       obj_pos < form_scroll_pos
    if ((obj_pos > form_scoll_pos + window_height) || (obj_pos < form_scoll_pos) ){
        obj.scrollIntoView();
    }
   
    // Flashing Box
    var end = 1000;
    
    for (var i=200; i < end; i += 400)
    {
        setTimeout(function(){
            obj.style.border = "4px solid red";
            obj.style.content = "TEST";
        }, i);

        setTimeout(function(){
            obj.style.border = "";
        }, i+ 200);
    }

    // Show Message
    error_message.style.display = "block";
//    error_message.style.top = obj_pos + obj_height + 'px';
//    error_message.style.left = obj_xpos + 'px';
    error_message.innerHTML = message;

    setTimeout(function(){
        error_message.style.display = "none";
    }, end);

    

    // Execute callback AFTER flashing
    var res = 0;
    if (callback!=0){
        setTimeout(function(){
            res = callback();
        }, end);
    }
    return res;
}



function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}


addLoadEvent(function(){
    error_message = document.getElementById("error_message"); // hidden div
});
