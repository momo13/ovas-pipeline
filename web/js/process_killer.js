

var kill_process_pid = -1; //Global, set by ./general_start.sh

function killpid()
{

    if (kill_process_pid === -1){
        console.log("kill_pid is invalid")
        return -1;
    }


    var xhr = new XMLHttpRequest(),
        res = null;

    xhr.onreadystatechange = function()
    {
        if (xhr.readyState == 4 && xhr.status == 200)
        {
            //            res = xhr.responseText;
            //            console.log(res)
            document.getElementById('kill_button').style.display = 'none';
            document.getElementById('form_submit').style.display = 'block';

            // Show everything but summary when page is ready
            addLoadEvent(function(){
                document.getElementById('side_view').style.display = 'block';
                document.getElementById('pedigree_view').style.display = 'block';
                document.getElementById('results_view').style.display = 'block';
                document.getElementById('summary_view').style.display = 'none';
            });

            // Stop autoscroller
            if (scrollResults !== undefined) {clearInterval(scrollResults);}
            
            return 0
        }
    };


    xhr.open('get', "web/php/kill_process.php?p="+kill_process_pid+"", true);
    xhr.send();

}


function deleteAllFiles()
{  
    var xhr = new XMLHttpRequest(),
        res = null;

    xhr.onreadystatechange = function()
    {
        if (xhr.readyState == 4 && xhr.status == 200)
        {
            res = xhr.responseText;
            console.log(res);

            populatePedigreeTable(0,justclear=true);
            previous_cb.checked = false;
            togglePedUpload();
            
            return 0
        }
    };

    xhr.open('get', "web/php/delete_all_files.php", true);
    xhr.send();

}
