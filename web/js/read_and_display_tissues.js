var tissue_table = null,
    rowCount = 0,
    table_body = null;


addLoadEvent(function(){
    tissue_table = document.getElementById('tissue_table');
    table_body = tissue_table.getElementsByTagName('tbody')[0];
    readTextFile('web/data/tissues.txt');
});


function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, true);
    // true - asynchronous, false - synchronous (depreciated)
    rawFile.onreadystatechange = function ()
    {
	if(rawFile.readyState === 4)
	{
	    if(rawFile.status === 200 || rawFile.status == 0)
	    {
		// Parse text data
		var allText = rawFile.responseText.split(/\n/)
		var tissue_map = {}
		
		for (var t=0; t < allText.length; t++)
		{
		    var tissue = allText[t]
		    
		    if (tissue[0]==='#' || tissue.length < 2) continue;
		    
		    var name_only = tissue.split(/\s[0-9]+$/)[0],
			num_split = tissue.split(name_only)[1]
		    suffix = 1;
		    
		    if (num_split.length>1){
			suffix = parseInt( num_split[1] );
		    }
		    tissue = name_only
		    
		    if (!(tissue in tissue_map)){
			tissue_map[tissue] = [];
		    }
		    tissue_map[tissue].push(suffix)
		}
		//console.log( tissue_map );
		
		//Sort
		var tissue_list = Object.keys(tissue_map).sort();
		
		for (var t=0; t < tissue_list.length; t++)
		{
		    var tissue = tissue_list[t];
		    var row = table_body.insertRow(rowCount++);
		    
		    var cellCount = 0
		    var tiss = row.insertCell(cellCount++);
		    
		    //tiss.innerHTML = "<span class='tissues'>" + tissue + "</span>";
		    tiss.innerHTML = tissue

		    for (var v=0; v < tissue_map[tissue].length; v++){
			var ex = tissue_map[tissue][v];
			
			var expe = row.insertCell(cellCount)
			cellCount ++;
			//expe.innerHTML = '<input type="number" id="'+tissue+'_'+ex+'" name="'+tissue+'_'+ex+'" value=0 />'
			
			var input = document.createElement('input');
			input.setAttribute("type","number");
			input.setAttribute("value",0);
                        input.setAttribute("step",0.05);
                        
			
			expe.appendChild( input );
		    }
		}
	    }
	}
    }
    rawFile.send(null);
}
