
/* access to:
 *  - family_map (from pedigree_parser.js)
 *  - tissue_table (from read_and_display_tissues.js)
 */

/* ids generated via:
 * 	grep -oP "(?<=id=)[^\s]+[\"']" monika_form.html
 */


function validateForm(){
    /* Most form validation is done server-side, but a few pre-reqs must still be met:*/
  
    if (previous_cb.checked === false){
    
        //Uploaded a pedigree?
        var ped_inp = document.getElementById('pedigree_upload');
        if (ped_inp.value === ""){
            notify_user("Please upload a pedigree", ped_inp.parentNode);
            return -1;
        }

    
        // Check that files have been selected for upload
        var ped_table = document.getElementById('case_control_table'),
            spans = ped_table.getElementsByClassName('person');
    
        var all_unticked = true
        var error_ids = []
    
        for (var s=0; s < spans.length; s++){
            var span = spans[s]

            var inputs = span.getElementsByTagName('input'),
                id = inputs[0].id.split('_')[1];

            var value_set = false,
                box_ticked = false;

            for (var i = 0; i < inputs.length; i ++){
                var inp = inputs[i]
                
                if (inp.type === "file"){}
                else if (inp.type === "checkbox"){ box_ticked = inp.checked;}
		else if (inp.type === "text" ){    value_set = (inp.value !== "") } //placeholder

            }
            
            if (box_ticked){
                all_unticked = false;
                
                if (!value_set){
                    error_ids.push( id )
                }
            }
        }

        if (all_unticked){
            notify_user("Please upload at least one sample.", ped_table);
            return -1
        }

        if (error_ids.length > 0){
            notify_user("The following ticked ids have no files: " + error_ids.join(', '),
                        ped_table);
            return -1;
        }
    }
    else { // Check that the previous run actually has useable data

        var ret_res = 0
        var res = ajaxPedigreeRequest("ped_data","",generate="no", callback=function(){
            // On fail, highlight box
            ret_res = notify_user("No data available, please reupload", previous_cb.parentNode, callback=function(){
                //Untick box after message
                previous_cb.checked = false;
            });
        }); // pull from server
        
        if (ret_res === -1) {
            return -1
        }
    }


    // Linkage region parsed correctly?
    var linkage_box_ticked = document.getElementById('location_filter_checkbox');

    if (linkage_box_ticked.checked){

        var error_message = ""

        var linkage_box = document.getElementById('location_filter_regions');
        var regions = linkage_box.value.split('\n');

        console.log(regions)

        for (var r=0; r < regions.length; r++){
           
            var reg = regions[r],
                data = reg.split(/\s/);

            var current_error_msg = reg

            if (data.length < 3){
                current_error_msg += " must follow 'chrom start stop' format."
                console.log(reg)
                continue
            }
            
            var chrom = data[0].trim(),
                pos1 = data[1].trim(),
                pos2 = data[2].trim();

            // Check chrom
            var number_part = chrom.replace(/.hr/,"")

            console.log("np",chrom,pos1,pos2,number_part);

            if (number_part.trim() == ""){
                current_error_msg += " [Invalid chromosome]";
            }

            //Check positions match
            if ( isNaN(Number(pos1)) || isNaN(Number(pos2)) ){
                current_error_msg += " [Invalid region spec]";
            }

            
            if (current_error_msg.replace(reg,"").length >0){
                error_message += "<br/>"+current_error_msg
            }
        }
        if (error_message.length > 0){
            error_message = error_message.replace("<br/>","")
            notify_user(error_message, linkage_box);
            return -1;
        }
    }             

    return 0
}

function generateConfig(){

    if (validateForm() === -1) return -1;
    
    console.log("sent to server");
    
    if ( document.getElementById('annot_genexpr_box').checked )
    {
        addTissueDataToPOST ( grabTissueData() );
    }
    daform.submit();  
}



function addTissueDataToPOST ( expr_data )
{
	var hp = document.getElementById('hidden_post_expr');
	hp.value = expr_data;
}



function grabTissueData()
{
	var outline=""  // massive string to pass to PHP
	
	// referenced in read_and_display_tissues.js
	for (var i=2, row; row = tissue_table.rows[i]; i++) // skip headers i=2
	{
		
		var tiss_name = row.cells[0].innerHTML
		
		for (var j=1, col; col = row.cells[j]; j++)
		{
			var val = col.firstChild.value
			tiss_name += '_' + val;
		}
		
		outline += '|' + tiss_name;
	}
//	console.log(outline);
	return outline
}
