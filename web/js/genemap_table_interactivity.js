// Table logic for handling spam

var splice_button = document.getElementById('include_splice'),
    splice_only_button = document.getElementById('splice_only'),
    row_splice = document.getElementById('row_splice'),
    row_spliceonly= document.getElementById('row_spliceonly'),
    row_spliceonlyacceptor = document.getElementById('row_spliceonlyacceptor'),
    row_spliceonlydonor = document.getElementById('row_spliceonlydonor'),
    row_splicemargin = document.getElementById('row_splicemargin'),
    row_promoter = document.getElementById('row_promoter');

var utr_button = document.getElementById('include_utr'),
    row_utr = document.getElementById('row_utr'),
    utr_only_button = document.getElementById('utr_only'),
    row_utronly = document.getElementById('row_utronly');

//var intergenic_button = document.getElementById('include_intergenic'),
//    intron_button = document.getElementById('include_introns'),
var row_intergenic = document.getElementById('row_intergenic'),
    row_intron = document.getElementById('row_intron');
    

function spliceOnlySet(enabled)
{
    let block='table-row'
    
    splice_only_button.checked = enabled;
    
    row_spliceonlyacceptor.style.display = enabled?block:'none';
    row_spliceonlydonor.style.display = enabled?block:'none';

    row_intergenic.style.display = enabled?'none':block;
    row_intron.style.display = enabled?'none':block;
    row_utr.style.display = enabled?'none':block;
    row_utronly.style.display = enabled?'none':block;
    row_promoter.style.display = enabled?'none':block;
}

function includeSpliceSet(enabled)
{
    let block='table-row'
    
    splice_button.checked = enabled;    
    
    row_spliceonly.style.display = enabled?block:'none';
    row_splicemargin.style.display = enabled?block:'none';

    spliceOnlySet(enabled);
}

function includeUTRset(enabled)
{
    let block='table-row'
    
    utr_button.checked = enabled;
    
    row_utronly.style.display= enabled?block:'none';

    if (!enabled){
	UTRonlySet(false);
    }

    
}

function UTRonlySet(enabled)
{
    let block='table-row'
    utr_only_button.checked = enabled;

    row_splicemargin.style.display = enabled?'none':block;
    row_spliceonly.style.display = enabled?'none':block;
    row_splice.style.display = enabled?'none':block;
    row_intergenic.style.display = enabled?'none':block;
    row_intron.style.display = enabled?'none':block;
    row_promoter.style.display = enabled?'none':block;
}




splice_button.onclick = function(yes){includeSpliceSet(yes.target.checked);};
splice_only_button.onclick = function(yes){spliceOnlySet(yes.target.checked);};

utr_button.onclick = function(yes){includeUTRset(yes.target.checked);};
utr_only_button.onclick = function(yes){UTRonlySet(yes.target.checked);};

includeSpliceSet(true);
includeUTRset(true);
spliceOnlySet(false);


