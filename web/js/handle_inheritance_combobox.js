
//Gets all the step ids 
var accepted_models = function(){
	var autohider_children = document.getElementById("autohider").getElementsByClassName("step")
	
	var arr=[]
	for (var a=0; a < autohider_children.length; a++){
		arr.push( autohider_children[a].id )
	}
	return arr;
}();


//Updates combobox
var combobox = document.getElementById('trait_model')

combobox.onchange = function(e){
	var value = e.target.value;
	
	//Hide all
	for (m=0; m < accepted_models.length; m++){
		var model_id = accepted_models[m];
		var model_div = document.getElementById( model_id )
		
		model_div.style.display = ""; // hide
	}
	
	//Unhide current
	var current_div = document.getElementById( value )
	current_div.style.display = 'block'; //show
};
