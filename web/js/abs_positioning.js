
var main_pipe = null,
    side_view = null,
    summary_view = null,
    results = null, 
    ped_view = null,
    header = null;



function populateResizers(){
    main_pipe = document.getElementById('form-wrapper');
    side_view = document.getElementById('side_view');
    summary_view = document.getElementById('summary_view');
    results = document.getElementById('results_view');
    ped_view = document.getElementById('pedigree_view');
    header = document.getElementById('float_header');
}    


addLoadEvent(function(){
    populateResizers();
//    document.body.onresize();
});


function __resizeAll()
{
    var top_x = header.clientHeight;
    
    main_pipe.style.position = "absolute";
    main_pipe.style.top = top_x + 'px';
//    results.style.display = "block"; // --> set via php
    
//    if (results.style.display === "block" ){
    var left_width = main_pipe.clientWidth,
	height = main_pipe.clientHeight;
    
    var space = 20;
    
    // left_width + space + right_width + space = window.innerWidth
    var right_x = left_width + space,
	right_width = window.innerWidth - (right_x + space/2);

    var hack_top = -8,
	hack_bottom = -10;

    side_view.style.position = "absolute";
    side_view.style.top = (top_x + hack_top) + 'px';
    side_view.style.left = right_x + 'px';
    side_view.style.width = right_width +'px';
    side_view.style.height = (height + hack_bottom) + 'px';
}

//document.body.onresize = function(){resizeAll();}
