
var Person = function(id, gender, affected, mother, father){
	this.id = id;
	this.gender = gender;			 // 1 - male, 2-female, 0-unknown
	this.affected = affected ; 		 // 0,1,2
	this.mother = mother;
	this.father = father;
};

var previous_cb = null,
    ped_input_div = null,
    hidden_post_dom = null;


addLoadEvent(function(){
    previous_cb = document.getElementById('use_previous_files');
    ped_input_div = document.getElementById('pedigree');
    hidden_post_dom = document.getElementById('hidden_post'); // embedded in the form

    togglePedUpload(); //init;

});

function togglePedUpload(){

    if (previous_cb.checked){
        ped_input_div.style.display = "none";
        ped_view.style.display= "";
        document.getElementById('pedigree_checkbox').checked = true;

        var res = ajaxPedigreeRequest("ped_data","",generate="no", callback=function(){
            // On fail, highlight box
            notify_user("No data available, please reupload", previous_cb.parentNode, callback=function(){
                //Untick box after message
                previous_cb.checked = false;
                togglePedUpload();
            });
        }); // pull from server
    }
    else {
        ped_input_div.style.display= "";
        ped_view.style.display= "none";
        side_view.style.display="none";
    }
}


function uploadVCFAction(id){
    //console.log("CLICKED", id)
    let placeholder= document.getElementById('fileplaceholder_'+id);

    let tmp = this.value.split('\\');
    tmp = tmp[tmp.length - 1].split('/');
    
    let base = tmp[tmp.length - 1];
    placeholder.value = base;   
}



function uploadVCFPrompt(id){
    
    var span = document.getElementById('spanperson_'+id),
	tickbox = document.getElementById('tickperson_'+id),
	fileprompt = document.getElementById('fileperson_'+id),
	spacers = span.getElementsByTagName('span');

    if (tickbox.checked){
        fileprompt.addEventListener('change', uploadVCFAction.bind(fileprompt, id), false);
	fileprompt.style.display = 'inline';
	//span.innerHTML = '<br />'+span.innerHTML+"<br />";
	
	//Grab updated tickbox and tick it
	//tickbox = document.getElementById('tickperson_'+id);
	//tickbox.checked = true;
	for (s=0; s < spacers.length; s++){
	    spacers[s].style.display = 'inline-block'
	}       

    } else {
        fileprompt.removeEventListener('change', uploadVCFAction.bind(fileprompt, id), false);
	//Clear any loaded file (maybe keep for ease)
	//		var file_prompt = document.getElementById('fileperson_'+id);
	fileprompt.style.display = 'none';
	//		file_prompt.value = null;
	//		file_prompt.remove();
	
	//Remove BR
	//var inner = span.innerHTML;
	//var start = inner.indexOf('>')+1,
	//	end = inner.indexOf('<br', start +1);
	
	//span.innerHTML = inner.substr(start, end-start);
	for (s=0; s < spacers.length; s++){
	    spacers[s].style.display = 'none'
	}
    }
    
    //resizeAll(); // side view shifts, needs updating

}


// Pedigree Table populating
function populatePedigreeTable(map, justclear=false)
{   
    var table = document.getElementById('case_control_table');
    table.style.display = 'block';
    
    //Clear rows 
    while (table.rows.length > 1){
        table.deleteRow(1);
    }

    if (justclear){
        table.style.display = "none";
        return 0;
    }
    
    var fid_keys = Object.keys(map).sort();

    var hidden_fam_array = [];
    
    //[ [ 101, [37], [] ], [102, [32], []], [103, [40,41,42] ] ];
    //" 101 :  37 #  || 102 :  32  #  || 103 :  40  41  42  #  ||"
    
    for (var f=0; f < fid_keys.length; f++){
	var fid = fid_keys[f];
        
	//Each family gets a row to themselves
	var row = table.insertRow(table.rows.length);
	
	var case_boxes = "", 
	    control_boxes = "";
        
	var hidden_case_ids = [],
	    hidden_control_ids = [];
	
	for (var id in map[fid])
	{
	    var pers = map[fid][id],
		perc_tickbox = (
		    "<span class='person' id='spanperson_"+id+"' >"
			+"<span id='spacer' style='display:none' ><br /></span>"
			+id+"<input type='checkbox'" + " id='tickperson_"+id+"' name='tickperson_"+id+"' onchange='uploadVCFPrompt("+id+")' ></input>"
                    	+"<span id='spacer' style='display:none' >"
                           +"<div class='fileUpload' >"
                              +"<input class='fileplaceholder' id='fileplaceholder_"+id+"' placeholder='Upload VCF' disabled='disabled' />"
                              +"<input type='file' class='uploadbutton' id='fileperson_"+id+"' name='fileperson_"+id+"' style='display:none;'  accept='.vcf'  />"
                           +"</div>"
                        +"</span><br/>"
		   +"</span>");
	    
	    if (pers.affected === 2){
		case_boxes += perc_tickbox
		hidden_case_ids.push( id );
	    }
	    else {
		control_boxes += perc_tickbox;
		hidden_control_ids.push( id );
	    }
	}
	
	// hidden family name
	var case_names = hidden_case_ids.join(' '),
	    cont_names = hidden_control_ids.join(' ');
	
	hidden_fam_array.push(fid+" : "+case_names+" # "+cont_names);
	
	//Add Case and Control Cells
	row.insertCell(0).innerHTML = fid;
	row.insertCell(1).innerHTML = case_boxes;
	row.insertCell(2).innerHTML = control_boxes;
    }

    //Add hidden text to the hidden input for POST submission
    var hidden_text = hidden_fam_array.join(" || ");
    //console.log(hidden_text);
    hidden_post_dom.value = hidden_text;
    
}

// Multiple VCF handling
function showMultipleVCFButton(){
    document.getElementById('multiple_vcf_upload_container').style.display = "block";
}

function uploadMultipleVCF()
{
    let multiple_placeholder = document.getElementById('multiple_vcf_upload_placeholder'),
        multiple_files       = document.getElementById('multiple_vcf_upload');

    multiple_placeholder.value = ( multiple_files.files.length ) + " file(s) selected"

    let resolved  = resolveVCFsIntoPedigreeTable( multiple_files.files );

    multiple_placeholder.value = ("Resolved [" + resolved.num + "/" + ( multiple_files.files.length ) + "] file(s)")
        + ((resolved.nots.length > 0)?("[Not:" + resolved.nots + "]"):"");
    
}

function resolveVCFsIntoPedigreeTable( files ){

    let names_map = {}
    for (let n=0; n < files.length; n++){
        let name = files[n].name
        let id_multiple = name.split(/[^0-9A-Za-z]/).filter( x => x.replace(/[^0-9]/g,"") != "" ); // id must be a number !

        if (id_multiple.length > 1){
            console.log("Multiple numeric ids detected in file", name, " : ", id_multiple, ". Please use a single numeric ID in the filename")
            continue
        }
        let id = Number(id_multiple[0])
        
        names_map[id] = { file: files[n], used: false};
    }

    //console.log(names_map)

    let persons = document.getElementsByClassName("person"),
        unused = []

    
    for (let p = 0; p < persons.length; p ++){
        let span = persons[p],
            id   = Number( span.id.split("spanperson_")[1] );

        if (id in names_map){
            let tickbox = document.getElementById('tickperson_'+id),
                filehold= document.getElementById('fileplaceholder_'+id);

            tickbox.checked = true;
            uploadVCFPrompt(id);
            
            filehold.value = names_map[id].file.name
            names_map[id].used = true;
            /* NOTE: The file value is not actually set, since this is insecure in the browser */
        }
        //console.log("id=", id)
    }
    for (let id in names_map){
        if (!names_map[id].used){
            unused.push(id)
        }
    }
       
    return { num: files.length - unused.length, nots: unused }
}





// File input handling

function uploadPedigree(){
    document.getElementById('results_output').innerHTML = ""; //clear results on new pedigree upload
    document.getElementById('summary_view').style.display= "none"; //Hide summary view

    processPedigree();

    showMultipleVCFButton();
}


function processPedigree() {
    
    
    function isPedfile(text_unformatted)
    {
	var text = text_unformatted.split('\n');
	
	for (var l=0; l < 10; l++){ // Just read first 10 lines
	    var tokes = text[l].split(/\s+/)
	    
	    if (tokes.length === 6){
		return true;
	    }
	    return false;
	}
    }
    
    
    function makeFamMap(text_unformatted)
    {
	var family_map={};
	var text = text_unformatted.split('\n');
	var header_lines = [];
	
	// Handle haplo data
	for (var l=0; l< text.length; l++){
	    var line = text[l];
	    
	    if (line.length < 5 ){continue};
	    
	    //Populate family map
	    //
	    var people_info = line.trim().split(/\s+/).map(toInt);
	    
	    var fam = people_info[0], id  = people_info[1],
		pat = people_info[2], mat = people_info[3],
		sex = people_info[4], aff = people_info[5];
	    
	    if (!(fam in family_map)) family_map[fam] = {};			//sanity check...
	    
	    if (!(id in family_map[fam]))
	    {
		var pers = new Person(id, sex, aff, mat, pat);
		family_map[fam][id] = pers;
	    }
	}
	return family_map
    }
    
    var file = document.getElementById("pedigree_upload").files[0];
    var lr = new FileReader();

    var name = file.name.split('.'),
        ext = name[name.length-1];

    if (ext === "vcf"){       
	notify_user("Please upload a pedigree file (not VCF)", ped_input_div);
	return -1;     
    }
    
    
    lr.onloadend = function(e){
	
	if (!( isPedfile(e.target.result) )){
            
	    notify_user("Bad pedigree file.", ped_input_div);
	    return -1;
	}
	
	var pedigree_data = e.target.result;
	var fam_map = makeFamMap(pedigree_data);
	//console.log(fam_map);
	
	populatePedigreeTable(fam_map);
	ajaxPedigreeRequest("ped_data", pedigree_data);
    };
    lr.readAsText(file);
}
