#!/usr/bin/env python3

file="index.php"

with open(file,'r') as index:

    for line in index:
        tokens = line.split(' ')
        tokens[-1] = tokens[-1].splitlines()[0]

        for t in range(len(tokens)):
            token = tokens[t];

            if token.startswith("id="):
                string_part = token.split("id=")[1]

                end = ""
                

                if string_part[-1] == '>':
                    string_part = string_part[:-1]
                    token = token.split('>')[0]
                    end = " >"

                if string_part[0] == "'" or string_part[0] == '"':
                    string_part = string_part[1:]
                if string_part[-1] == '"' or string_part[-1] == "'":
                    string_part = string_part[:-1]

                                    
                already_got_name=False
                
                if t != len(tokens)-1:
                    if tokens[t+1].startswith("name="):
                        already_got_name = True;

                if not already_got_name:
                    tokens[t] = token + ' ' + "name='" + string_part + "'" + end

        print( ' '.join(tokens))
