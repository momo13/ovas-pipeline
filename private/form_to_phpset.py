#!/usr/bin/env python3

import sys
from bs4 import BeautifulSoup

#
# parses form input and adds <?php isset() to each input value ?>
#

file=sys.argv[1]

soup = BeautifulSoup(open(file,'r').read(),"html.parser")


form = soup.find(id='form-wrapper')


## the dd part at the end just shifts the ="" part away from the php
phpend=" dd" 


def phpval(name):
    return "$_POST['"+name+"']"

def phpif(name,check=0):
    start = "(isset("+phpval(name)+")"
    
    if check==0:
        return start+")";
    
    return start+" && "+phpval(name)+"==\""+check+"\") "



for inp in form.find_all("input"):
    info = inp.attrs

    # Applies to checkboxes, numbers, and buttons (ignored)
    if 'type' in info:
        
        if 'name' not in info:continue
        
        typ = info['type']
        name = info['name']
        
        if typ == 'checkbox':
                       
            inp["<?php if "+phpif(name,"on")+"{echo \"checked\";} else { echo \"\";}?>"+phpend] = ""

            
        
        elif typ == 'number':
            
            default = inp['value']
            inp["value"] = "STARTPHP if "+phpif(name)+"{echo "+phpval(name)+";}else{ echo "+default+";}; ENDPHP"


for inp in form.find_all("select"):
    
    info = inp.attrs
    if 'name' not in info:continue
    
    # Iterate through options
    name = inp['name']
    for opt in inp.find_all("option"):

        opt_val = opt['value']
        
        if opt.find("php")!=-1:continue #already editted
        
        opt["<?php if" + phpif(name, opt_val) + "{echo \"selected\";};?>"] = ""
    
    #print(inp)


for inp in form.find_all("textarea"):
    
    info = inp.attrs
    if 'name' not in info:continue

    name = info['name']
    
    inp.append("STARTPHP "+phpif(name)+"{ echo "+phpval(name)+";};ENDPHP")

    
print(soup.prettify())
