#!/bin/bash

[ $# -lt 1 ] && echo "`basename $0` <script>

Use this script to test individuals parts of the pipeline. It loads environment vars" && exit -1


. helpers/load_environment_vars.source

$*

