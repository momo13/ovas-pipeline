#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs common filtering over all files depending on level" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Common filter " green
suffix=""

opt_commonality_work=0

if [ "$opt_commonality_level" = "level_variant" ];then

    opt_commonality_work="Y"
    #
    printline "  Variant (Sibling) level "
    #
    venn_siblings.py $vcfs
    # produces .output files for each VCF @ $infold
    suffix="variant_level"

elif [ "$opt_commonality_level" = "level_gene" ]; then

    opt_commonality_work="Y"
    #
    printline "  Gene (Non-Sibling) level "
    #
    genelist="$outfold/common_genes.list"
    extract_common_gene_list.py $vcfs | sort | uniq > $genelist
    #
    venn_non_sibling.py $genelist $vcfs
    # produces .output files for each VCF
    suffix="gene_level"
fi


###
#   Move all the .output case files to the $output folder
###
for vcf in $vcfs;do
    #    currFileProgress $vcf
    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".common."$suffix".vcf"

    tmp_name=$vcf".output"
    
    mv $tmp_name $new_name
    finishProgress $new_name $vcf    
done


echo ""
