#!/usr/bin/env python2
import sys


def usage ():
	print >> sys.stderr, 'usage: %s <common_gene_list> <VCF> (optional <more_VCF>)' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, '\nthis script uses common gene list produced by ''extract_common_gene_list.py'' and extracts variants for eatch VCF file which fall in that common gene list'
	exit(0)

if len (sys.argv) < 3:
	usage()

common_gene_list = open (sys.argv [1], "r")
vcf_files = sys.argv [2:]

common_gene_map = {}

format_card='a'
annotation_card ='b'
has_been_printed_before = False

for gene in common_gene_list:
	gene=gene.splitlines () [0]
	if gene not in common_gene_map:
		common_gene_map[gene]=1

for vcf_file in vcf_files:
	f=open (vcf_file, 'r')
	o=open (vcf_file + '.output', 'w')
	for line in f:
		line=line.splitlines () [0]
		if line.startswith ('#'):
			print >> o,line

		if line.startswith ('#CHROM'):
			guide_cards=line.split ('\t')
			format_card = guide_cards.index ('FORMAT')
			for item in guide_cards:
				if item.isdigit():
					annotation_card=guide_cards.index (item)

		if line.startswith ('chr'):
			talkens = line.split ('\t')
			try:
				guide_line = talkens[format_card]
			except TypeError:
				if has_been_printed_before == False:
					print >> sys.stderr, 'did not find Guide line which starts with #CHROM	POS....'
					has_been_printed_before = True
				guide_line = talkens[8]
			guide_line_splitted = guide_line.split (':')
			if 'AL' in guide_line_splitted:
				gene_card_index = guide_line_splitted.index ('AL')
			else:
				print >> sys.stderr, 'AL indicator was not found, genpender.py MUST run before this script'
				exit (-1)
			annotation_line = talkens [annotation_card]
			annotation_line_splitted = annotation_line.split (':')
			gene_with_info = annotation_line_splitted [gene_card_index]
			gene_with_info_splitted = gene_with_info.split (',') [0]
			gene_in_vcf = gene_with_info_splitted.split ('|') [0]

			if gene_in_vcf in common_gene_map:
				print >> o,line

	f.close()
	o.close()
