#!/usr/bin/env python2
import sys
import fileinput

def usage ():
	print >> sys.stderr, 'usage: %s [<VCFs>]' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, '\nthis script takes 2 or more VCF files, extracts common gene list'
	exit(-1)

if len (sys. argv) < 3:
	usage()

#vcf_output = open ('common_gene_list', 'w')
gene_map = {}

for line in fileinput.input():
	new_line=line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		format_card = guide_cards.index ('FORMAT')
		for item in guide_cards:
			if item.isdigit():
				annotation_card=guide_cards.index(item)


	if new_line.startswith ('chr'):
		talkens = new_line.split ('\t')
		format_line = talkens [format_card]
		format_talkens = format_line.split (':')
		gene_card = format_talkens.index ('AL')
		gene_line = talkens [annotation_card]
		gene_line_talkens = gene_line.split(':')
		gene_with_add_info = gene_line_talkens [gene_card]
		gene_separated = gene_with_add_info.split ('|')
		gene = gene_separated [0]
		if gene not in gene_map:
			gene_map[gene]=7

gene_list=gene_map.keys()

for true_gene_list in gene_list:
	if true_gene_list != 'INTERGENIC':
		print true_gene_list
	fileinput.close()




















