#!/usr/bin/env python2

import sys, os

if len(sys.argv) < 4:
    print >> sys.stderr, ''' %s  <list+of+case+VCFs> [<list+of+control+VCFs>] --outfolder=<control_output_folder> --prefix=<output directory prefix extraction>

Produces a list of unique (not neccesarily common) variants across all cases and filters the controls for them to produce a shortened but comprehensive list of controls that encompasses case data''' % sys.argv[0]
    exit(-1)


cases = sys.argv[1].split('+')
conts = []
outfold = None
prefext = None

for arg in sys.argv[2:]:
    if arg.startswith("--"):
        if arg.startswith("--outfolder="):
            outfold = arg.split("--outfolder=")[-1].strip()
        elif arg.startswith("--prefix="):
            prefext = arg.split("--prefix=")[-1].strip()
        else:
            print >> sys.stderr, "Could not parse", arg
            exit(-1)
    else:
        conts = argv.split('+')
    
if conts == []:
    print >> sys.stderr, "No controls detected"

if outfold == None:
    print >> sys.stderr, "No output folder specified."
    exit(-1)



case_variant_map = {} # chrom -> variant

print >> sys.stderr, " [Harmonizing Controls to Case regions]"
print >> sys.stderr, "\rReading case: ",
offs=0    
for case_file in cases:
    offs += 1   
    print >> sys.stderr, '\r\t\t', '\t'*offs, os.path.basename(case_file).split('.')[0],

    with open(case_file,'r') as cf:
        for line in cf:
            if line[0] == '#':continue

            tokens = line.split('\t')

            chrom = tokens[0]
            pos = int(tokens[1])

            if chrom not in case_variant_map:
                case_variant_map[chrom] = {}

            case_variant_map[chrom][pos] = 1
        cf.close()


def extractDirectoryStructure(vcf):
    global prefext

    dirs = cont_file.split(os.path.sep)
    st_index=-1
    for x in range(len(dirs)):
        dx = dirs[x]
        if dx.startswith(prefext):
            st_index = x

    if st_index == -1:
        print >> sys.stderr, "could not find", prefext, "in", vcf
        return os.path.basename( vcf )

    return os.path.sep.join(dirs[st_index:])



print >> sys.stderr, "\nProcessing control: ",
offs=0
for cont_file in conts:
    offs += 1

    file_base = extractDirectoryStructure( cont_file ) # family_1234/controls/22.vf   
    new_base = os.path.join( outfold, file_base )

    file_name = '.'.join( os.path.basename( new_base ).split('.')[:-1] + ['harmonized', 'vcf'] )
    new_base =  os.path.join( os.path.dirname( new_base ), file_name )

    try:
        os.makedirs( os.path.dirname( new_base ) )
    except OSError:
        pass # exists
    
    out_file = open(new_base , 'w')
    print >> sys.stderr, "\r\t\t", '\t' * offs, os.path.basename(out_file.name).split('.')[0],

    with open(cont_file, 'r') as cof:
        for line in cof:
            line = line.splitlines()[0]
            if line[0] == '#':
                # Keep headers
                print >> out_file, line               
                continue

            tokens = line.split('\t')
            chrom = tokens[0]
            pos = int(tokens[1])

            if chrom in case_variant_map:
                if pos in case_variant_map[chrom]:
                    print >> out_file, line

        cof.close()

print >> sys.stderr, ""
