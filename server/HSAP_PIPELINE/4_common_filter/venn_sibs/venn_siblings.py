#!/usr/bin/env python2
import sys
import fileinput

def usage ():
	print >> sys.stderr, 'usage: %s <[VCFs]>' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, '\nthis script takes 2 or more VCF files (siblings), extracts all the variants which are the same in all siblings at these postiions in VCF file: chr	position	nucleotide _change'
	exit(-1)

if len (sys.argv) < 3:
	usage ()

vcf_files = sys.argv [1:]

the_key_map = {}  
the_new_key_map = {}

f_length = len(vcf_files)


for line in fileinput.input():
	new_line=line.strip()
	if new_line.startswith ('#CHROM'):
		guide_card_splitted = new_line.split ('\t')
		chrom_card = guide_card_splitted.index ('#CHROM')
		position_card = guide_card_splitted.index ('POS')
		ref_card = guide_card_splitted.index ('REF')
		alt_card = guide_card_splitted.index ('ALT')

	if new_line.startswith ('chr'):
		talkens = new_line.split ('\t')
		chr_pos = talkens [chrom_card]
		genome_position = talkens [position_card]
		ref_allele = talkens [ref_card]
		alt_allele = talkens [alt_card]
		the_key = chr_pos + '_' + genome_position + '_' + ref_allele + '_' + alt_allele

		if the_key not in the_key_map:
			the_key_map [the_key] = 0

		the_key_map [the_key] = the_key_map [the_key] + 1

for k,v in the_key_map.iteritems():
	if f_length == v:
		the_new_key = k

		if the_new_key not in the_new_key_map:
			the_new_key_map [the_new_key] = 1

for vcf_file in vcf_files:
	f=open (vcf_file, 'r')
	o=open (vcf_file + '.output', 'w')
	for line in f:
		line=line.splitlines () [0]
		if line.startswith ('#'):
			print >> o,line

		if line.startswith ('#CHROM'):
			guide_card=line.split ('\t')
			chrom_card_vcf = guide_card.index ('#CHROM')
			position_card_vcf = guide_card.index ('POS')
			ref_card_vcf = guide_card.index ('REF')
			alt_card_vcf = guide_card.index ('ALT')

		if line.startswith ('chr'):
			talkens = line.split ('\t')
			chrom_pos_vcf = talkens [chrom_card_vcf]
			genome_position_vcf = talkens [position_card_vcf]
			ref_allele_vcf = talkens [ref_card_vcf]
			alt_allele_vcf = talkens [alt_card_vcf]
			the_key_vcf = chrom_pos_vcf + '_' + genome_position_vcf + '_' + ref_allele_vcf + '_' + alt_allele_vcf

			if the_key_vcf in the_new_key_map:
				print >> o,line

	f.close()
	o.close()
