#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Truncates controls to show only variants that the cases have" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

case_vcfs=$(grabCaseVCFs $infold)
cont_vcfs=$(grabControlVCFs $opt_last)

# Use case VCFS to mold control VCFs
logAndDo extract_unique_list_of_variants.py `echo $case_vcfs | sed 's/\s/+/g'`  `echo $cont_vcfs | sed 's/\s/+/g'` --outfolder=$outfold --prefix="family_"

updateAndReplaceCases $infold
updateAndReplaceControls $outfold
