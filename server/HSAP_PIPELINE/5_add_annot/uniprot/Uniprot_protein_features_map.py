#!/usr/bin/env python2

class UniprotHandler:
	
	
	def __init__ (self, file_name):
		self.file_name = file_name
		self.map = {}
		self.reading_the_file ()
	
	
	def reading_the_file (self):
		f = open (self.file_name, "r")
		for line in f:
			line=line.splitlines ()[0]
			#talkens = line.split ("\t")
			#gene_name = talkens [0]
			#start_pos = talkens [1]
			#end_pos = talkens [2]
			#feature = talkens [3]
			gene_name, start_pos, end_pos, feature = line.split ("\t")
			
			try:
				start_pos = int (start_pos)
			except ValueError:
				start_pos = -99
			
			try:
				end_pos = int (end_pos)
			except ValueError:
				end_pos = -99
			
			gene_list = gene_name.split (",")
			for gene in gene_list:
				if not gene in self.map:
					self.map [gene] = []
				self.map [gene].append ((start_pos, end_pos, feature))

