#!/usr/bin/env python2
import sys
from Uniprot_protein_features_map import UniprotHandler

def usage ():
	print >> sys.stderr, "usage: %s <uniprot_data> <vcf_file>" % sys.argv[0].split('/')[-1]
	print >> sys.stderr, "this script uses Uniprot databse info to annotate the coding variants (prior PCH annotation needed)"
	exit (-1)


if len (sys.argv)!= 3:
	usage ()

my_variable = UniprotHandler( sys.argv[1] )
my_Uniprot_map = my_variable.map

filename = open(sys.argv [2], 'r')


def handleFORMATlines(my_array):
	my_list_of_formats = my_array
	for format_line in my_list_of_formats:
		if '##FORMAT=<ID=PA, Number=.,Type=String,Description="Protein annotation list corresponding to a genelist">' in format_line:
			exit (-1)
	my_list_of_formats.append ('##FORMAT=<ID=PA, Number=.,Type=String,Description="Protein annotation list corresponding to a genelist">')
	return my_list_of_formats


def printFORMATlines(array):
	for element in array:
		print element

format_list = []
last_line = ""

has_been_printed_before = False

for line in filename:
	line=line.splitlines ()[0]
	if line.startswith ('#'):
		if line.startswith ('##') and not line.startswith ('##FORMAT'):
			print line
			continue

		if line.startswith ('##FORMAT'):
			format_list.append (line)
		else:
			if len (format_list)!=0:
				if last_line.startswith ('##FORMAT'):
					new_list_of_formats = handleFORMATlines (format_list) 
					printFORMATlines(new_list_of_formats)
					format_list =[]
				else:
					print line

		last_line = line

		if line.startswith ('#CHROM'):
			print line
			guide_cards = line.split ('\t')
			format_card = guide_cards.index ('FORMAT')
			for item in guide_cards:
				if item.isdigit():
					annotation_card=guide_cards.index (item)
		continue


	talkens = line.split ('\t')
	try:
		indicators = talkens [format_card]
	except TypeError:
		if has_been_printed_before == False:
			print >> sys.stderr, 'did not find Guide line which starts with #CHROM	POS....'
			has_been_printed_before = True
		format_card = 8
		indicators = talkens [format_card]
	subindicators = indicators.split (":")

	protein_change ='c'
	gene_index = 'd'

	if 'PCH' in subindicators:
		my_var = 'PCH'
		protein_change=subindicators.index (my_var)
	else:
		print >> sys.stderr, 'could not find PCH'
		exit (-1)


	if 'AL' in subindicators:
		gene_index = subindicators.index ('AL')
	else:
		print >> sys.stderr, 'could not find AL'
		exit (-1)


	if 'PA' not in subindicators: ###
		subindicators.append ('PA')
		new_subindicators = ":".join (subindicators)
		talkens [format_card] = new_subindicators
		line= "\t".join (talkens)


	try:
		annotations = talkens [annotation_card]
	except TypeError:
		if has_been_printed_before == False:
			print >> sys.stderr, 'did not find Guide line which starts with #CHROM	POS....'
			has_been_printed_before = True
		
		annotations = talkens [9]


	subannotations = annotations.split (":")

	this = subannotations [protein_change]
	that = subannotations [gene_index]
	protein_list= this.split (",")
	gene_and_other = that.split ("|")
	the_gene = gene_and_other [0].strip ()


	if the_gene in my_Uniprot_map:
		features = my_Uniprot_map [the_gene]
		all_feature_storage = []
		for protein in protein_list:
			bisected_feature_storage = []
			try:
				just_number = int (protein [3:-1])
			except ValueError:
				just_number = -1

			for single_feature in features:
				start_pos, end_pos, feature = single_feature
				if just_number >= start_pos and just_number <= end_pos:
					
					feature=feature.replace (',', '+')
					
					unique_line = feature + ' ' + 'position=' + str (start_pos) + '-' + str (end_pos)
					bisected_feature_storage.append (unique_line)

			feature_line = "+".join (bisected_feature_storage)

			if len (bisected_feature_storage)==0:
				feature_line = '*'



			all_feature_storage.append (feature_line)

		feature_to_append = ",".join (all_feature_storage)

		line = line + ':' + feature_to_append
		print  line

