#!/usr/bin/env python2
import sys

def usage():
	print >> sys.stderr, 'usage: %s <hg19_map_with_NM> <VCF>' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, '\nthis scripts adds an extra annotation column with RefSeq uniq names for each gene and isoform. it is dependant on "genpender" whitch adds gene annotation column'
	exit(-1)

if len (sys. argv)!=3:
	usage ()

def handleFORMATlines(my_array):
	my_list_of_formats = my_array
	for format_line in my_list_of_formats:
		if '##FORMAT=<ID=NM, Number=.,Type=String,Description="For each isoform (including "main" gene) an unique RefSeq NM assiged">' in format_line:
			exit (-1)
	my_list_of_formats.append ('##FORMAT=<ID=NM, Number=.,Type=String,Description="For each isoform (including "main" gene) an unique RefSeq NM assiged">')
	return my_list_of_formats

def printFORMATlines(array):
	for element in array:
		print element


hg_map_file = open (sys.argv [1], "r")
vcf_file= open (sys.argv [2],"r")

format_list = []
gene_map = {}


last_line = ""
has_been_printed_before = False

for line in hg_map_file:
	line=line.splitlines ()[0]
	if line.startswith ('#'):
		guide_cards=line.split ('\t')
		refseq_gene_card=guide_cards.index ('RGNAME')
		gene_name_card=guide_cards.index ('GENEINFO')
		continue


	talkens = line.split ('\t')
	refseq_gene_name = talkens [refseq_gene_card]
	gene_with_info = talkens [gene_name_card]
	gene_with_info_splitted = gene_with_info.split ('|')
	just_gene = gene_with_info_splitted [0]

	if just_gene not in gene_map:
		gene_map[just_gene]=refseq_gene_name


for line in vcf_file:
	line=line.splitlines ()[0]
	if line.startswith ('#'):
		if line.startswith ('##') and not line.startswith ('##FORMAT'):
			print line
			continue
		
		if line.startswith ('##FORMAT'):
			format_list.append (line)
		else:
			if len (format_list)!=0:
				if last_line.startswith ('##FORMAT'):
					new_list_of_formats = handleFORMATlines (format_list) 
					printFORMATlines(new_list_of_formats)
					format_list =[]
				else:
					print line
		last_line = line

		if line.startswith ('#CHROM'):
			print line
			guide_cards = line.split ('\t')
			format_card = guide_cards.index ('FORMAT')
			for item in guide_cards:
				if item.isdigit():
					annotation_card=guide_cards.index (item)
		continue

	talkens = line.split ('\t')
	try:
		indicators = talkens [format_card]
	except TypeError:
		if has_been_printed_before == False:
			print >> sys.stderr, 'did not find Guide line which starts with #CHROM	POS....'
			has_been_printed_before = True
		indicators = talkens [8]
	subindicators = indicators.split (":")

	if 'NM' not in subindicators:
		subindicators.append ('NM')
		new_guide_indexes = ":".join (subindicators)
		talkens [format_card] = new_guide_indexes
		line= "\t".join (talkens)
	else:
		continue
		
	NM_all = "x"
	NM_array = []
	if 'AL'in subindicators:
		gene_line_vcf=subindicators.index('AL')
		annotation_line = talkens [annotation_card]

		gene_line_splited = annotation_line.split (":")
		gene_with_isoforms = gene_line_splited [gene_line_vcf]
		eatch_isoform_vcf=gene_with_isoforms.split (",")

		for isoform in eatch_isoform_vcf:
			isoform_with_info = isoform.split ('|')
			just_isoform = isoform_with_info [0]

			if just_isoform == "INTERGENIC":
				NM_number = '***'
				NM_array.append (NM_number)

			else:
				if just_isoform in gene_map:
					NM_number = gene_map[just_isoform] 
					NM_array.append (NM_number)


	else:
		print >> sys.stderr, 'AL indicator was not found, genpender.py MUST run before this script'
		exit (-1)


	NM_all = ",".join (NM_array)
	line = line + ':' + NM_all
	print line
























