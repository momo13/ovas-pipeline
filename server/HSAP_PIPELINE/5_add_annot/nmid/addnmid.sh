#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder><output folder>

Annotated batch of cases for NM identifiers" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

# Only need to annotate the cases
vcfs=$(grabCaseVCFs $infold)
vcf_inputs=`echo $vcfs | sed 's|\s|\+|g'`;

printline " Adding NMIDs " green

for vcf in $vcfs; do
	
	new_dir=$(setNewDir $vcf $infold $outfold)
	new_file_prefix=$new_dir/`basename $vcf .vcf`
	new_name=$new_file_prefix".nmid.vcf"
	
#	currFileProgress $vcf
	logAndDo RefSeq_gene_names.py $dbmap $vcf > $new_name
	finishProgress $new_name $vcf
	
done

echo ""
