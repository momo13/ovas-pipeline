#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder>          
                                                                           
Annotates gene expression data to all files" && exit -1

infold=$1
outfold=$2

# copy expression input from server
append_gexpr=$working_folder/$append_gexpr

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Adding Gene Expression " green

for vcf in $vcfs; do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".gexpr.vcf"

#    currFileProgress $vcf
    logAndDo expression.py $append_gexpr $gexprdata $vcf > $new_name
    finishProgress $new_name $vcf
    
done

echo ""
