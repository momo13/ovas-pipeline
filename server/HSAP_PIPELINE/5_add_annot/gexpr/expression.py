#!/usr/bin/env python2
import sys

def usage():
	print >> sys.stderr, 'usage: expression.py <configuration_file> <expression_file> <vcf_file>'
	print >> sys.stderr, '\nthis script reads the configuration file, where the tissues of interest are ticked and ratios defined'
	exit(-1)

if len (sys.argv)!=4:
        usage()

def addExpressionFormatline(my_array):
	my_list_of_formats = my_array
	for format_line in my_list_of_formats:
		if '##FORMAT=<ID=EXP, Number=.,Type=String,Description="expression profile, depending on configuration file">' in format_line:
			exit (-1)
	my_list_of_formats.append ('##FORMAT=<ID=EXP, Number=.,Type=String,Description="expression profile, depending on configuration file">')
	return my_list_of_formats


def printFORMATlines(array):
	for element in array:
		print element

configuration_file = open (sys.argv [1], "r")
expression_file = open (sys.argv [2], "r")
vcf_file = open (sys.argv [3], "r")

tissue_map = {}
guide_cards = []
configuration_map = {}
expression_gene_map = {}

format_list = []
last_line = ""

ratio = -1
UCSC_guide_card = -1
NM_guide_card = -1
tissue_card = -1
ratio_card = -1
ratio_card = -1
select_all_ratio = -1

for line in configuration_file:
	line = line.splitlines()[0]
	
	if line.startswith ('#'):
		guide_cards = line.split('\t')
		tissue_card = guide_cards.index('#TISSUES')
		ratio_card = guide_cards.index('RATIO')
		ratio_card2= guide_cards.index('RATIO2')  # mct - two ratios given for two experiments
		continue

	talkens = line.split ('\t')

	tissue1 = talkens [tissue_card].strip()
	tissue2 = tissue1 + ' 2'   # no, really.

	ratio = float(talkens [ratio_card].strip())
	ratio2= float(talkens[ratio_card2].splitlines()[0].strip())

	if tissue1 not in configuration_map:
		configuration_map[tissue1] = ratio
		configuration_map[tissue2] = ratio2
		

for line in expression_file:
	line= line.splitlines()[0]
	if line.startswith ('#'):
		guide_cards = line.split ('\t')
		HUGO_card = guide_cards.index ('#HUGO')
		UCSC_guide_card = guide_cards.index('UCSC_ID')
		NM_guide_card = guide_cards.index('NM_ID')
		tissue_card = guide_cards[4:]
		output_header = '#HUGO'+ '\t' + 'UCSC_ID'+ '\t' + 'NM_ID' + '\t' + "Ratio" + '\t' + 'Tissue'
		continue

	items = line.split('\t')
	HUGO_ID = items [HUGO_card]
	UCSC_ID = items [UCSC_guide_card]
	NM_ID = items [NM_guide_card]
	tissue_values = items [3:]

	counter_var = 3
	for value in tissue_values:
		tissue = guide_cards [counter_var].strip()
		wanted_tissue_ratio = configuration_map[tissue]
		counter_var = counter_var + 1


		if wanted_tissue_ratio >0 :
#			print >> sys.stderr, wanted_tissue_ratio
			if float(value) >= float(wanted_tissue_ratio):
				variant_expression_info = HUGO_ID + '\t' + UCSC_ID + '\t' + NM_ID + '\t' + value + '\t' + tissue
				expression_gene = NM_ID
				expression_tissue = tissue
				if expression_gene not in expression_gene_map:
					expression_gene_map[expression_gene]=NM_ID + '-'+ value + '-' + tissue


for line in vcf_file:
	line = line.splitlines()[0]
	if line.startswith ('#'):
		if line.startswith ('##') and not line.startswith ('##FORMAT'):
			print line
			continue

		if line.startswith ('##FORMAT'):
			format_list.append (line)
		else:
			if len (format_list)!=0:
				if last_line.startswith ('##FORMAT'):
					new_list_of_formats = addExpressionFormatline (format_list) 
					printFORMATlines(new_list_of_formats)
					format_list =[]
				else:
					print line

		if line.startswith ('#CHROM'):
			print line
			guide_cards = line.split ('\t')
			format_card = guide_cards.index ('FORMAT')
			for item in guide_cards:
				if item.isdigit():
					annotation_card=guide_cards.index (item)
		last_line = line
		continue

	talkens = line.split ('\t')
	format_vcf_line = talkens [format_card]
	format_vcf_items = format_vcf_line.split (':')
	
	try:
		NM_vcf_index = format_vcf_items.index ('NM')
	except ValueError:
		print >> sys.stderr, "VCF file does not contain NM ids, please run first"
		exit(-1)
		
	info_vcf_line = talkens [annotation_card]
	info_vcf_items = info_vcf_line.split (':')
	NM_vcf = info_vcf_items [NM_vcf_index]


	if 'EXP' not in format_vcf_items:
		format_vcf_items.append ('EXP')
		new_format_vcf_items = ":".join (format_vcf_items)
		talkens [format_card] = new_format_vcf_items
		line = '\t'.join (talkens)

		if NM_vcf in expression_gene_map:
			expression_values = expression_gene_map [NM_vcf]
			info_vcf_items.append (expression_values)
			new_info_vcf_items = ":".join (info_vcf_items)
			talkens [annotation_card] = new_info_vcf_items
			line = '\t'.join (talkens)
			print line







