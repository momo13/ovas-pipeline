#!/bin/bash

list_of_variants_to_match=$*
num_args=$#

wdir=../runs/working_dir/
stages=`find $wdir -type d | sort | grep "/family_" | grep -P "(/[0-9]{2}|final).*" | sed -r "s|(.*)/family_.*|\1|" | grep -v "rejects" | sort | uniq`
peeps=`find $wdir/00_input -type f | sed -r 's/.*\/(family_.*).*\.vcf/\1/'`

cases=`echo "$peeps" | grep cases | sort`
conts=`echo "$peeps" | grep controls | sort`

peers="$cases"
[ "$conts" != "" ] && peeps="$peeps
$conts"

#echo "$stages"
#exit 0

init=39
rest=15

function determineRestSpace(){
    colsize=`stty size | cut -d' ' -f 2`
    numpeeps=`echo "$peeps" | wc -l`

    #if [ "$list_of_variants_to_match" != "" ]; then
    #    numpeeps=$(( $numpeeps + $num_args ))
    #fi

    playable_space=$(( ( $colsize - $init ) - $numpeeps ))
    
    people_space=`echo " $playable_space/ $numpeeps " | bc`
    excess_space=`echo " $playable_space - ( $people_space * $numpeeps )" | bc`

    #echo " $colsize - ( $people_space * $numpeeps )"
    #echo "$colsize $numpeeps"
    #echo "$people_space  $excess_space"

    #set
    #echo "$init"
    init=$(( $init + $excess_space -1 ))
    rest=$people_space
    #echo "$init"
}

determineRestSpace


function fixed(){
    val="$1"
    strip="$2"
    space="$3"
   
    spacer="%${space}s"
    short=`echo "$val" | sed "s|$strip||"`
    short=`printf "$spacer" $short`

    spcn=`echo $space | sed 's/-//'`
    short=${short:0:$spcn}

    #sleep 1
    echo -en "${short}|"
}
    
# Header
if [ "$list_of_variants_to_match" != "" ]; then
    echo "Matching for [ "`echo $list_of_variants_to_match | sed 's/\s/,/g'`" ]"
fi

fixed "#" "#" $init
for perc in $peeps; do
    affect=`echo $perc | grep -oP "(cases|controls)" | sed 's/cases/KAY/' | sed 's/controls/CON/'`
    fixed $perc"[$affect]" "family_.*/" $rest
done
echo ""
fixed ========================================================================== "#" $init
for perc in $peeps; do
    fixed ================================================ "#" $rest
done
echo ""


#fixed "#" "#" $init

last_av=1
for stage in $stages; do    
    fixed $stage $wdir -$init

    case_num=0
    case_total=0
    
    for person in $peeps; do
        #echo "$person"
        file=`find $stage/ -type f | grep "${stage}/${person}" | grep -v "rejects"`
       
        if [ "$file" = "" ]; then
            fixed " " "#" $rest
        else
            numvars=`grep -cv "^#" $file`

            isCase=`echo $person | grep -oP "cases"`
            if [ "$isCase" != "" ]; then
                case_num=$(( $case_num + 1))
                case_total=$(( $case_total + $numvars ))
            fi

            if [ "$list_of_variants_to_match" != "" ]; then
                array=""
                for variant in $list_of_variants_to_match; do
                    num=$(grep -Pc "\s${variant}\s" $file)
                    array=$array",$num"
                done
                array=${array:1:100}
                array="["$array"]"
                numvars=$numvars"_$array"
                #echo $numvars
            fi
            fixed "$numvars" "#" $rest
        fi        
    done

    [ "$case_num" = "0" ] && echo "" && continue

    average=`echo "scale=2; $case_total / $case_num" | bc`
    reduct=`echo "scale=1; 100 - ( 100 * $average / $last_av )" | bc`
    (( `echo "$reduct < 1" | bc -l` )) && echo "" && last_av=$average && continue
    
    echo "$last_av --> $average   =   $reduct %"
    last_av=$average
    #echo ""
done
