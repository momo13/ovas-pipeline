#!/bin/bash

[ "$ih_model" = "0" ] &&\
	printline "No inheritance model selected, skipping..." "blue"


case $ih_model in
AR)
	runOptIf filterAR.sh $fopt_ar Y;;
AD) 
	runOptIf filterAD.sh $fopt_ad Y;;
XR)
	runOptIf filterXR.sh $fopt_xr Y;;
XD)
	runOptIf filterXD.sh $fopt_xd Y;;
MO)
	runOptIf mosaic.sh $fopt_mo Y;;
esac
