#!/bin/bash

logAndDo addgenes.sh $init_folder $core_genes
logAndDo addfuncs.sh $core_genes  $core_funcs
logAndDo addzygo.sh  $core_funcs $core_zygos 
