#!/bin/bash

# OPTS
#  Copy last processed into the 'last' folder of the opts
copyToLastOp $core_zygos


# Linkage needs some manual parsing
if [ "$opt_linkage" = "Y" ];then
    linkfile=$working_folder/linkage_region.bed
    echo "$opt_linkage_range" > $linkfile
    dos2unix $linkfile;
    runOpt filterlinkage.sh $fopt_linkage $linkfile;
    rm tmp;
fi

runOptIf filterdepth.sh $fopt_depth $opt_read_depth
runOptIf filtercall.sh $fopt_callqual $opt_call_qual
runOptIf filteraaf.sh $fopt_aaf $opt_aaf
runOptIf filtermut.sh $fopt_mut $opt_mutation
runOptIf filternovel.sh $fopt_nov $opt_novel_variants
