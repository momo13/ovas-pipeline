#!/usr/bin/env python2
import sys

def usage ():
	print >> sys.stderr, "usage: %s <vcf_file> [threshold]" % sys.argv[0].split('/')[-1]
	print >> sys.stderr, "   this script adds zygosity status to each variant. It looks at the base counts and determines what is homozygous (HOMOZY, defaut >65%), heterozygous(HETEROZY default <=65%))"
	exit ()

if len (sys.argv) < 2:
	usage ()


def handleFORMATlines(my_array):
	my_list_of_formats = my_array
	for format_line in my_list_of_formats:
		if '##FORMAT=<ID=ZYG, Number=.,Type=String,Description="Heterozygosity determined from AD">' in format_line:
			exit (-1)
	my_list_of_formats.append ('##FORMAT=<ID=ZYG, Number=.,Type=String,Description="Heterozygosity determined from AD">')
	return my_list_of_formats


def printFORMATlines(array):
	for element in array:
		print element

format_list = []
last_line = ""
vcf = open (sys.argv [1], 'r')

threshold = 0.75

if len (sys.argv) == 3:
	threshold = float (sys.argv [2])

for line in vcf:
	line=line.splitlines ()[0]
	if line.startswith ('#'):
		if line.startswith ('##') and not line.startswith ('##FORMAT'):
                        print line
			continue

		if line.startswith ('##FORMAT'):
			format_list.append (line)
		else:
			if len (format_list)!=0:
				if last_line.startswith ('##FORMAT'):
					new_list_of_formats = handleFORMATlines (format_list) 
					printFORMATlines(new_list_of_formats)
					format_list =[]
				else:
                                        print line

		if line.startswith ('#CHROM'):
			print line
			guide_cards=line.split ('\t')
			info_card = guide_cards.index ('INFO')
			format_card = guide_cards.index ('FORMAT')
			for item in guide_cards:
				if item.isdigit():
					annotation_card=guide_cards.index (item)
		last_line = line
		continue

	talkens = line.split ('\t')
	format_line = talkens [format_card]
	format_line_splitted = format_line.split (':')

	if 'ZYG' not in format_line_splitted:
		format_line_splitted.append ('ZYG')
		new_format_line_splitted = ":".join (format_line_splitted)
		talkens [format_card] = new_format_line_splitted
		line = "\t".join (talkens)

	if 'AD' in format_line_splitted:
		allele_depth_card = format_line_splitted.index ('AD')
		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		AD = annotation_line_splitted [allele_depth_card]
		each_AD = map(lambda x: float(x), AD.split (','))
		largest_allele = max (each_AD)
		
		DP = -1
		if 'DP' in format_line_splitted:
			read_depth = format_line_splitted.index ('DP')
			DP = float(annotation_line_splitted [read_depth])

		allele_count = largest_allele / DP
		if allele_count > threshold:
			allele_zygo_to_append = 'HOMOZY'
		if allele_count <= threshold:
			allele_zygo_to_append = 'HETEROZY'

		line = line + ':' + allele_zygo_to_append
                print line

