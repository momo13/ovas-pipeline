#!/bin/bash

[ $# != 2 ] && 	echo "`basename $0` <input_folder> <output folder>" && exit -1 
	
infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabAllVCFs $infold)

printline " Adding Zygosity " green

for vcf in $vcfs; do
	
	new_dir=$(setNewDir $vcf $infold $outfold)
	new_file_prefix=$new_dir/`basename $vcf .vcf`
	new_name=$new_file_prefix".zygo.vcf"
	
	fraction_hetlimit=`echo "scale=2;$core_zygo_hetlimit/100" | bc`
	
        #currFileProgress $vcf
        #[ -e $new_name ] && logAndDo echo "$(getNum $vcf) already processed" && continue
        
	logAndDo add_zygo.py $vcf $fraction_hetlimit > $new_name
	finishProgress $new_name $vcf

        # Controls are moved to FINAL folder
        isControl=`echo $vcf | grep "control"`

        #if [ "$isControl" != "" ];then
        final_folder_path=$(setNewDir $new_name $outfold $final_fold)
        cp $vcf $final_folder_path
#            echo "moved $vcf control to $final_fold"
        #fi
done

echo ""
