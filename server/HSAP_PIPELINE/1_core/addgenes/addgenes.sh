#!/bin/bash

if [ $# -lt 2 ]; then
	echo -e "
`basename $0` <input folder> <output folder>
	
 Performs genepender annotations on all files in .last/ folder
 Batch caller for genepender.
 
 Please initialise config before running."
	exit -1
fi

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(find $infold/ -type f -name "*.vcf")


printline "Adding gene annotations:" blue

[ "$bedtarget_intergenicInclude" = "Y" ] && opts="--keepall"
logAndDo genepender $dbmap  $vcfs $opts --output-folder=$outfold --prefix-extract=family_
#genepender test_inputs/hg19.splice_5.utr.genemap `find ./test_inputs/00_input/ -type f` --force --output-folder=test_output/hier/ --prefix-extract=family_

#for vcf in $vcfs; do
#	new_dir=$(setNewDir $vcf $infold $outfold)
#	new_file_prefix=$new_dir/`basename $vcf .vcf`
#	
#	new_file_annotations=$new_file_prefix".genes.vcf"
#	new_file_rejects=$new_file_prefix".genes.rejects.vcf"
##
#        echo ""
#	currFileProgress $vcf
#        echo ""
#	runGenepender $dbmap $vcf $new_file_rejects $new_file_annotations
#	finishProgress $new_file_annotations $vcf
#        echo "========"
#done
#
#echo ""
