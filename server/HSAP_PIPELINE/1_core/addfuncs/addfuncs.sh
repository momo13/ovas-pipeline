#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder>\n\tFunctionally annotates a batch of cases in the input dir" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold/"rejects"

# Only need to annotate the cases
vcfs=$(find $infold/ -type f -name "*.vcf" | grep -v "rejects" )
vcf_inputs=`echo $vcfs | sed 's|\s|\+|g'`;

printline " Adding Protein Function " green



if [ "$vcf_inputs" != "" ]; then
    # Determine if output files already exist by testing the first
    #echo $outfold
    if [ "`find $outfold -type f 2>/dev/null | wc -l`" != "0" ]; then
        already_processed=Y
        logAndDo echo "VCFs already functionally annotated"
    else
        logAndDo funcannot $vcf_inputs $dbmap $dnamap $fasta_dir --geneid=AL $outfold $outfold/"rejects"

        files2process=""
        for vcf in $vcfs; do

            new_dir=$(setNewDir $vcf $infold $outfold)
            new_file_prefix=$new_dir/`basename $vcf .vcf`

            temp_name=$outfold/$(getNum $vcf).genes.func.vcf
            finishProgress $temp_name $vcf

            [ -e $new_dir/$new_file_prefix ]
            ! [ -e $temp_name ] && echo "Couldn't find corresponding output for `basename $vcf`" && exit -1

            mv $temp_name $new_dir
            #echo "$temp_name --> $new_dir"
            
        done
        echo ""
    fi
else
    logAndDo echo "No vcfs to process"
fi




# Process as one FIRST, sort into directories later --> retain map

