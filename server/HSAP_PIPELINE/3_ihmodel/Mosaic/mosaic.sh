#!/bin/bash

# ih_model_m_upper="35";
# ih_model_m_lower="15";

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs Mosaicism filtering over all files" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Mosaicism, limit: $ih_model_m_lower - $ih_model_m_upper % " green

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".mosaic_"$ih_model_m_lower"_"$ih_model_m_upper".vcf"

    currFileProgress $vcf
    mosacisim.py $vcf $ih_model_m_upper $ih_model_m_lower > $new_name
    finishProgress $new_name $vcf
done

echo ""
