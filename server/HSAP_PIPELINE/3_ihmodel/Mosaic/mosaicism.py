#!/usr/bin/env python2
import sys

def usage ():
	print >> sys.stderr, 'usage: %s <vcf_file> [threshold_up] [threshold_down]' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this script lookes at each allele, if it is wihin the specified threshold (default 15-35%) it assignes a possible mosaicism status'
	exit(-1)

if len (sys.argv) < 2:
	usage ()

vcf = open (sys.argv [1], 'r')

format_list = []
last_line = ""

threshold_up = 0.35
threshold_down = 0.15

if len (sys.argv) == 4:
	threshold_up = float (sys.argv [2])
	threshold_down = float (sys.argv [3])


def printFORMATlines(array):
	for element in array:
		print element

def handleFORMATlines(my_array):
	my_list_of_formats = my_array
	for format_line in my_list_of_formats:
		if '##FORMAT=<ID=MOZ, Number=.,Type=String,Description="mozaicism determined from the % of minor allele within genotype">' in format_line:
			exit (-1)
	my_list_of_formats.append ('##FORMAT=<ID=MOZ, Number=.,Type=String,Description="mozaicism determined from the % of minor allele within genotype">')
	return my_list_of_formats

for line in vcf:
	line=line.splitlines ()[0]
	if line.startswith ('#'):
		if line.startswith ('##') and not line.startswith ('##FORMAT'):
			print line
			continue

		if line.startswith ('##FORMAT'):
			format_list.append (line)
		else:
			if len (format_list)!=0:
				if last_line.startswith ('##FORMAT'):
					new_list_of_formats = handleFORMATlines (format_list) 
					printFORMATlines(new_list_of_formats)
					format_list =[]
				else:
					print line

		if line.startswith ('#CHROM'):
			print line

			guide_cards=line.split ('\t')
			info_card = guide_cards.index ('INFO')
			format_card = guide_cards.index ('FORMAT')
			for item in guide_cards:
				if item.isdigit():
					annotation_card=guide_cards.index (item)
		last_line = line
		continue

	talkens = line.split ('\t')
	format_line = talkens [format_card]
	format_line_splitted = format_line.split (':')

	if 'MOZ' not in format_line_splitted:
		format_line_splitted.append ('MOZ')
		new_format_line_splitted = ":".join (format_line_splitted)
		talkens [format_card] = new_format_line_splitted
		line = "\t".join (talkens)

	if 'AD' in format_line_splitted:
		allele_depth_card = format_line_splitted.index ('AD')
		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		AD = annotation_line_splitted [allele_depth_card]
		AD_list = AD.split (',')


		if 'DP' in format_line_splitted:
			read_depth = format_line_splitted.index ('DP')
			DP = float(annotation_line_splitted [read_depth])

		for each_AD in AD_list:
			each_AD = float (each_AD)
			each_AD_count = float (each_AD / DP)

			if each_AD_count <= threshold_up and each_AD_count >= threshold_down:
				print line + ':' + "%.3f" % each_AD_count

