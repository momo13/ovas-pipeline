#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs Sex-Linked Dominant filtering" && exit -1


infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Sex-Linked Dominant " green

for vcf in $vcfs;do

	new_dir=$(setNewDir $vcf $infold $outfold)
	new_file_prefix=$new_dir/`basename $vcf .vcf`
	new_name_pref=$(addPrefix $new_file_prefix".SD" "temp_")
	
	cp $vcf $new_name_pref
	
	tmp_in=$new_name_pref
	tmp_out=$tmp_in
	
	currFileProgress $vcf
	
	id=$(getNum $vcf)
	
	##  -- Unaffected Parents
        parents=$(getParentIDs $id)

	for p_id in $parents; do
	
		[ $p_id = 0 ] && continue
		
		parent_vcf=$(getControlVCF $p_id)
		
		if [ "$parent_vcf" = "" ]; then
			echo "Warning: Could not find VCF for parent $p_id" >&2
			continue
		else
			printline "  filtering $id against parent $p_id " red
		fi
		
		# It is assumed that parents are unaffected
		tmp_in=$tmp_out
		tmp_out=$tmp_out".unaff_parent_"$p_id
		
		logAndDo sex-linked_dom_unaffected_control.py $tmp_in $parent_vcf > $tmp_out
	done

	## -- Unaffected Sibs
	usibs=`findFam.py $working_folder/pedfile.pro --siblings --unaffecteds $id | awk -F "-- " '{print $2}'`
		
	for us_id in $usibs; do
	
		usib_vcf=$(getControlVCF $us_id)
		
		if [ "$usib_vcf" = "" ]; then
			echo "Warning: Could not find VCF for parent $us_id" >&2
			continue
		else
			printline "  filtering $id against control sibling $us_id " red
		fi
		
		tmp_in=$tmp_out
		tmp_out=$tmp_out".unaff_sib_"$us_id
		logAndDo sex-linked_dom_unaffected_control.py $tmp_in $usib_vcf > $tmp_out
	done
	
	new_name=$(removePrefix $tmp_out "temp_")".vcf"
	cp $tmp_out $new_name
	
	finishProgress $new_name $vcf
done
