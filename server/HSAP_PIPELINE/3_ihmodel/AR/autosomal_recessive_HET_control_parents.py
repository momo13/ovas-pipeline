#!/usr/bin/env python2
# monika
import sys
import fileinput

def usage():
	print >> sys.stderr, 'usage: %s <VCF_of_affected> <[VCF_of_unaffected_parents]>\n' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this script deals with recessive scenario. It keeps all the varients of the affected individual which are if homozygous in affected then heterozygous in parents, if compound heterozygous in affected - then heterozygous in any of the parents'
	exit(-1)

if len (sys. argv) <3:
	usage()

vcf_affected = open (sys.argv [1], "r")

parents = sys.argv[2:]
parent_map = {}

chrom_card = -1
position_card = -1
format_card = -1
annotation_card = -1

both_parents = len(parents) > 1

for file in parents:
        vcf_parent = open(file,'r')

        vcf_parent.readline()

        for line in vcf_parent:
                new_line = line.strip()
                if new_line[0]=='#':
                        if new_line.startswith ('#CHROM'):
                                guide_cards = new_line.split ('\t')
                                for item in guide_cards:
                                        if item.isdigit():
                                                annotation_card=guide_cards.index (item)
                                chrom_card = guide_cards.index ('#CHROM')
                                position_card = guide_cards.index ('POS')
                                format_card = guide_cards.index ('FORMAT')
                        continue
 

                talkens = new_line.split ('\t')
                chr_pos = talkens [chrom_card]
                genome_position = talkens [position_card]
                format_line = talkens [format_card]
                format_line_splitted = format_line.split(':')
                zygo_card = format_line_splitted.index('ZYG')
                annotation_line = talkens [annotation_card]
                annotation_line_splitted = annotation_line.split (':')
                zygosity = annotation_line_splitted [zygo_card]

                first_key = chr_pos + '_'+ genome_position

                if first_key not in parent_map:
                        parent_map [first_key] = zygosity

        vcf_parent.close()



for line in vcf_affected:
	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
                chr_pos = talkens [chrom_card]
		genome_position = talkens [position_card]

		format_line = talkens [format_card]
		format_line_splitted = format_line.split (':')
		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		child_zygo = annotation_line_splitted [zygo_card]

		first_key_affected = chr_pos + '_'+ genome_position

                # for a variant in child and parent:
                #    child is homozygous, and parent is homozygous - reject(??)
                #    child is homozygous, and parent is heterozygous - keep
                #    child is heterozygous, and parent is homozygous - reject
                #    child is heterozygous, and parent is heterozygous - keep(??)
                #
		if first_key_affected in parent_map:
			parents_zygo = parent_map [first_key_affected]

                        if child_zygo =='HOMOZY' and parents_zygo == 'HETEROZY':
				print line
                                continue

                        if child_zygo == "HETEROZY" and parents_zygo == "HETEROZY": # compound het?
                                print line
                                continue

                if not (both_parents):
                        # If both parents do not have the mutation, it is definitely de novo
                        # but since we only have one, we cannot truly know, so we keep it in,
                        print line
                        continue
