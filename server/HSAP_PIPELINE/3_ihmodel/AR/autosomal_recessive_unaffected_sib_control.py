#!/usr/bin/env python2
import sys

def usage():
	print >> sys.stderr, 'usage: %s <VCF_of_affected> <VCF_of_unaffected_sibbling> ' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, '\n\tthis script deals with recessive scenario. It filters out variants at the GENE level using a sibling as control. From a unaffected sibling we expect a variant to be or in heterozygous state or do not exist in VCF file)\nNote, this script is can not be run before zygosity script'
	exit(-1)

if len (sys.argv) <3:
        usage()


vcf_affected = open (sys.argv [1], "r" )
vcf_sib = open (sys.argv [2], "r")


gene_map = {}    # genes/iso -> [ variant1, variant2 ]                            # affected
variant_map = {} # variant   -> {'zygosity':, 'genes':  [ intersecting, genes ] } # affected

chrom_card = -1
position_card = -1
format_card = -1
annotation_card = -1


def processLine(line):
        global chrom_card, position_card, format_card, annotation_card
        
        new_line = line.strip()
        if new_line[0]=='#':
	        if new_line.startswith ('#CHROM'):
                        guide_cards = new_line.split ('\t')
                        for item in guide_cards:
                                if item.isdigit():
                                        annotation_card=guide_cards.index (item)
                        chrom_card = guide_cards.index ('#CHROM')
                        position_card = guide_cards.index ('POS')
                        format_card = guide_cards.index ('FORMAT')
                return -1,-1,-1,-1, new_line
                
        talkens = new_line.split ('\t')
        chrom = talkens [chrom_card]
        pos = talkens [position_card]
        format_line = talkens [format_card]
        format_line_splitted = format_line.split (':')
        zygo_card = format_line_splitted.index ('ZYG')
        annotation_line = talkens [annotation_card]
        annotation_line_splitted = annotation_line.split (':')
        zygosity = annotation_line_splitted [zygo_card]

        # mct - 2017
        AL_index = format_line_splitted.index('AL')
        genelist = annotation_line_splitted[AL_index]

        return chrom, pos, genelist, zygosity, new_line



for line in vcf_affected:
        chrom, pos, genelist, zygosity, new_line = processLine(line)
        if chrom == -1:continue

        if chrom not in variant_map:
                variant_map[chrom] = {}

        if pos not in variant_map[chrom]:
                variant_map[chrom][pos] = {
                        'keep' : False,
                        'genes': {},
                        'affected' : zygosity,
                        'unaffected' : None
                }

        
        gene_and_isos = genelist.split(',')

        for gene in gene_and_isos:
                genenameonly = gene.split('|')[0]

                if chrom not in gene_map:
                        gene_map[chrom] = {}

                if genenameonly not in gene_map[chrom]:
                        gene_map[chrom][genenameonly] = { }

                gene_map[chrom][genenameonly][pos] = 1            # 1 - Affected, 2 - unaffected -- both = 1+ 2 = 3
                variant_map[chrom][pos]['genes'][genenameonly] = True
                
vcf_affected.close()


flag_deletion = {}

for line in vcf_sib:
        chrom, pos, genelist, zygosity, new_line = processLine(line)
        if chrom == -1:continue

        gene_and_isos = genelist.split(',')

        if chrom in variant_map:
                if pos in variant_map[chrom]:
                        variant_map[chrom][pos]['unaffected'] = zygosity

        for gene in gene_and_isos:
                genenameonly = gene.split('|')[0]

                # Only concerned with overlapping
                if chrom not in gene_map:
                        continue

                if genenameonly in gene_map[chrom]:
                        if pos in gene_map[chrom][genenameonly]:
                                gene_map[chrom][genenameonly][pos]  = 3


vcf_sib.close()


# Create list of genes where:
#    For each gene:
#         remove common variants between case and control
#         if a variant is leftover in the cases, keep the gene

#    case        control
#    ---          ---
#    ---          ---
#    ---           o
#    ---          ---

#import pdb
#pdb.set_trace()

for chrom in gene_map:
        for geneiso in gene_map[chrom]:

                keep_gene = False
                set_vals = list(set(gene_map[chrom][geneiso].values()))

                if len(set_vals) == 1:
                        val = set_vals[0]

                        if val == 1: # Case only gene, keep
                                keep_gene = True

                        elif val == 3: # Control has the exact same variants as Cases, ignore
                                keep_gene = False

                        else:
                                print >> sys.stderr, "DEBUG", chrom, geneiso
                                exit(-1)

                elif len(set_vals) == 2:
                        # Gene shared by case and controls
                        keep_gene = True

                else:
                        print >> sys.stderr, "DEBUG2", chrom, geneiso
                        exit(-1)
                        

                # Gene is being kept, keep all variants that bisect it
                if keep_gene:
                        #print >> sys.stderr, "Gene being kept=", geneiso
                        
                        for variant in variant_map[chrom]:
                                if geneiso in variant_map[chrom][variant]['genes']:
                                        variant_map[chrom][variant]['keep'] = True



# Reopen and process variant file

import pdb
#pdb.set_trace()

vcf_affected = open (sys.argv [1], "r" )

for line in vcf_affected:
        chrom, variant, genelist, zygosity, new_line = processLine(line)

        # print headers
        if chrom == -1:
                print new_line
                continue

        keep = variant_map[chrom][variant]['keep']
        
        #print >> sys.stderr, chrom, pos, keep,

        if keep:
                #pdb.set_trace()
                if variant_map[chrom][variant]['unaffected'] == None:
                        print new_line
                        continue

                unaf_zygo = variant_map[chrom][variant]['unaffected']

                # case is HET and cont is HET: keep
                # case is HET and cont is HOM: reject
                # case is HOM and cont is HET: keep
                # case is HOM and cont is HOM: reject
                #pdb.set_trace()
                #print >> sys.stderr, zygosity + " --- " +unaf_zygo

                if zygosity == "HETEROZY" and unaf_zygo == "HETEROZY":
                        print new_line
                        continue;

                if zygosity == "HETEROZY" and unaf_zygo == "HOMOZY":
                        print new_line
                        continue

                if zygosity == "HOMOZY" and unaf_zygo == "HETEROZY":
                        print new_line
                        continue

                if zygosity == "HOMOZY" and unaf_zygo == "HOMOZY":
                        continue

                # otherwise
                print >> sys.stderr, "errors encountered here", chrom, variant
        #print >> sys.stderr, ""
