#!/usr/bin/env python2

import sys
from os import makedirs

files=[]
outfold=None

debug=True

for argv in sys.argv[1:]:
    if argv.startswith("--"):
        if argv.startswith("--outfold="):
            outfold = argv.split("--outfold=")[1]
        else:
            print >> sys.stderr, "Unknown --", argv
            exit(-1)

    else:
        files.append( argv )



# The question we are asking:
# - For a given individual:
#   ->  do they have at least : 2 het, or 1 het + hom, or 1 hom variant for a given gene
# - If that gene exists (not neccesarily for the same variants) in all cases, keep the gene
def mapOutFile(file):
   
    gene_map = {} # chrom -> gene -> [ het:, hom: ]
    
    format_index = None
    sample_index = None

    with open(file,'r') as f:
        for line in f:
            line = line.splitlines()[0]
            if line[0] == '#':
                if line[0:4] == "#CHR":
                    tokes = line.split('\t')
                    format_index = tokes.index('FORMAT')
                    sample_index = format_index + 1

                continue

            tokens = line.split('\t')
            chrom = tokens[0]
            pos = int(tokens[1])

            sample_data = tokens[sample_index].split(':')
            form_split = tokens[format_index].split(':')

            gene_subindex = form_split.index('AL')
            zygo_subindex = form_split.index('ZYG')
            
            genes = sample_data[gene_subindex].split(',')
            zygo =  sample_data[zygo_subindex].strip()

            if chrom not in gene_map:
                gene_map[chrom] = {}

            for gen in genes:
                geneiso = gen.split('|')[0]

                if geneiso not in gene_map[chrom]:
                    gene_map[chrom][geneiso] = { 'HETEROZY': {}, 'HOMOZY': {} }

                gene_map[chrom][geneiso][zygo][pos] = True # unique

        f.close()



    # Keep only genes with a hom, or a hom and a het, or at least 2 hets
    print >> sys.stderr, " Genes",
    
    for chrom in gene_map:
        for geneiso in gene_map[chrom]:

            het_vars = gene_map[chrom][geneiso]['HETEROZY']
            hom_vars = gene_map[chrom][geneiso]['HOMOZY']
            keep = False

            if   len(hom_vars) >= 1:keep = True
            elif len(het_vars) >= 2:keep = True

            #if geneiso == "TMEM186":
            #    print >> sys.stderr, "SHOSHON", gene_map[chrom][geneiso]

            if keep:
                ren_het = gene_map[chrom][geneiso]['HETEROZY'].keys()
                ren_hom = gene_map[chrom][geneiso]['HOMOZY'].keys()
                
                #print >> sys.stderr, "%s [%d,%d]," % (geneiso, len(ren_het), len(ren_hom)),
                gene_map[chrom][geneiso]['keep'] = True
                
    print >> sys.stderr, sum([ len(gene_map[x]) for x in gene_map ]) , '-->',
    print >> sys.stderr, sum([ 1 for x in gene_map for y in gene_map[x] if 'keep' in gene_map[x][y] ])
    return gene_map




print >> sys.stderr, "Hom Chet Processing:"

common_gene_map = {}
indiv_gene_map = {}
use_gene_map = {}

for file in files:

    id = file.split('/')[-1].split('.')[0]
    print >> sys.stderr, '\t', id,

    gene_map = mapOutFile(file)
    indiv_gene_map[id] = gene_map
       
    for chrom in gene_map:
        for geneiso in gene_map[chrom]:

            if 'keep' in gene_map[chrom][geneiso]:
                if chrom not in common_gene_map:
                    common_gene_map[chrom] = {}
            
                if geneiso not in common_gene_map[chrom]:
                    common_gene_map[chrom][geneiso] = 0

                common_gene_map[chrom][geneiso] += 1

                if common_gene_map[chrom][geneiso] == len(files):
                    # This is the common gene map
                    if chrom not in use_gene_map:
                        use_gene_map[chrom] = {}

                    use_gene_map[chrom][geneiso] = True


print >> sys.stderr, "Overlapping genes:", sum([ len(use_gene_map[x]) for x in use_gene_map ])
#import pdb
#pdb.set_trace()


                    
# Reopen files and use just those genes in the common gene map
for file in files:       

    # Reopen file and kick variants not in that genes
    with open(file,'r') as f:
        
        suffix_extract = "family_" + file.split('/family_')[1]
        prefix = '/'.join(suffix_extract.split('/')[:-1])
        new_dir = outfold + '/' + prefix
        try:
            makedirs(new_dir)
        except OSError:
            pass

        basename = file.split('/')[-1].split('.vcf')[0]

        new_filename = new_dir + "/" + basename + ".twohet_hom.vcf"
        outf = open(new_filename, 'w')
        
        for line in f:
            line = line.splitlines()[0]
            if line[0] == '#':
                if line[0:4] == "#CHR":
                    tokes = line.split('\t')
                    format_index = tokes.index('FORMAT')
                    sample_index = format_index + 1

                print >> outf, line
                continue

            tokens = line.split('\t')
            chrom = tokens[0]
            pos = int(tokens[1])

            if chrom not in use_gene_map:
                continue

            sample_data = tokens[sample_index].split(':')
            form_split = tokens[format_index].split(':')

            gene_subindex = form_split.index('AL')
            zygo_subindex = form_split.index('ZYG')

            keep_variant = False
            genes = sample_data[gene_subindex].split(',')
            for gen in genes:
                geneiso = gen.split('|')[0]

                if geneiso in use_gene_map[chrom]:
                    keep_variant = True
                    break;

            if keep_variant:
                print >> outf, line
                
