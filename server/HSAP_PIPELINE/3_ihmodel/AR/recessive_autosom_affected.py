#!/usr/bin/env python2
import sys

def usage():
	print >> sys.stderr, 'usage: %s <VCF>' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this script deals with recessive scenario. It keeps all the variants which are homozygous, and compound heterozygous (at least 2 heterozygous variants within one gene)'
	exit (-1)

if len (sys. argv) < 2:
	usage ()

vcf = open (sys.argv [1], "r")
key_map = {}
new_key_map = {}

format_card = 'a'
annotation_card ='b'

for line in vcf:
	new_line=line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		format_card = guide_cards.index ('FORMAT')
		for item in guide_cards:
			if item.isdigit():
				annotation_card=guide_cards.index(item)

	if new_line.startswith ('chr'):
		talkens = new_line.split ('\t')
		format_line = talkens [format_card]
		format_talkens = format_line.split (':')
		gene_card = format_talkens.index ('AL')
		zyg_card = format_talkens.index ('ZYG')
		gene_line = talkens [annotation_card]
		gene_line_talkens = gene_line.split(':')
		gene_with_add_info = gene_line_talkens [gene_card]
		gene_separated = gene_with_add_info.split ('|')
		gene = gene_separated [0]
		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		zyg_status = annotation_line_splitted [zyg_card]
		key = gene + '_' + zyg_status
		if key.endswith ('HETEROZY'):
			if key not in key_map:
				key_map [key] = 0

			key_map [key] = key_map [key] + 1

vcf.close()

for k,v in key_map.iteritems():
	if v > 1:
		new_key = k
		if new_key not in new_key_map:
			new_key_map [new_key] = 1

vcf = open (sys.argv [1], "r")

for line in vcf:
	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		format_line = talkens [format_card]
		format_line_splitted = format_line.split (':')
		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		gene_card = format_line_splitted.index ('AL')
		zygo_card = format_line_splitted.index ('ZYG')

		gene_with_info = annotation_line_splitted [gene_card]
		gene_with_info_splitted = gene_with_info.split ('|')
		gene = gene_with_info_splitted [0]
		zygo = annotation_line_splitted [zygo_card]

		key_vcf = gene + '_' + zygo

		if key_vcf.endswith ('HOMOZY'):
			print line

		elif key_vcf.endswith ('HETEROZY'):
			if gene == 'INTERGENIC':
				print line
			elif key_vcf in new_key_map:
				print line






