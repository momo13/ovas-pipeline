#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs Autosomal Recessive filtering" && exit -1

info=""
info_ext=""

if [ $ih_model_ar_filter_parents = "Y" ]; then
    info="[Filter Parents] "
    info_ext="_fpar"
fi

if [ $ih_model_ar_filter_unaff_sib = "Y" ]; then
    info=$info"[Filter Unaff Sibs]"
    info_ext=$info_ext"_fusib"
fi


infold=$1
outfold=$2

suboutfold=$outfold

mkdir -p $suboutfold

vcfs=$(grabCaseVCFs $infold)


printline " Autosomal Recessive:  $info " green

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $suboutfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name_pref=$new_file_prefix".AR.$info_ext"
    
    currFileProgress $vcf
    
    # First filter -- HOM CHET and non-autosomes
    tmp_out=$(addPrefix $new_name_pref".1_hom_chet" "temp_")
    logAndDo recessive_autosom_affected.py $vcf | grep -vP "^(chr)?[XYMT][XYMT]?\s" > $tmp_out

    # Opt filters:
    id=$(getNum $vcf)
    
    ##  -- Parents
    if [ $ih_model_ar_filter_parents = "Y" ]; then
        parents=$(getParentIDs $id)

        tmp_in=$tmp_out
	tmp_out=$tmp_in".het_parents"`echo $parents | sed 's/\s/and/'`
        printline "  filtering $id against parent $parents " red

        parents_array=""
        for p_id in $parents; do
            pvcf=$(getControlVCF $p_id)
            [ "$pvcf" = "" ] && echo "$id: no data for parent $p_id"
            parents_array="$parents_array $pvcf"
        done

        parents_array=`echo $parents_array` #strip

        # It is assumed that parents are unaffected
        #echo "PARENTS_ARRAY=|$parents_array|"
        if [ "$parents_array" != "" ]; then
            logAndDo autosomal_recessive_HET_control_parents.py $tmp_in $parents_array > $tmp_out
        else
            # no parents, copy over input
            cp $tmp_in $tmp_out
        fi
    fi

    ## -- Control Siblings
    if [ $ih_model_ar_filter_unaff_sib = "Y" ]; then
        usibs=$(getSiblingUnaffs $id)
        
	for us_id in $usibs; do
	    
	    usib_vcf=$(getControlVCF $us_id)
	    
	    if [ "$usib_vcf" = "" ]; then
		echo "Warning: Could not find VCF for parent $us_id" >&2
		continue
	    else
		printline "  filtering $id against control sibling $us_id " red
	    fi
	    
	    tmp_in=$tmp_out
	    tmp_out=$tmp_in".unaff_sib_"$us_id
            logAndDo autosomal_recessive_unaffected_sib_control.py $tmp_in $usib_vcf > $tmp_out	    
	done
    fi
   
#    basename_vcf=$( echo `basename $tmp_out` | sed 's/temp//' )
    
    new_name=$(removePrefix $tmp_out "temp_")".vcf"
#    `dirname $tmp_out`/$basename_vcf".vcf"
    cp $tmp_out $new_name   

    finishProgress $new_name $vcf    
done


# Shake out any genes where there are not at least two het variants in all, or a hom
new_outfold=$outfold/
mkdir -p $new_outfold

cases=$(grabCaseVCFs $suboutfold)
logAndDo autosomal_recessive_2het_hom.py "$cases" --outfold=$new_outfold

# rename the case files just used back to tmp
for file in $cases; do
    mv $file `dirname $file`/temp_`basename $file`
done

