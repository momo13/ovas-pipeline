#!/usr/bin/env python2
import sys

def usage():
	print >> sys.stderr, 'usage: %s <VCF-affected> <VCF-unaffected>' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this script deals with autosomal dominant scenatio. It checks if a variant from affected individual does exists in unaffected individual and prints if it only if it does not'
	exit(-1)


if len (sys.argv) <3:
	usage()

vcf_affected = open (sys.argv [1], "r" )
vcf_unaffected = open (sys.argv [2], "r" )

unaffected_map = {}

for line in vcf_unaffected:
	new_line = line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		chrom_card = guide_cards.index ('#CHROM')
		position_card = guide_cards.index ('POS')

	if new_line.startswith ('chr'):
		talkens = new_line.split ('\t')
		chr_pos = talkens [chrom_card]
		genome_position = talkens [position_card]

		unaff_position_key = chr_pos + '_' + genome_position
		if unaff_position_key not in unaffected_map:
			unaffected_map [unaff_position_key] = 0



for line in vcf_affected:
	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		chr_pos_affect = talkens [chrom_card]
		genome_position_affect = talkens [position_card]

		affected_position_key = chr_pos_affect + '_' + genome_position_affect

		if affected_position_key not in unaffected_map:
			print line
