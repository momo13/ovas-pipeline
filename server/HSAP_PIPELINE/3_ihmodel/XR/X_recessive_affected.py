#!/usr/bin/env python2
import sys

def usage():
	print >> sys.stderr, 'usage: %s <VCF-affected>' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this script deals with X-linked scenario. It filters out variants that are heterozygous and out of X linked region'
	exit(-1)

if len (sys.argv) <2:
	usage()

vcf_affected = open (sys.argv [1], "r" )
vcf_map = {}

for line in vcf_affected:
	new_line = line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		chrom_card = guide_cards.index ('#CHROM')
		format_card = guide_cards.index ('FORMAT')
		for item in guide_cards:
			if item.isdigit():
				annotation_card=guide_cards.index (item)

	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		chrom = talkens [chrom_card]
		format_line = talkens [format_card]
		format_line_splitted = format_line.split (':')
		zygo_card = format_line_splitted.index ('ZYG')
		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		zygosity = annotation_line_splitted [zygo_card]

		if new_line.startswith ('chrX'):
			if zygosity == 'HOMOZY':
				print line
