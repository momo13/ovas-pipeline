#!/usr/bin/env python2
import sys

def usage():
	print >> sys.stderr, 'usage: %s <VCF-affected> <VCF-unaffected>' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this scripts deals with variants in sex-linked inheritance. It will keep the variants that are homozygous in affected individual but heterozygous or not present in unaffected'
	exit(-1)

if len (sys.argv) <3:
	usage()

vcf_affected = open (sys.argv [1], "r" )
vcf_unaffected = open (sys.argv [2], "r")

unaffected_key_map = {}

for line in vcf_unaffected:
	new_line = line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		for item in guide_cards:
			if item.isdigit():
				annotation_card=guide_cards.index (item)
		chrom_card = guide_cards.index ('#CHROM')
		position_card = guide_cards.index ('POS')
		format_card = guide_cards.index ('FORMAT')

	if new_line.startswith ('chr'):
		talkens = new_line.split ('\t')
		chr_pos = talkens [chrom_card]
		genome_position = talkens [position_card]
		format_line = talkens [format_card]
		format_line_splitted = format_line.split (':')
		zygo_card = format_line_splitted.index ('ZYG')
		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		zygosity = annotation_line_splitted [zygo_card]

		unaffected_key = chr_pos + '_'+ genome_position

		if unaffected_key not in unaffected_key_map:
			unaffected_key_map [unaffected_key] = zygosity

for line in vcf_affected:
	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		chr_pos_affect = talkens [chrom_card]
		genome_position_affect = talkens [position_card]
		format_line_affect = talkens [format_card]
		format_line_affect_splitted = format_line_affect.split (':')
		annotation_line_affect = talkens [annotation_card]
		annotation_line_affect_splitted = annotation_line_affect.split (':')
		zygosity_affect = annotation_line_affect_splitted [zygo_card]

		first_key_affected = chr_pos_affect + '_'+ genome_position_affect
		second_key_affected = chr_pos_affect + '_'+ genome_position_affect + '_' + zygosity

		if new_line.startswith ('chrX'):
			if second_key_affected.endswith ('HOMOZY'):
				if first_key_affected not in unaffected_key_map:
					print line
				if first_key_affected in unaffected_key_map:
					unaffected_zygosity = unaffected_key_map [first_key_affected]
					if unaffected_zygosity == "HETEROZY": 
						print line



