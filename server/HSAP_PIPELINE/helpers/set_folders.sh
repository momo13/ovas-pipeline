#
# Folder names and order of processes
#

[ "$working_folder" = "" ] && echo "Please set working folder" && exit -1

init_folder=$working_folder/00_input/ # do not change, server set (static_vars.php)
mkdir -p $init_folder

# Core
core_fold=$working_folder/01_core/

core_genes=$core_fold/a_appendGenes/
core_funcs=$core_fold/b_appendFunctionalAnnotations/
core_zygos=$core_fold/c_appendZygosity/

unset core_fold

# Opts
#
#  Here we define a "last" folder such that the inputs for each
#  module only depents on the folder to exist.
#  
#  The last folder is a duplicate of the last module processed.
#
#  We also define a opt_counter which is incremented each
#  time an optional module is run, and sets appropriate folder
#  prefixes to the folder names
#
export opt_last=$working_folder/"last_processed" # dont use "cases" or "controls" in name, these exist as subdirs
mkdir -p $opt_last
export opt_counter=1


## 2 _ opt
opt_fold=$working_folder #/2_opts/
fopt_linkage=$opt_fold/"opt_linkage"
fopt_depth=$opt_fold/"opt_readdepth"
fopt_callqual=$opt_fold/"opt_callqual"
fopt_aaf=$opt_fold/"opt_aaf"
fopt_mut=$opt_fold/"opt_muttype"
fopt_nov=$opt_fold/"opt_novel"


## 3 - Inheritance
ih_folder=$working_folder
fopt_ar=$ih_folder/"inh_AR"
fopt_ad=$ih_folder/"inh_AD"
fopt_xr=$ih_folder/"inh_XR"
fopt_xd=$ih_folder/"inh_XD"
fopt_mo=$ih_folder/"inh_MO"


## 4 _ commonality
common_fold=$working_folder #/"4_commonality"
fopt_comm=$common_fold/"commonality"


## 5 _ annotation
annot_fold=$working_folder #/5_annots/
fopt_gexp=$annot_fold/"annot_genexpr"
fopt_nmid=$annot_fold/"annot_nmid"
fopt_unip=$annot_fold/"annot_uniprot"

## 6 _ summary
fopt_merge=$working_folder/"merge"
summary_fold=$working_folder/"summary"



## Final folder:
# Where controls are used for comparison after annotation step
# and cases are placed at the end of the run
export final_fold=$working_folder/"final"
mkdir -p $final_fold

