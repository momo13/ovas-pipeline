# Globalise all executables

# Find where all executables lie
base_hsap=/nomansland/MAIN_REPOS/hsap-pipeline-web-ui/server/HSAP_PIPELINE/


pathfile=$base_hsap/path.delete_to_renew

if [ -e $pathfile ]; then
    export PATH=`cat $pathfile`
else
    binaries=`find $base_hsap/ -type f -exec sh -c "file -i '{}' | grep -q 'x-executable;'" \; -print`
    helper_scripts=`find $base_hsap/helpers/ -type f -name "*.py" -o -name "*.sh" | grep -v "dontuse"`
    module_scripts=`find $base_hsap/{1,2,3,4,5,6}* -type f -name "*.py" -o -name "*.sh" 2>/dev/null | grep -v "helpers"`

    # Loop over and extract their directory names (for PATH export)
    bin_folds_tmp="";

    for file in $binaries $helper_scripts $module_scripts; do
        chmod 755 $file; 	# xr -w
        file=`readlink -f $file`
        bin_folds_tmp="$bin_folds_tmp\n`dirname $file`";
    done;

    bin_folds=`echo -e "$bin_folds_tmp" | sort | uniq`;
    bin_folds=`echo $bin_folds | sed 's/\s/:/g'`

    export PATH=$PATH":$bin_folds";
    echo "$PATH" > $pathfile

    # Unset vars during sourcing
    unset binaries helper_scripts module_scripts bin_folds_tmp;
fi

unset pathfile
