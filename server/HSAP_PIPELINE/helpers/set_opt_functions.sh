#!/bin/bash

export logfile=$working_folder/"log.txt"
[ -e $logfile ] && mv $logfile $logfile.previous
echo `date` > $logfile

export loglevel=4

function logAndDo(){

    prev_buffer="`printf '%0*d' $(( $loglevel - 4 )) | sed 's/0/ /g'`"
    buffer="`printf '%0*d' $loglevel | sed 's/0/ /g'`"

    echo -en "$prev_buffer" >> $logfile
    
    echo "$*" | sed "s|\s|\\\\\n$buffer|g" >> $logfile
        
    # Run
    loglevel=$(( $loglevel + 4 ))
    $*
    [ $? = 255 ] && exit -1
    loglevel=$(( $loglevel - 4 ))
    echo "" >> $logfile
}

#
# copy family folders to last
#
function copyToLastOp(){
    rm -rf $opt_last
    mkdir -p $opt_last
    cp -r $1/fam* $opt_last
}


## runs each optional module, increments the optional
## counter to use as the filename and copies over the
## result to the last folder
##
function runOpt(){
    script=$1
    outfold=$2
    optarg=$3

    opt_counter=$(( $opt_counter + 1 ))  # increment
    outname=$(dirname $outfold)/`printf "%02d" $opt_counter`"_"`basename $outfold`

    # global opt_last set in set_folders.sh
    # Opts are fast, so no need to check if already run, just delete old folder and
    # start again
    rm -rf $outname
    logAndDo $script $opt_last $outname $optarg

    #[ $? = 0 ] && copyToLastOp $outname
    if [ $? = 0 ]; then
        updateAndReplaceCases $outname
        updateAndReplaceControls $outname
    fi
}


#
# runOpt if variable is not N, and optionally pass a second
# parameter if required.
#
function runOptIf(){
    script=$1
    outfold=$2
    nflag=$3
    opt="$4"

    if [ "$nflag" == "N" ] || [ "$nflag" == "0" ]; then
        echo ""
    else
	runOpt $script $outfold "$opt"
    fi
}


export -f runOptIf
export -f runOpt
export -f copyToLastOp
export -f logAndDo
