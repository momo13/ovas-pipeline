# Print functions
#
#
function printline {
    sentence=$1
    color=$2
    if [ "$color" != "" ];then
	echo "<div style='color:"$color";text-align:center;' >$sentence</div>"
    else
	echo $sentence
    fi
}


#
# infold -> case_ids
function grabCaseVCFs(){
    infold=$1
    find $infold/ -type f -name "*.vcf" | grep "cases" | grep -v "rejects" | grep -v "temp"
}

function grabControlVCFs(){
    infold=$1
    find $infold/ -type f -name "*.vcf" | grep "controls" | grep -v "rejects" | grep -v "temp"
}



function grabAllVCFs(){
    infold=$1
    find $infold/ -type f -name "*.vcf" | grep -v "rejects" | grep -v "temp"
}

# /path/to/32.vcf -> /path/to/temp_32.vcf
function addPrefix(){
    file=$1
    pref=$2
    echo `dirname $file`/"$pref`basename $file`"
}

# /path/to/something32.vcf -> /path/to/32.vcf
function removePrefix(){
    file=$1
    pref=$2
    echo `dirname $file`/`basename $file | sed "s/^$pref//"`
}


#
# Creates new folder for a file copying the existing directory structure
# relative to a specified root into a new destination 
#
function setNewDir {
    
    file=$1
    root_path=$2
    new_path=$3
    
    #	echo "file=$1" >&2
    #	echo "root_path=$2" >&2
    #	echo "new_path=$3" >&2
    
    relative_path=`echo $file | sed "s|$root_path||g"`
    relative_dir=`dirname $relative_path`
    new_dir=$new_path/$relative_dir
    
    #	echo "new_dir=$new_dir" >&2
    
    mkdir -p $new_dir
    echo $new_dir
}

#
# Same as above, but returns the filename at the new directory
#
function setNewFile {
    
    new_dir=$(setNewDir $1 $2 $3)
    echo $new_dir/`basename $1`
}


#
#
# /path/to/file/37_xydfas.vcf  --> 37
function getNum(){
    file=$1
    echo `basename $file .vcf` | grep -oP "^[0-9]+"
}

#
#
#
function getFam(){
    file=$1
    echo $file | grep -oP "family_[0-9]+"
}

#
#
# 37 ---> /path/to/file/37_xyz.vcf
function getVCF() {
    number=$1
    infolder=$2
    
    #	echo "number = $number"
    
    find $infolder -type f -name "$number.*.vcf" | grep -v "rejects"
}

# 37 --> /final_fold/fam/control/37_asd.vcf
function getControlVCF() {
    #	echo "1 = $1"
    getVCF $1 $final_fold
}


# 11 ->  111 112
function getParentIDs(){
    child_id=$1
    echo `findFam.py $working_folder/pedfile.pro --parents $child_id | awk -F "-- " '{print $2}'`
}

# 11 -> 12 13
function getSiblingUnaffs(){
    id=$1
    echo `findFam.py $working_folder/pedfile.pro --siblings --unaffecteds $id | awk -F "-- " '{print $2}'`
}



# /path/to/family_1234/cases/24.vcf -> family_1234/cases/24.vcf
function preserveFamilyDir(){
    vcf=$1
    echo "$vcf" | sed -r 's/.*family_(.*)/family_\1/'
}


# move controls to last_folder
function updateAndReplaceControls(){
    indir=$1

    c_vcfs=$(grabControlVCFs $indir)
    [ "$c_vcfs" = "" ] && echo "No Control VCFs found in $indir" >&2 && return
    updateAndReplaceVCFs $c_vcfs
}

# move cases to last_folder
function updateAndReplaceCases(){
    indir=$1
    
    c_vcfs=$(grabCaseVCFs $indir)
    [ "$c_vcfs" = "" ] && echo "No Case VCFs found in $indir" >&2 && return
    updateAndReplaceVCFs $c_vcfs
}

function updateAndReplaceAll(){
    indir=$1

    c_vcfs=$(grabAllVCFs $indir)
    [ "$c_vcfs" = "" ] && echo "No VCFs found in $indir" >&2 && return
    updateAndReplaceAll $c_vcfs
}


function updateAndReplaceVCFs(){
    c_vcfs=$*
    
    if [ "$opt_last" = "" ];then
        echo "unspecified last folder" >&2
        exit -1
    fi

    for vcf in $c_vcfs; do
        struct=$(preserveFamilyDir $vcf)
        prev_vcf=$opt_last/$(dirname $struct)/$(getNum $vcf).*.vcf

        if ! [ -e $prev_vcf ]; then
            echo "Warning: Previous VCF for $vcf does not exist in $opt_last folder" >&2
        else
            if [ $prev_vcf != $vcf ]; then
                rm $prev_vcf;
            else
                echo "src and dest are the same: $vcf" >&2
            fi
        fi
        
        new_vcf=$opt_last/$(preserveFamilyDir $vcf)
        mkdir `dirname $new_vcf` 2>/dev/null

        #echo "$prev_vcf replaced by $new_vcf"
        cp $vcf $new_vcf
    done
}





# Progress:
function currFileProgress(){
    vcf=$1
    echo -n "$(getFam $vcf) - $(getNum $vcf) "
}

function finishProgress() {
    #	[ $? != 0 ] && exit -1
    vcf=$1
    if [ "$vcf" = "" ]; then
	echo " x"
    else
	# Compare to old vcf
	vcf_old=$2
	
	fam=$(getFam $vcf)
	[ "$fam" = "" ] && fam=$(getFam $vcf_old)
	fullname="$fam, $(getNum $vcf)"
	
	if [ "$vcf_old" = "" ]; then
	    echo -e "\r$fullname:# (`numDataLines $vcf`)                                      "
	else
	    echo -e "\r$fullname:# (`numDataLines $vcf_old` --> `numDataLines $vcf`)          "
	fi
    fi
}


# File stats:

# Number of data lines
function numDataLines(){
    echo `printf "%'d" $(egrep -v "^#" $1 -c)`
}

# Exports only for subscripts
export -f setNewDir
export -f setNewFile
export -f numDataLines
export -f getNum
export -f getFam
export -f printline
export -f currFileProgress
export -f finishProgress
export -f getVCF
export -f getControlVCF
export -f grabCaseVCFs
export -f grabControlVCFs
export -f grabAllVCFs
export -f getParentIDs
export -f getSiblingUnaffs
export -f addPrefix
export -f removePrefix
export -f updateAndReplaceControls
export -f updateAndReplaceCases
export -f updateAndReplaceAll
export -f updateAndReplaceVCFs
export -f preserveFamilyDir
