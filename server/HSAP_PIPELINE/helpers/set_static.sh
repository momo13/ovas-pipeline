#
# Static data
#
# Depends: database version must be set before sourcing

static_dir=$(readlink -f `find ./ -type d -name "*static*"`)

export fasta_dir=$static_dir"/FASTA/$bedtarget_dbver/"
export dnamap=$static_dir"/dna_codon.table"
export gexprdata=$static_dir"/gene_expr_ratios.txt"
export uniprdata=$static_dir"/uniprot_HUMAN_ALL.dat"
