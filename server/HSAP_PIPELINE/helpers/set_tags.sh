#!/bin/sh

#
# Static VCF tags. Better not to touch.
#

#[Annotations IDs]
genelist_id="AL"
zygosity_id="ZYG"
type_id="TYP"
direction_id="DIL"
codon_id="COL"
protein_id="PRL"
mutation_id="MUL"
codonnom_id="CCH"
proteinnom_id="PCH"
