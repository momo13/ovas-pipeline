#
# Set by web:
#
db_opts=""     # overrided by 'ONLY' scenarios, appended to otherwise

# Parse splice args
splice_opts=""
if [ "$bedtarget_spliceInclude" = "Y" ]; then
    splice_opts="--splice "$bedtarget_spliceMargin
    
    if [ "$bedtarget_spliceOnly" = "Y" ]; then
	splice_opts="$splice_opts --splice-only"

	[ "$bedtarget_spliceOnlyNoDonor" = "Y" ] && splice_opts="$splice_opts --splice-no-donors"
	[ "$bedtarget_spliceOnlyNoAcceptor" = "Y" ] && splice_opts="$splice_opts --splice-no-acceptors"

	db_opts="$splice_opts"                 # override
    else
	db_opts="$db_opts $splice_opts"        # append
    fi    
fi
unset splice_opts

# Parse utr args
utr_opts=""
if [ "$bedtarget_spliceOnly" != "Y" ]; then
	if [ "$bedtarget_utrInclude" = "Y" ]; then
		utr_opts="--utr"
		
		if [ "$bedtarget_utrOnly" = "Y" ]; then
			utr_opts="$utr_opts --utr-only"	
			db_opts="$utr_opts"           # override
		else
			db_opts="$db_opts $utr_opts"  # append	
		fi
    fi
fi
unset utr_opts


## placeholder for PROMOTER variable
db_opts="$db_opts --promoters $bedtarget_promoterDS $bedtarget_promoterUS"

# Parse introns and intergenic
if [ "$bedtarget_utrOnly" != "Y" ] && [ "$bedtarget_spliceOnly" != "Y" ]; then
    [ "$bedtarget_intronsInclude" = "Y" ] && db_opts="$db_opts --introns"; #
    ##    
    [ "$bedtarget_intergenicInclude" = "Y" ] && db_opts="$db_opts --intergenic";
fi

# Let's make the directory name for the map now based on the args
mapname=`echo $bedtarget_dbver $db_opts | sed 's/\-\-/./g' | sed 's/\s\././g' | sed 's/\s/_/g'`

map_dir="$base_hsap/static_data/hgMaps/"
export dbmap=$map_dir/$bedtarget_dbver/$mapname".genemap"

unset mapname



if [ "$dbmap" = "" ] || [ ! -f "$dbmap" ];then
	printline " GeneMap not found, creating new one " red
	printline "Creating GeneMap $bedtarget_dbver"

	# It is assumed that the map will always include exons, scores, frames, direction, promoters, and introns.
	default_db_opts="-k -f -d -q"
	db_opts="$default_db_opts $db_opts"
	unset default_db_opts;

	makegenemaps.sh $bedtarget_dbver $dbmap "$db_opts"
	echo "makegenemaps.sh $bedtarget_dbver $dbmap $db_opts" >&2
else
	printline " GeneMap found `basename $dbmap` " blue
fi
