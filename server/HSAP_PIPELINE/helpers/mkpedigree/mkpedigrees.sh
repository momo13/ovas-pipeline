#!/bin/bash

if [ $4 = "" ] ; then
	echo "Usage: mkpedigrees.sh <pedfile.pro> <haplobin> <outdir> <doprocess>"
	echo
	exit -1
fi

pedfile=$1
haplobin=$2
outdir=$3
doprocess=$4

logfile=$outdir/"log.txt"

function log(){
    echo $* >> $logfile
}

function deletePedImages(){
    rm $oldfile $outdir/*.png  2>/dev/null
}

function deletePreviousRun(){
    rm -rf $outdir/../{0*,last*,pipeline.config*,summary*} 2>/dev/null
}

mkdir -p $outdir 2>/dev/null
log "`date`"
log "doprocess = $doprocess"

# Test to see if it's the same pedfile
oldfile=$pedfile".eski"

# Pedfile is zero if we're just pulling, so make sure
# that to only check if BOTH files are non-zero
if [ -s $pedfile ] && [ -s $oldfile ]; then

    if [ "`diff -b $pedfile $oldfile | wc -l`" != "0" ];then
        # Different file, remove oldfile and images
        deletePedImages
#        deletePreviousRun -- this should be okayed via the user first
        log "NOT SAME, REMOVE ALL"
    else
        log "ARE SAME, DO NOTHING"
    fi
fi

mkdir -p $outdir 2>/dev/null
! [ -e $outdir ] && log "No valid outdir given" && exit -1


# Make images
if [ "$doprocess" = "yes" ];then

    deletePedImages # delete last

    for i in `awk '{ print $1 }' $1 | uniq | sort -n`; do
        $haplobin -b -sortbygender 1 -pedfile $pedfile -family $i -resolution 100 -outfile $outdir/family_$i.png.untrimmed -outformat png 2>/dev/null >>$logfile
    done

    [ $? != 0 ] && exit

    #Convert to base64
    divstring=""

    for f in $outdir/*.png.untrimmed; do
        fam=`basename $f .png.untrimmed`
        outf=$outdir/"$fam.png"
#        log $outf
        convert $f -trim +repage $outf
    done
    rm $outdir/*.png.untrimmed
fi

# pull imagesred

#Convert to base64
divstring=""

pedigree_pngs=$(ls $outdir/*.png 2>/dev/null)

if [ "$pedigree_pngs" = "" ];then
    divstring="NONE"
else
    for outf in $outdir/*.png; do
        fam=`basename $outf .png`
        b64=$(base64 $outf 2>/dev/null)
        #        tmp="<input type='range' id=\"ped_"$fam"_slider\" />"
        tmp="<img src=\"data:image/png;base64,$b64\" id=\"ped_$fam\" alt=`basename $outf`>ZIZIZ"
        divstring=$divstring$tmp
    done
fi

echo $divstring

