#!/bin/bash

[ $# != 2 ] && 	echo "`basename $0` <input_folder> <output folder>" && exit -1 
	
infold=$1
outfold=$2

mkdir -p $outfold 2>/dev/null

#Move cases
vcfs=$(find $infold/ -type f -name "*.vcf" | grep "case" | grep -v "rejects")

printline " Finalizing Cases " green

for vcf in $vcfs; do
	
        final_folder_path=$(setNewDir $vcf $infold $outfold)
        cp $vcf $final_folder_path
#        echo "moved $vcf case to $outfold"
done

echo ""
