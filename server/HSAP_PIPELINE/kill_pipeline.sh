#!/bin/bash

root_pid=`ps aux | grep "bash -c ./general" | grep -v grep | awk '{print $2}'`

if [ "$root_pid" != "" ]; then
    tree_pids=`pstree $root_pid -p -a -l | cut -d, -f2 | awk '{print $1}'`
    echo "kill "$tree_pids;
fi
