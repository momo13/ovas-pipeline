#!/bin/bash


[ $# != 0 ] && echo "`basename $0` -- Runs HSAP, takes no arguments." && exit -1

. other/onpipeline_start.source
. helpers/load_environment_vars.source

echo ""

#exit


## ---- start ----
##  At this stage the files are in $init_folder

##
## CORE
##   Run mandatory mods
. auto/runcore.mod


##
## OPTS
##   Optional modules that run if checkbox flags are set
. auto/runopts.mod


##
## INHERITANCE
##   Autsomal/Sex-linked Recessive/Dominant
. auto/runih.mod


##
## COMMON
##    Filters at Variant and Gene Level
runOptIf filtercommon.sh $fopt_comm $opt_commonality


##
## ANNOTATION
##
. auto/runannot.mod


## Cases to Last Folder (must be before summary)
#helpers/move_cases_to_final.sh $opt_last $final_fold
runOpt truncate_controls.sh $fopt_merge

rm -rf $final_fold
cp -r $opt_last $final_fold


# Summary pulls from final
summary_maker.sh $final_fold $summary_fold

#rm -rf /tmp/last
#cp -r $opt_last /tmp/last

#rm -rf $opt_last
#mv tmp $opt_last

. other/onpipeline_complete.sh
