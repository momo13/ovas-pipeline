

class MergeSorter {

    constructor( tableID, cols_to_check, cols_to_merge ){
	this.table = document.getElementById( tableID );
	this.cols_to_check = cols_to_check;
	this.cols_to_merge = cols_to_merge;

        //Quick filter that all mergeable columns exist
        let num_cols = this.table.rows[1].cells.length;
        //console.log("before", this.cols_to_merge)
        this.cols_to_merge = this.cols_to_merge.filter( col => col < num_cols );
        //console.log("after", this.cols_to_merge)

	this.expanded_td = [];
	this.hidden_td = [];

	this.mergeAdjacentRows();
    }

    unhide(){
	this.expanded_td.forEach( td => td.rowSpan = 1 );
	this.hidden_td.forEach( td => td.style.display = "table-cell" );

	this.expanded_td = [];
	this.hidden_td = [];
    }

    __performMerge(row_body, i){
        // Go back and attach rowSpan to first, and make rest up to current search hidden
        for (let b=0; b < this.stretch_len; b++){
            let lrow = row_body[(i - this.stretch_len) + b];		    
            let mergeable_cells = this.cols_to_merge.map( col => lrow.cells[col] );
	    
            if (b === 0){
		mergeable_cells.forEach( cell => cell.rowSpan = this.stretch_len);
		mergeable_cells.forEach( cell => this.expanded_td.push( cell ) );
            } else {
		mergeable_cells.forEach( cell => cell.style.display = "none" );
		mergeable_cells.forEach( cell => this.hidden_td.push( cell ) );
            }
	}
	this.stretch_len = 0; //reset
    }


    mergeAdjacentRows(){
	
	if (this.expanded_td.length > 0){
	    this.unhide();
	}

	let last_key = "";   // key to match across adjacent rows
        this.stretch_len = 0  // current length of adjacent stretch
	
	let key_delim = ":"

        let row_body = this.table.tBodies[0].rows,
            num_rows = row_body.length;


        for (var i = 0, row; row = row_body[i]; i++)
	{
            let key = this.cols_to_check.map( col => row.cells[col].innerHTML ).join(key_delim);
            
            if (last_key === "" ){
		last_key = key
		continue
            }

            // Advance row
            this.stretch_len += 1
            
            // boundary met
            if (last_key !== key){
                this.__performMerge(row_body, i)
            }
	    last_key = key;
        }
        // If dangling search, merge to last
        if (this.stretch_len > 0) {
            this.stretch_len += 1
            this.__performMerge(row_body, num_rows)
        }
    }
}


function SortableTable(tableParentDiv, tableID, valNames, check_and_merges = [] )
{
    let lister = new List( tableParentDiv, { valueNames : valNames });

    let merge_array = [];
    for (let s=0; s < check_and_merges.length; s++)
    {
        let cols_to_check = check_and_merges[s][0];
        let cols_to_merge = check_and_merges[s][1];
        
        let merges = new MergeSorter( tableID, cols_to_check, cols_to_merge );
        merge_array.push( merges )
    } 
    
    function unhide(){
        merge_array.forEach( merges => merges.unhide() );       
    }
    function mergeSort(){
        merge_array.forEach( merges => merges.mergeAdjacentRows() );
    } 
    
    lister.on('sortStart'   , unhide);
    lister.on('searchStart' , unhide);
    lister.on('filterStart' , unhide);
    lister.on('sortStart'   , unhide);
    
    lister.on('sortComplete'   , mergeSort);
    lister.on('searchComplete' , mergeSort);
    lister.on('filterComplete' , mergeSort);
    lister.on('sortComplete'   , mergeSort);
}


var st = SortableTable('my-table-of-fun', 'genetable', 
	      ['chrom','pos','rsid','gene', 'mtype', 'changecDNA', 'mutetype' ],
	      [
                  [ [0,1], [0,1,2,7,8] ],
                  [   [3],         [3] ]
              ]
             );

