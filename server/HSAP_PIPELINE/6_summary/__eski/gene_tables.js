//Global Pointers
var tabla="", gcounter="", num_affs=-1, depth_box="", id_header = "", floater=null;

var butdiv = null,
    dispdiv = null,
    mutediv = null,
    exondiv = null,
    othrdiv = null;


var default_butt_color='#fff0ff',
    inactive_butt_color='#999999';

var button_dict = {},
    class_dict = {};

// -- Init --//
function initialise(){
  
    makeButtons();    

    depth_box.style = "width:30px;"
    depth_box.value = "0";
    
    tabla = document.getElementById("genetable");
    id_header = document.getElementById("genetable_header_table");
    floater = document.getElementById("floating_header");
    
    gcounter = document.getElementById("genecounter");
    depth_box = document.getElementById("depthabove_but");

    
    //Set default colours
    for (k in button_dict) setButtonColor(k)
    
    num_affs = tabla.rows[0].cells.length-2;
    checkBlanks_UpdateCommon();
    
    resizeHeaders();
    
    window.onscroll = moveHeaderWithScroll;


    setTimeout(function(){
        updateBlanks();
        moveHeaderWithScroll();
        showLoading(false);
    }, 1000);
}


window.onload = initialise;
//tabla.onload = function(){showLoading(false);}



// ------- Dynamics Resizing ------ //
var patient_columns = {},
    num_cases = 0;

function resizeHeaders(){
   
    //Find first unhidden row
    var num_row = tabla.rows.length;

    var n;
    for (n=0; n < num_row-1; n++){
	var row_style = tabla.rows[n].style.display;
	if (row_style !== "none"){
	    break;
	}
    }

    var first_unhidden_row = tabla.rows[n].cells;
    
    var header_row = id_header.getElementsByTagName("div");
    
    num_cases = 0;
    
    var f;
    for (f=0; f < first_unhidden_row.length; f++)
    {
	var isCase = true;
	if (header_row[f].className.indexOf('case')===-1)
	{
	    isCase = false;
	}
	else {
	    num_cases += 1;
	}
	
	patient_columns[f] = isCase;
	
	header_row[f].style.position = 'absolute';
	header_row[f].style.width = first_unhidden_row[f].offsetWidth;
	header_row[f].style.left =  first_unhidden_row[f].offsetLeft;
	
	// If controls are hidden, apply to header
	header_row[f].style.display = first_unhidden_row[f].style.display;
    }

    patient_columns[0] = null; // blank out gene column
    
     // blank out common mutations column (but only if it exists)
     // If only two columns, then it's just one file with no common
     // mutation column
    if (header_row.length > 2){
	patient_columns[f-1] = null;
    }
}

function moveHeaderWithScroll() {
    var scroll = window.pageYOffset;

    var table_top = tabla.offsetTop - 40;
    
    if (scroll <= table_top) {
        floater.style.top = table_top + 'px';
    } else {
        floater.style.top = window.pageYOffset;
    }
}


// ------- Hiding attributes for the nasil container object --------- //

//The following flags define which classes independently affect visibility of nasil
//If >1 are active in the object, then nasil is hidden. If ALL inactive, nasil is shown
var nasil_flags= { "syn":'synflag', "depth":'depthflag' } // extensible

function initFlag(object_ref){
    for (k in nasil_flags)
        if (typeof object_ref[nasil_flags[k]] === "undefined") 
            object_ref[nasil_flags[k]]=false;
}

function flag_toggle(object_ref,booler, flag_class){
    initFlag(object_ref)
    object_ref[nasil_flags[flag_class]] = booler
}
function valueFlag(object_ref,flag_class){
    initFlag(object_ref)
    return object_ref[nasil_flags[flag_class]];
}
//untested
function allflagsDown(object_ref){
    var all_false = true;
    for (k in nasil_flags){
        if(object_ref[nasil_flags[k]]){
            all_false = false;
            break;
        }
    }
    return all_false;
}

function flagDEPTHOn (object_ref){ flag_toggle(object_ref, true , "depth");}
function flagDEPTHOff(object_ref){ flag_toggle(object_ref, false, "depth");}


/** Switches display for specific class objects, or changes the 
 *  hide value of the nasil container object **/
function toggleClass(button_id, hideparent=false, checkblanks=false, checkpatients=true, subclass="")
{
    var booler = buttonPress(button_id),
	class_name = class_dict[button_id];

    var cells = tabla.getElementsByClassName(class_name)
    
    for (var s=0; s < cells.length; s++){
        var parent = cells[s];

        if (hideparent){
            while( !parent.className.startsWith("nasil") ) parent = parent.parentNode;
            flag_toggle(parent, !booler, class_name);
        } 
	parent.style.display = booler?"":"none";
    }
}



function toggleCont()
{
    var button_id = 'toggcont_but',
	hide_bool = buttonPress(button_id),
	class_name = class_dict[button_id];   
    
    var control_columns = []

    for (var key_col in patient_columns){
	if (patient_columns[key_col] === false){
	    control_columns.push( key_col )
	}
    }

    for (var r=0; r < tabla.rows.length; r++){
	var cells = tabla.rows[r].cells;
	for (var c=0; c < control_columns.length; c++)
	{
	    var cont_index = control_columns[c];    
	    cells[cont_index].style.display = hide_bool?"":"none";
	}
    }

    resizeHeaders();
    
}



// Filtering function upon read depth

function setDepth(){
    curr_depth = Number(depth_box.value);  //unary plus treats as numeric
    cells=tabla.getElementsByClassName("depth")

    for (var s=0; s < cells.length; s++)
    {
        var parent = cells[s],
            innervalue = Number(parent.innerHTML.trim());

        while( parent.className != "nasil" ){parent = parent.parentNode};

        if ( innervalue <= curr_depth ){
            parent.style.display = "none";
//            flagDEPTHOn(parent) //incrementHide(parent)
        }
        else {
            parent.style.display = "";
//            flagDEPTHOff(parent)
        }
    }
    checkBlanks_UpdateCommon()
}


/**  1. Updates row (tr element) visibility if all 'nasil' mutation containers are hidden
 *   2. Find common mutations across rows
 **/

function showLoading(show=true){
    document.getElementById('loading_screen').style.display = show?"":'none';
}


var common_rows = null;


var ElementHighlight = {

    highlightAllElementsOfClass: function ( class_r, show=true ){
        let class_items = document.getElementsByClassName( class_r );

        for (let ci=0; ci < class_items.length; ci++){
            class_items[ci].style.prev_background = class_items[ci].style.background || null;

            if (show){
                class_items[ci].style.background = 'red';
            }
            else {
                if (class_items[ci].style.prev_background != null){
                    class_items[ci].style.background = class_items[ci].style.prev_background;
               }
            }
        }        
    },

    getlistOfVisibleClassNameVariants: function(){
        return new Set(
            Array.prototype.slice.call(document.getElementsByClassName('nasil'))
                .filter( x => x.style.display != 'none' )
                .map( x => x.classList[1] )
        );
    },

    makeOnHoverListenersForClass: function(class_r, add=true){
        let class_items = document.getElementsByClassName( class_r );

        for (let ci=0; ci < class_items.length; ci++){
            if (add){
                class_items[ci].onhover = ElementHighlight.highlightAllElementsOfClass.bind(class_items[ci].onhover, class_r, true);
            }
            else {
                class_items[ci].onhover = undefined;
            }
        }        
    }
}

function checkBlanks_UpdateCommon()
{
    var num_genes=0;

    common_rows = [];
    showLoading(true);

    setTimeout(function(){
        showLoading(false);
    }, 3000);
    
    for (var y=0, row; row = tabla.rows[y]; y++)
    {      
        var cells = row.cells,
            mutie_map = {},
            num_blank_cells_in_row = 0;

	var entire_unhidden_row_is_blank = true;

        var gene = row.getElementsByClassName("genename")[0].innerHTML.trim()
        
        for (var x=1; x < cells.length -1; x++)
        {
	    var isCase = patient_columns[x];

	    if (!isCase){
		continue; //Ignore controls
	    }
	    
            var cell = cells[x];
            
            var mutations = cell.getElementsByClassName("nasil"),
                num_mutes_in_td = mutations.length,
                num_blanks = 0;
                       
            for (var s=0, mute; mute = mutations[s]; s++)
            {

                // Check if mutation is blank
                var isBlank = (mute.style.display.trim() === "none")
                if (isBlank){
                    num_blanks += 1

                    continue; //Ignore populating mutie map if it's blank for current filter
                }

                
                // Populate unique mutation map (to row)
                var mutie = mute.getElementsByClassName("mutie")[0],
                    mutie_data = mutie.innerHTML.trim(),
                    variant = mute.getElementsByClassName("varies")[0].innerHTML,
                    mutie_key = variant + "+++" + mutie_data;
                
                if (!(mutie_key in mutie_map)){
                    mutie_map[mutie_key] = 0;
                }
                mutie_map[mutie_key] += 1
            }

            // Entire cell blank?
            var entire_cell_is_blank = false;
            
            if (num_blanks === num_mutes_in_td){
                entire_cell_is_blank = true;
            }

	    if (!entire_cell_is_blank){
		entire_unhidden_row_is_blank = false;		
	    }
        }

        //Entire row blank?
        if (entire_unhidden_row_is_blank){
            row.style.display = "none"
        }
        else{
            row.style.display=""
            if (!gene.startsWith("Intergenic")){
                num_genes ++;
            }
        }

        // Update common mutations (if common col exists)
        //if (cells.length > 2){
         if (true){
       
            var common = row.getElementsByClassName("data common")[0] //empty
            var common_keys = [];

             for (var key in mutie_map){
                 if (mutie_map[key] === num_cases)
                 {
                     var_mut = key.split('+++');
                     common_keys.push(
                         "<div class='nasilC' ><span class='variesC'>"+
                             var_mut[0] +
                             "</span>&nbsp<span class='mutieC'>"+
                             var_mut[1] +
                             "</span></div>"
                     );
                 }
             }
             // Store row positions of genes with NOT common mutations for toggleCommon()
             if (common_keys.length === 0){
                 common_rows.push( y )
             }

             // Print common
             common.innerHTML = common_keys.join( "<br/>" );
	 }
    }
    setGeneCount(num_genes)

    if (button_dict["toggcom_but"] === false){
        button_dict["toggcom_but"] = true;
        setButtonColor("toggcom_but");

    }
    resizeHeaders();
}


function setGeneCount( amount ){
    gcounter.innerHTML = "&nbsp"+amount+" genes&nbsp";
}

function getGeneCount(){
    return Number(gcounter.innerText.trim().split(/\s+/)[0]);
}





// --- Sorting Table by Chromosome, GeneName -------------------------//
function sortTable(){
    var sort_chrom = buttonPress("toggsort_but")
    
    var store = [];
    var rows = tabla.rows
 
    for(var i=0, len=rows.length; i<len; i++)
    {
        var row = rows[i];
        var sortnr = null;
        
        if (sort_chrom){
	    sortnr = parseInt(row.cells[0].getElementsByClassName("chrom")[0].innerHTML.split("chr")[1].split(':')[0]);
	}
        else { // sort gene_name
            sortnr = row.cells[0].getElementsByClassName("genename")[0].innerHTML;
        }
        store.push([sortnr, row]);
    }
   
    if (sort_chrom){
        store.sort(function(x,y){
            return x - y;
        });
    } else store.sort()
    
    for(var i=1, len=store.length; i<len; i++){
        tabla.appendChild(store[i][1]);
    }
    store = null;
}







// ---- Button Functions ---------------------------------------------//
/** Logs button presses and handles colors **/
function buttonPress(button_id, setcolor=true)
{
    var booler = (button_dict[button_id] = !button_dict[button_id]);

    if (setcolor){
        setButtonColor(button_id);
    }
    return booler
}


function setButtonColor(button_id){
        document.getElementById(button_id).style.background = button_dict[button_id]?default_butt_color:inactive_butt_color;
}



function toggleMut(){ toggleClass("toggmute_but") };
function toggleTyp(){ toggleClass("toggtype_but") };
function toggleVari(){ toggleClass("toggvari_but") };

function toggleSyn(){ toggleClass("toggsyns_but", hideparent=true) };
function toggleMis(){ toggleClass("toggmis_but", hideparent=true) };
function toggleNon(){ toggleClass("toggnon_but", hideparent=true) };
function toggleUnk(){ toggleClass("toggunk_but", hideparent=true) };

function toggleUtr(){ toggleClass("toggutr_but", hideparent=true) };
function toggleUtr3(){ toggleClass("toggutr3_but", hideparent=true) };
function toggleUtr5(){ toggleClass("toggutr5_but", hideparent=true) };

function toggleSplice(){ toggleClass("toggspl_but", hideparent=true) };
function toggleSpliceD(){ toggleClass("toggsplD_but", hideparent=true) };
function toggleSpliceA(){ toggleClass("toggsplA_but", hideparent=true) };

function toggleExon(){ toggleClass("toggexon_but", hideparent=true) };
function toggleIntron(){ toggleClass("toggintron_but", hideparent=true) };
function toggleIntergenic(){ toggleClass("toggintergenic_but", hideparent=true) };

//var toggle_common = false;

function countGenes()
{
    var rows = tabla.rows;

    var gcount = 0;
    
    for(var i=0, len=rows.length; i<len; i++)
    {
        var row = rows[i];

        if (row.style.display.trim() === ""){
            //Unhidden
            gcount ++;
        }
    }
    return gcount;
}

function toggleCommon()
{

    var numg = getGeneCount();
    console.log("numg bef", numg);
    
    var toggle_common = buttonPress("toggcom_but");

    for (var c=0; c < common_rows.length; c++)
    {
        var r = common_rows[c],
            row = tabla.rows[r];

        row.style.display= toggle_common?"":"none";
    }

    numg = countGenes()   
    setGeneCount(numg);
    console.log("numg aft", numg);

}

function updateBlanks(){
    checkBlanks_UpdateCommon();
};



function setButt(id, classes, value, callback, on=true)
{
    var btn = document.getElementById(id);
    
    btn.value = value;
    btn.onclick = callback;

    // Add to Dicts
    class_dict[id] = classes;
    button_dict[id]= on;
}


function makeButtons()
{       
    butdiv  = document.getElementById("button_div")
    
    dispdiv = document.getElementById('display_filter_buttons');
    mutediv = document.getElementById('mutation_filter_buttons');
    exondiv = document.getElementById('exon_filter_buttons');
    othrdiv = document.getElementById('other_filter_buttons');

    setButt("toggmute_but", "mutie", "Nomencl.", toggleMut );
    setButt("toggtype_but", "types", "Detail", toggleTyp );
    setButt("toggvari_but", "varies", "Variant", toggleVari );
    setButt("toggcont_but", "control", "Controls", toggleCont );

    setButt("toggsyns_but", "syn", "Synonymous", toggleSyn );
    setButt("toggmis_but",  "mis" , "Missense", toggleMis );
    setButt("toggnon_but",  "non" , "Nonsense", toggleNon );
    setButt("toggunk_but",  "unk" ,"Unknown", toggleUnk );
    
    setButt("toggutr_but", "utr", "UTR    ", toggleUtr );
    setButt("toggutr3_but", "utr 3", "3'", toggleUtr3 );
    setButt("toggutr5_but", "utr 5", "5'", toggleUtr5 );
    
    setButt("toggspl_but", "splice",    "Splice    ", toggleSplice );
    setButt("toggsplD_but","splice D",  "Don", toggleSpliceD );
    setButt("toggsplA_but","splice A",  "Acc", toggleSpliceA );

    setButt("toggexon_but", "exon", "Exon", toggleExon );
    setButt("toggintron_but", "intron", "Intron", toggleIntron );
    setButt("toggintergenic_but", "intergenic", "Intergenic", toggleIntergenic);

    setButt("toggblanks_but", "blanks", "Update Table", updateBlanks, false );
    button_dict["toggblanks_but"] = true;
    
    setButt("toggsort_but",   "IGNORE", "Sort Chrom", sortTable, false );
    setButt("toggcom_but",   "IGNORE", "Common Only", toggleCommon );
}
