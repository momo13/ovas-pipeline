#!/usr/bin/env python2

import sys
from VCF_Operations import VCFOps
from time import ctime, sleep

version="2017_05_15"


if len(sys.argv)<2:
    print >> sys.stderr, '''
v%s
Takes in multiple functionally annotated VCF files and prints an HTML table showing the mutations that occured for each gene for each patient

    usage: %s <genelist> [</case/ VCFs>] [</control/ VCFs>]


''' % (version, sys.argv[0].split('/')[-1])
    exit(-1)


g_id="AL"
m_id="MUL"
c_id="CCH"
p_id="PCH"
z_id="ZYG"
#pa_id = "PA"
dp_id = "DP"

genelist=sys.argv[1]

files=sys.argv[2:]

class GeneContainer: # chrom implicit by key, as is GeneName
    def __init__(self, nmid, pos1, pos2):
        self.nmid = nmid
        self.pos1 = pos1
        self.pos2 = pos2
#        self.exon_data = {}  # Exonnumber --> [actual,splice,utr]


########################################################################
print >> sys.stderr, "Making Gene Map:",
gene_positions={} # chrom -> {}

numlines = VCFOps.countFileLines(genelist)
linecount=0

f = open(genelist,'r')
f.readline()
#chr1    69090    69090    OR4F5|Exon1_SpliceD    cC    +    NM_123123
for line in f:
    #   print >> sys.stderr, line
    linecount += 1

    tokens = line.split('\t')
    tokens[-1] = tokens[-1].splitlines()[0]

    #   print >> sys.stderr, tokens
    #    exit(-1)
    chrom = tokens[0].strip()
    pos1 = int(tokens[1])
    pos2 = int(tokens[2])
    
    gene,exon_info = map( lambda x: x.strip(), tokens[3].split('|'))
    gene = gene.strip()
    
    nmid = tokens[6].strip()

    # Chrom Specific
    if chrom not in gene_positions:
        gene_positions[chrom] = {}
    
    # Map into Genes
    if gene not in gene_positions[chrom]:
        gene_positions[chrom][gene] = GeneContainer(nmid, pos1, pos2)
        
    #gc = gene_positions[chrom][gene] # grab gene container
    
    # Update limits
    gene_positions[chrom][gene].pos1 = pos1 if (pos1 < gene_positions[chrom][gene].pos1 ) else gene_positions[chrom][gene].pos1
    gene_positions[chrom][gene].pos2 = pos2 if (pos2 > gene_positions[chrom][gene].pos2 ) else gene_positions[chrom][gene].pos2


    if linecount%90==0:
        print >> sys.stderr, "\r\t\t", (100*linecount)/numlines,'%',

print >> sys.stderr, "\r\t\t100 %     "

debug = False
if debug:
    test="chr1"
    test_gene="UBE2Q1"
    print >> sys.stderr, "%30s%30s%10d%10d" % (
        test_gene,
        gene_positions[test][test_gene].nmid,
        gene_positions[test][test_gene].pos1, 
        gene_positions[test][test_gene].pos2
    )
    exit(-1)


########################################################################
print >> sys.stderr, "Making Mutation Map:",
########################################################################
def getType(codon_change):
    if (codon_change.find("ins")!=-1):
        return "ins"
    if (codon_change.find("del")!=-1):
        return "del"
    return "snv"

numtabs=2;
gene_map={}      # Populated across all files
                 # map -> Gene -> File -> List of codon and protein changes

intergenic_map={}

html_map = {} # Gene -> [File -> [ (c,p,snv,mis,het,exon_no,exon_data) ] ]

arg_ids = [ dp_id , g_id, m_id, c_id, p_id, z_id ] # pa_id]

for file in files:
    
    file=file.strip()
    prefix=file.split('/')[-1].split('_')[0].split('.')[0]

    isControl = file.find("controls")!=-1
    
    vcf = VCFOps(file)
    vcf.getFieldIndexes(arg_ids)
    
    f = open(file,'r')
    
    countlines=0
    
    for line in f:
        countlines += 1
        
        if line[0]=='#':
            continue
        
        line = line.splitlines()[0]
        
        tokens = line.split('\t')
        chrom = tokens[0].strip()
        variant = tokens[1].strip()
        rsid    = tokens[2].strip()
        
        func_data = tokens[vcf.IFORMAT_INDEX].split(':')
        data_map = vcf.getElementsFromData(func_data, check=arg_ids);      

        zygosity = func_data[vcf.field_map[z_id]]
        read_depth = int(func_data[vcf.field_map[dp_id]])

        # Control means all the cases have already been processed
        #if isControl and (chrom not in html_map):
        #    print >> sys.stderr, "\r", prefix, "is control, skipping chrom", chrom,
        #    continue

        
        for index in xrange(len(data_map[g_id])):
#        for index in xrange(100):

            try:
                #Gene|Exon1 Gene|Intron
                gene, exon_data = data_map[g_id][index].split('|')
                #if isControl and (gene not in html_map[chrom]):
                #    print >> sys.stderr, "\r", prefix, "is control, on chrom", chrom, "skipping gene", gene,
                #    continue
                
            except ValueError:
                # Intergenic
                gene = data_map[g_id][index].strip()
                                
                if chrom not in intergenic_map:
                    intergenic_map[chrom] = {}
                    
                if prefix not in intergenic_map[chrom]:
                    intergenic_map[chrom][prefix] = {}

                intergenic_map[chrom][prefix][variant] = (rsid, read_depth,zygosity)
                continue
                    
                
            exon_info = exon_data.split('_')
            try:
                exon_no = int(exon_info[0].split("Exon")[1])
            except IndexError:
                if exon_info[0] == "Intron":
                    exon_no = -1
                else:
                    #print >> sys.stderr, exon_info, gene
                    # promoter
                    exon_no = -3
                
            exon_d = ""
            if len(exon_info)==2:
                exon_d = exon_info[-1].strip()

            # Sanity checks
            if gene not in gene_positions[chrom]:
                if gene != "Intergenic":
                    print >> sys.stderr, "This cannot be...", gene
                    exit(-1)
            
            # Init populate HTML map
            if chrom not in html_map:
                html_map[chrom] = {}
            
            if gene not in html_map[chrom]:
                html_map[chrom][gene] = {}
            
            if prefix not in html_map[chrom][gene]:
                html_map[chrom][gene][prefix] = {}  # exon key is purely for ordering

            if exon_no not in html_map[chrom][gene][prefix]:
                html_map[chrom][gene][prefix][exon_no] = []
            
            mutation = data_map[m_id][index]
            codon_change = data_map[c_id][index]
            protein_change = data_map[p_id][index]
            
            #uniq = (   (c. 14A>G, p. 3C>R) , ( snv, MIS, HET, 40 ), exon_d )
            uniq = (
                rsid,
                (codon_change, protein_change),
                (getType(codon_change), mutation[0:3], zygosity, read_depth, variant ),
                exon_d
            )
            
            html_map[chrom][gene][prefix][exon_no].append(uniq)
            

        if (countlines%21==0):
            print >> sys.stderr, '\r', '\t'*numtabs, prefix,':', int(100*countlines/vcf.numlines), '%',

    print >> sys.stderr, '\r', '\t'*numtabs, "%s:100%%   " % prefix,
    f.close()
    numtabs += 1


#import pdb
#pdb.set_trace()



########################################################################
print >> sys.stderr, "\nPrinting HTML:",
########################################################################

#Subsequent row spacers
elm_sep=" "  #tab;
mut_sep="<br/>"

num_affecteds = 0

genecount=0;                        # progress
chromkeys=sorted(gene_map.keys());
numchroms = len(chromkeys)     # num chroms


def generateIntergenicTable(intergenic_map, files):
    global elm_sep
    global mut_sep
    # Finished processing genes + exons
    # Now process intergenic variants in each chrom
    for chrom in intergenic_map:
        outline = "<tr id=\" Intergenic " + chrom +" \" >"
        outline += "<td class='gene' ><span class='genename'> Intergenic "+chrom+"</span><br /></td>"

        blank_cases = 0

        for file in files:
            fileid = file.split('/')[-1].split('.')[0]

            affectation = 'cases' in file.split('/')
            outline += "<td class=\"data\" >"

            # File doesn't have any intergenic variants in that chrom
            if fileid not in intergenic_map[chrom]:
                outline += "</td>"
                if affectation:
                    blank_cases += 1
                continue

            # File has at least one variant
            #import pdb
            #pdb.set_trace()

            for variant in intergenic_map[chrom][fileid]:
                rsid, depth, zyg = intergenic_map[chrom][fileid][variant]

                outline += "<div class=\"nasil "+ chrom+'_'+variant  + "\" >"
                outline += "<span class=\"varies\" >"+variant+"</span>"
                outline += "<span class=\"rsid\" >"+rsid + "</span>"
                outline += "<span class=\"mutie\" > </span>"
                outline += "<br />"
                outline += ("<span class=\"types\" >"+
                            "<span class=\"intergenic\" >"+zyg+"</span>"+
                            "<span class=\"depth\">"+str(depth)+"</span>"+
                            "</span>")

                outline += mut_sep
                outline += "</div>"
            outline += "</td>"

        if len(files)>1:
            outline += "<td class=\"data common\" align=center ></td>"
        else:
            outline += "<td class=\"data common\" style='display:none;' align=center ></td>"

        outline += "</tr>"
        #import pdb
        #pdb.set_trace()
        if blank_cases != num_affecteds:
            print outline





def generateCodingTable(html_map, files):
    global elm_sep
    global mut_sep
    html_keys = sorted(html_map.keys())

    for chrom in html_keys:  
        for gene in html_map[chrom]:

            blank_cases = 0
            outline = ""
            
            outline += "<tr id=\" " + gene +" \" >"
            outline += "<td class='gene' ><span class=\"genename\"> "+gene+"</span><br />"

            g_data = gene_positions[chrom][gene]
            gene_pos= chrom+":<br />"+str(g_data.pos1)+"<br />-"+str(g_data.pos2)

            outline += "<span class=\"chrom\" >"+gene_pos+"</span></td>"
            
            for file in files:
                fileid = file.split('/')[-1].split('.')[0]
                
                affectation = 'cases' in file.split('/')
                outline += "<td class=\"data\" >"
                
                # File doesn't have that gene
                if fileid not in html_map[chrom][gene]:
                    outline += "</td>"
                    if affectation:
                        blank_cases += 1
                    continue
                
                for exon_no in html_map[chrom][gene][fileid]:
                    supertype_class = "exon"
                    exon_info = "Exon"+str(exon_no)
                    if exon_no == -1:
                        supertype_class = "intron"
                        exon_info = "Intron"
                        #elif exon_no == -2:
                        #    supertype_class = "intergenic"
                        #    exon_info = "Intergenic"

                    for uniq in html_map[chrom][gene][fileid][exon_no]:                   
                        rsid, cp, detail, exon_data = uniq
                        c,p = cp
                        snv,typ,zyg,depth,variant = detail        
                        #                    print >> sys.stderr, typ

                        nomen = c + elm_sep + p                   
                        next_space_index = nomen.index(' ');

                        # if nomenclature is long, split at every 20 characters
                        maxlen=18
                        if next_space_index>maxlen:
                            tmp_nom=""
                            while len(nomen)>maxlen:
                                tmp_nom += nomen[0:maxlen]+" "
                                nomen = nomen[maxlen:]
                                nomen = tmp_nom;
                                
                        misc_classes = "unk "
                        if typ=="SYN":
                            misc_classes = "syn"+" "
                        elif typ=="MIS":
                            misc_classes = "mis"+" "
                        elif typ=="NON":
                            misc_classes = "non"+" "
                            
                        superexon_class="" # UTR, SPLICE
                        subexon_class="" # 5' 3', Donor Acceptor
                        
                        if exon_data.find("UTR")!=-1:
                            superexon_class = "utr"
                            subexon_class = exon_data.split("'UTR")[0].strip()    # 5 3
                            
                        elif exon_data.find("Splice")!=-1:
                            superexon_class = "splice"
                            subexon_class = exon_data.split("Splice")[-1].strip() # D A
                            
                        misc_classes +=" "+superexon_class+" "+subexon_class+" "+supertype_class
                        
                        misc_super = ""
                        if len(misc_classes)>3:
                            misc_super = "class=\""+misc_classes+"\""
                            outline += "<div class=\"nasil " + chrom+"_"+variant  + "\" >"
                            outline += "<span class=\"varies\" >"+variant+"</span>"
                            outline += "<span class=\"rsid\" >"+rsid + "</span>"
                            outline += "<span class=\"mutie\" >"+nomen+"</span>"
                            outline += "<br />"
                            outline += ("<span class=\"types\" >"+
                                        "<span "+misc_super+" >"+
                                        snv + elm_sep + typ + elm_sep + zyg + elm_sep + exon_info + elm_sep+ exon_data + elm_sep+
                                        "</span>"+
                                        "<span class=\"depth\">"+
                                        str(depth)+
                                        "</span>"+
                                        "</span>")
                            outline += mut_sep
                            outline += "</div>"           
                outline += "</td>"
                            
            if len(files)>1:outline += "<td class=\"data common\" align=center ></td>"
            else:outline += "<td class=\"data common\" style='display:none;' align=center ></td>"
            outline += "</tr>"

            if blank_cases != num_affecteds:
                print outline










def printFooter():
    global version
    #Disclaimer
    print "<span style=\"font-size:0.6em\">"
    print "(<i>GeneTables_v"+version+" - OVAS Pipeline</i>)<br/>" # (<i>Nephrology, Royal Free Hospital</i>)<br/>"
    #print "<b>  Authors: </b>Horia Stanescu, Mehmet Tekman, Monika Mozere @ <i>University College London</i><br/>"
    print "<b>Generated: </b> "+ctime()+"<br/><br/>"
    print "note: Gene positions are derived directly from the gene map, which may be configured to print coding regions only."
    print "</span>"
    
    print "<br />"*3
    print "</body>"
    print "</html>"


def printHeader():
    #HTML HEADERs
    print '''
<meta charset="UTF-8">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">'''

    print '''<!-- \nThe following is autogenerated by gene_tables_HTML.py\nc. Mehmet Tekman @ Royal Free Hospital, Nephrology\n -->'''
    print "<head>"
    print "<link rel='stylesheet' type='text/css' href='./gene_tables.css' />"
    print "<script src='./gene_tables.js' ></script>"
    print "</head>"

    print "<body>"

    # Loading screen
    print "<div id='loading_screen' >Loading ...</div>";
    print "<span style=\"font-size:0.6em\">Toggle data using buttons below</span>"
    print "<div id=\"genecounter\"></div>"

    # Button placement
    print '''<div class='buttons' id='button_div' >
  <div id='display_filter_buttons' class='groups' >  <!-- Nomenc. Detail -->       
    Display Options:<br/>
    <input type='button' id='toggmute_but' />
    <input type='button' id='toggtype_but' />
    <input type='button' id='toggvari_but' />
    <input type='button' id='toggcont_but' />
  </div>
  <div id='mutation_filter_buttons' class='groups' > <!-- MIS SYN NON -->
    Mutation Filters:<br/>
    <input type='button' id='toggsyns_but' />
    <input type='button' id='toggmis_but' />
    <input type='button' id='toggnon_but' />
    <input type='button' id='toggunk_but' />
  </div>
  <div id='exon_filter_buttons' class='groups' >     <!-- Splice DA UTR 35 -->
    Functional Filtering:<br/>
    <div class='bigbutton' id='toggutr_but_encaps' >
       <div id='toggutr_but' class='bigbutter' >
         <p>UTR</p>
       </div>
       <div class='bigbutton_sub'>
           <input type='button' id='toggutr3_but' />
           <input type='button' id='toggutr5_but' />
       </div>
    </div>
    <div class='bigbutton' id='toggspl_but_encaps' >
       <div id='toggspl_but' class='bigbutter' >
         <p>Splice</p>
       </div>
       <div class='bigbutton_sub'>
           <input type='button' id='toggsplD_but' />
           <input type='button' id='toggsplA_but' />
       </div>
    </div>
  </div>
  <div id='other_filter_buttons' class='groups' >     <!-- Depth Unknown Common --> 
    Other Options:<br/>
    <input type='button' id='toggblanks_but' />
    <input type='button' id='toggsort_but' />
    <input type='button' id='toggcom_but' />
    Read Depth: <input type='number' id='depthabove_but' value=0 min=0 onchange='setDepth()' />
  </div>
    <div id='varianttype_filter_buttons' class='groups' >     <!-- Exon Intron Intergenic -->
    Variant Type:<br/>
    <input type='button' id='toggexon_but' />
    <input type='button' id='toggintron_but' />
    <input type='button' id='toggintergenic_but' />
   </div>
</div>'''


def printFloatingHeader(files):
    global num_affecteds

    print '''<div id='floating_header' >'''

    #First row
    print "<div id=\"genetable_header_table\">"
    print "<div></div>"



    # patient ID
    for file in files:
        fileid = file.split('/')[-1].split('.')[0]
        affectation = 'cases' in file.split('/')
    
        class_field="data"

        if affectation:
            class_field += " case"
            viewable = "[case]"
            num_affecteds += 1
        else:
            viewable="[control]"
            class_field += " control"
    
        print "<div class=\"%s\" align=center><a href=\"%s\" >%s %s</a></div>" % (
            class_field,
            "../"+file.split('working_dir/')[-1],
            fileid, viewable );


    # Common Mutations -- print, but hide if only single file
    disp_style = "font-size:0.8em;text-align:center;"
    if len(files) == 1:
        disp_style = "font-size:0.8em;text-align:center;display:none;"

    print "<div class=\"data common\" style=\"%s\" >Common<br/>Mutations</div>" % disp_style

    print "</div>"
    print "</div>" # end floating header



printHeader()
printFloatingHeader(files)
print "<table id=\"genetable\">"
generateCodingTable( html_map, files )
generateIntergenicTable( intergenic_map, files )
print "</table>"
printFooter()
