#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <infolder> <outfolder>

Generates a summary HTML file from VCFs found in the infolder" && exit -1
    

#echo $0 $* >&2
#exit -1

infold=$1
outfold=$2

mkdir -p $outfold

case_vcfs=$(find $infold/ -type f -name "*.vcf" | grep -v "rejects" | grep "cases" )
cont_vcfs=$(find $infold/ -type f -name "*.vcf" | grep -v "rejects" | grep "controls" )

pedigree_fold=$working_folder/pedigrees/
[ -e $pedigree_fold ] && peds="--pedigree-folder=$pedigree_fold" || peds=""

#echo "$pedigree_fold"

# generate unique gene map (read: NOT common)
#genelist="$outfold/unique_genes.list"
#extract_common_gene_list.py $vcfs | sort | uniq > $genelist

# feed into HTML parser
html_doc="$outfold/summary.html"

# Mutation filter
mut_filter_opts=""
[ $opt_mutation_nonsense = Y ] && mut_filter_opts=$mut_filter_opts" NONSENSE"
[ $opt_mutation_missense = Y ] && mut_filter_opts=$mut_filter_opts" MISSENSE"
[ $opt_mutation_synonymous = Y ] && mut_filter_opts=$mut_filter_opts" SYNONYMOUS"
[ $opt_mutation_noncoding = Y ] && mut_filter_opts=$mut_filter_opts" NONCODING"

if [ "`echo $mut_filter_opts`" != "" ]; then
    mut_filter_opts="--mutation-filter="`echo $mut_filter_opts | sed 's/\s/+/g'`
else
    mut_filter_opts=""
fi

# Het/Hom filter
hethom_opts=""
if [ "$ih_model" = "AR" ] || [ "$ih_model" = "XR" ]; then
    hethom_opts="--zygo-filter=HETEROZY"
fi

logAndDo report_summary.py $case_vcfs $cont_vcfs $peds --exportable=`readlink -f $(dirname $0)` $mut_filter_opts $hethom_opts > $html_doc

#echo "`dirname $0`/gene_tables_HTML.py $dbmap $vcfs > $html_doc" >&2
#logAndDo `dirname $0`/gene_tables_HTML.py $dbmap $case_vcfs $cont_vcfs > $html_doc

# Move to tmp for webserver to pull from
#rm /tmp/*.html 2>/dev/null
#cp $html_doc /tmp/
#cp `dirname $0`/gene_tables.{css,js} /tmp/
#cp `dirname $0`/gene_tables.{css,js} $outfold # Also copy to summary folder, cos why not.

# Copy all to summary folder in working dir
#cp `dirname $0`/gene_tables.{css,js} $outfold/

