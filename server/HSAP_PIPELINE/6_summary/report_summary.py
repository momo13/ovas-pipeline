#!/usr/bin/env python2

import sys
from glob import glob as gl
import base64

if len(sys.argv)<2:
    print '''\n\t%s <[VCF files]> --pedigree-folder=path [--exportable]

Generates a report upon VCF files (with 'cases' and 'controls') in their pathnames.

--pedigree-folder=path       pedigree images to be (base64) encoded into the HTML
--exportable=path            bundles all style and javascript into the HTML (complete)
--mutation-filter=blah+blah  keywords to filter for in the MUL field -- MISSENSE,NONSENSE,SYNONYMOUS,NONCODING
--zygo-filter=blah+blah      keywords to filter for in the ZYG field -- HETEROZY,HOMOZY

''' % sys.argv[0].split('/')[-1]
    exit(-1)

def args():
    pedfold = ""
    exportable=False
    filters= []
    zygo = []
    files= []
    for arg in sys.argv[1:]:
        if arg.startswith("--"):
            if arg.startswith("--pedigree-folder="):
                pedfold = arg.split("--pedigree-folder=")[1]
            elif arg.startswith("--exportable="):
                exportable = arg.split("--exportable=")[1]
            elif arg.startswith("--mutation-filter="):
                filters = arg.split("--mutation-filter=")[1].split('+')
                if "NONCODING" in filters:
                    ind = filters.index("NONCODING")
                    filters[ind] = "*"

            elif arg.startswith("--zygo-filter="):
                zygo = arg.split("--zygo-filter=")[1].split('+')
            else:
                print >> sys.stderr, "Could not parse:", arg
                exit(-1)
        else:
            files.append( arg )

    return files, pedfold, exportable, filters, zygo


files, pedfold, exportable, filters, zygo = args()

chrom_map = {}
affect_map = {
    'cases' : [], 'controls' : []
}


def readInFiles(files):
    global chrom_map, filters, zygo

    for file in files:
        try:
            file.index("cases")
            isCase = True;
        except ValueError:
            isCase = False

        file_id = file.split('/')[-1].split('.')[0]

        # Sorts out ordering
        if isCase:affect_map['cases'].append(file_id)
        else:affect_map['controls'].append(file_id)

        with open(file,'r') as f:
            for line in f:
                if line[0]=='#':
                    continue

                # Data
                #CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  30232
                chrom, pos, rsid, ref, alt, qual, filt, info, form, sample = map(lambda x: x.strip(), line.splitlines()[0].split('\t'))
                pos = int(pos)

                if chrom not in chrom_map:
                    chrom_map[chrom] = {}

                if pos not in chrom_map[chrom]:
                    chrom_map[chrom][pos] = {
                        'rsid' : rsid,
                        'cases' : {},       # variant specific, not
                        'controls' : {},    # specific to the intersecting gene.
                        'genes' : {}
                    }

                

                form_split = form.split(':')
                samp_split = sample.split(':')

                #GT : AD:DP:   GQ:      PL:              AL:DIL:COL:PRL:MUL:CCH:PCH:ZYG
                #0/1:5,2: 7:55.87:56,0,140:PMM2|Exon8_3'UTR:  *:  *:  *:  *:  *:  *:HETEROZY
                index_dp  = form_split.index('DP')   # 25
                index_al  = form_split.index('AL')   # Gene|Blah
                index_dil = form_split.index('DIL')  # + -
                index_col = form_split.index('COL')  # TGG GTT
                index_prl = form_split.index('PRL')  # [Trp Sel]
                index_mul = form_split.index('MUL')  # MISS, NONS,
                index_cch = form_split.index('CCH')  # c.255G>A
                index_pch = form_split.index('PCH')  # p.W59S
                index_zyg = form_split.index('ZYG')  # Het | hom

                if zygo != []:
                    zy = samp_split[ index_zyg ].strip()
                    if zy not in zygo:
                        continue
                        
                
                # person specific                
                person_specific = {
                    'read_depth' : int( samp_split[ index_dp  ] ),
                    'zygosity'   : samp_split[ index_zyg ][0:3] == "HET"
                }

                if isCase:
                    chrom_map[chrom][pos]['cases'][file_id] = person_specific
                else:
                    chrom_map[chrom][pos]['controls'][file_id] = person_specific
                               

                # variant specific
                genes = samp_split[ index_al  ]
                col   = samp_split[ index_col ];
                prl   = samp_split[ index_prl ];            mul   = samp_split[ index_mul ]
                cch   = samp_split[ index_cch ];            pch   = samp_split[ index_pch ]

                list_of_genes = genes.split(',')
                list_of_col   = col.split(',')
                list_of_prl   = prl.split(',');            list_of_mul   = mul.split(',')
                list_of_cch   = cch.split(',');            list_of_pch   = pch.split(',')

                if len(set(
                        [ len(list_of_genes), len(list_of_col),
                          len(list_of_prl),   len(list_of_mul),
                          len(list_of_cch),   len(list_of_pch) ])) !=1:
                    print >> sys.stderr, "ER OR"
                    exit(-1)



                for x in range(len(list_of_genes)):
                    ge = list_of_genes[x]
                    co = list_of_col[x];
                    pr = list_of_prl[x];                mu = list_of_mul[x]
                    cc = list_of_cch[x];                pc = list_of_pch[x]

                    gene_with_iso, extra = ge.split('|')

                    if filters != []:
                        #import pdb
                        #pdb.set_trace()

                        # Skip any mutation if filters are up and the variant is not of the same type
                        if mu not in filters:
                            continue

                    mutation = {'pr' : pr, 'mu' : mu, 'cc' : cc, 'pc' : pc, 'co' : co}

                    detail = { 'promoterAmount' : None,  # num
                               'promoterType'   : None,  # up/dn
                               'utr'            : None,  # 5'/3'
                               'splice'         : None,  # A/D
                               'exon'           : None,  # 1,2,3 ( -1 if promoter or UTR)
                    }

                    if extra.startswith("Promoter"):
                        prom_detail = extra.split("Promoter_")[1].split("stream")[0]
                        detail['promoterAmount'] = int( prom_detail[:-2] )
                        detail['promoterType']   = prom_detail[-2:]

                        # Skip downstream promoters, should be handled by UTR
                        if detail['promoterType'] == "dn":
                            continue

                    elif extra.startswith("Exon"):
                        ex_detail = extra.split('_')
                        detail['exon'] = int( ex_detail[0].split('Exon')[1]  )

                        if len(ex_detail) > 1:
                            more_detail = ex_detail[1]

                            # Splice
                            if more_detail.startswith("Splice"):
                                detail['splice'] = more_detail.split("Splice")[1]

                            elif more_detail.endswith("UTR"):
                                detail['utr']  = int(more_detail.split("'UTR")[0])

                    # Exists already? Leave alone if exon
                    if gene_with_iso in chrom_map[chrom][pos]['genes']:
                        if chrom_map[chrom][pos]['genes'][gene_with_iso][1]['exon'] != None:
                            continue
                    
                    chrom_map[chrom][pos]['genes'][gene_with_iso] = ( mutation, detail )
                    

                    

                   

def printHead():
    global exportable
    stylecss = "style.css"
    listjs   = "list.min.js"
    
    print '''<html><meta http-equiv="content-type" content="text/html; charset=utf-8" /><head>'''

    if exportable == "":
        print '''<link rel="stylesheet" type="text/css" href="%s"><script src='%s'></script>''' % (stylecss, listjs)
    else:
        style_file=open(exportable + '/style.css','r'); stext = style_file.read(); style_file.close();
        list_file=open(exportable + '/list.min.js','r');ltext = list_file.read() ; list_file.close();

        print '''<style>%s</style><script>%s</script>''' % (stext,ltext)

    print "</head><body>"


def printTail():
    print '''
<!-- <div id='summary'>
    <span id='num_variants'></span> over <span id'num_genes' ></span>
</div> -->
</body></html>'''



def formatCaseControl( cases, controls, colspan ):
    
    global affect_map

    print "<td class='cases' %s >" % "" #colspan
    for case_id in affect_map['cases']:
        try:
            call = cases[case_id]['zygosity']
            zyg  = "zygo_HET" if call else "zygo_HOM" # ignore read_depth for now
        except KeyError:
            zyg = "zygo_NON"

        print "<span class='"+ zyg + "' >"+ case_id  +"</span>",
    print "</td>"

    if len(controls) > 0:
        print "<td class='controls' %s >" % "" #colspan
        for cont_id in affect_map['controls']:
            try:
                call = controls[cont_id]['zygosity']
                zyg = "zygo_HET" if call else "zygo_HOM" # ignore read_depth for now
            except KeyError:
                zyg = "zygo_NON"

            print "<span class='"+ zyg + "' >" + cont_id + "</span>",
        print "</td>"
        
    


    
def formatDataLine( row_chrom, row_pos, row_rsid,
                    cases, controls, tmp_array):

    num_isos = len(tmp_array)
    colspan = "" # "rowspan='" + str(num_isos) + "'"
    #not_printed = True
    #print "<tr>"
    #print "<td class='chrom' " + colspan +" >" + row_chrom + "</td>"
    #print "<td class='pos'   " + colspan +" >" + str(row_pos) + "</td>"
    #print "<td class='rsid'  " + colspan +" >" + row_rsid + "</td>"

    for row_gene,row_type,row_typeval,row_cdna, row_prot,row_mutetype,row_mutetype_more in tmp_array:

        print "<tr>"
        print "<td class='chrom' " + colspan +" >" + row_chrom + "</td>"
        print "<td class='pos'   " + colspan +" >" + str(row_pos) + "</td>"
        print "<td class='rsid'  " + colspan +" >" + row_rsid + "</td>"
        
        splitter = row_gene.split('-ISOF')
        row_gene = splitter[0]
    
        if len(splitter) > 1:
            gene = splitter[0]
            isono= int(splitter[1])
            row_gene = gene + "<div><span class='isonum'>ISO" + str(isono) + "</span></div>"
        else:
            row_gene += "<div><span class='isonum ref'>REF</span></div>"
       
        print "<td class='gene'>" + row_gene + "</td>"
        print "<td class='mtype contparent'>" + row_type + "<div class='mtypeval contextable'><div>" + row_typeval + "</div></div></td>"
        print "<td class='changecDNA contparent'>" + row_cdna + "<div class='changeProt contextable'><div>" + row_prot + "</div></div></td>"
        print "<td class='mutetype contparent'>" + row_mutetype + "<div class='mutetype_more contextable'><div>" + row_mutetype_more + "</div></div></td>"

        #if not_printed:
        #not_printed = False
        formatCaseControl( cases, controls, colspan )


        print "</tr>"



def renderPedigree():
    global pedfold

    if pedfold == "":
        return ""

    peds = gl(pedfold+"*.png");

    if len(peds)==0:
        print >> sys.stderr, "No peds found at", pedfold
        return ""

    out  = "<div id='pedigreehover' > Show Pedigrees </div>"
    out += "<div id='pedigrees'><div>"
    for ped in peds:
        with open(ped,'rb') as img:
            encod = base64.b64encode(img.read())
            out += '''<img src="data:image/png;base64,%s" />''' % encod
    out += "</div></div>"
    return out
    
def printTable():
    global chrom_map
    global affect_map

    print "<div id='my-table-of-fun'>"
    print '''<table id="genetable">'''
    print '''<thead>'''
    print '''<tr><td colspan=7><input class="search" placeholder="Search" /></td><td colspan=2 >%s</td></tr>''' % renderPedigree()
    print '''<tr>
<th class='sort' data-sort='chrom' >Chrom ||||</th>
<th class='sort' data-sort='pos'   >Position ||||</th>
<th class='sort' data-sort='rsid'  >Name ||||</th>
<th class='sort' data-sort='gene'  >Gene ||||</th>
<th class='sort' data-sort='mtype' >Context ||||</th>
<th class='sort' data-sort='changecDNA' >CDNA ||||</th>
<th class='sort' data-sort='mutetype' >Functional ||||</th>
<th class='sort' data-sort='cases' >Cases</th>
[CONTROLS]
</tr>
</thead>'''.replace('[CONTROLS]', "<th class='sort'  data-sort='controls' >Controls</th>" if len(affect_map['controls'])>0 else "").replace('||||', '''<span class='arrowup'>&#8593;</span><span class='arrowdn'>&#8595;</span><span class='arrowbth' >&#8645;</span>''')

    print "<tbody class='list'>"

    chrom_order = sorted(chrom_map.keys())
    for chrom in chrom_order:
        pos_order = sorted(chrom_map[chrom].keys())

        for pos in pos_order:

            rsid    = chrom_map[chrom][pos]['rsid']
            cases   = chrom_map[chrom][pos]['cases']
            controls= chrom_map[chrom][pos]['controls']

            # If all controls have the mutation, then don't print it
            num_controls = len(controls)

            if num_controls > 0 and (len(controls) == len(affect_map['controls'])):
                continue;

            row_chrom = chrom;
            row_pos = pos;
            row_rsid = rsid;

            num_isos = len(chrom_map[chrom][pos]['genes'].keys())

            tmp_array = []

            for gene in chrom_map[chrom][pos]['genes']:

                mutation, detail = chrom_map[chrom][pos]['genes'][gene]
                # Needs to be gene pos, rather than pos gene because isoforms mean the same position
                # can be exonic / intronic depending on context

                # 1    2          3      4       5           6             7                             
                # chr  gene       pos    type    typeval     code_change   mute_type (hver:-> mute_info)  
                # chr1 REST-ISOF1 12345  Exon    1           [c.234 p.89]  missense  [ttg->gtt,Rhe->Tre]
                # chr2 REST       12345  Splice  1Accept      -            -
                # chr1 REST       12345  UTR     5'           -            -
                # chr1 REST       12345  Prom    up 500       -            -

                row_gene = gene;
                row_type = ""  ;      row_typeval = "";
                row_changeCDNA = "";  row_changeProt = "";
                row_mutetype   = "" ; row_mutetype_more = "";
                                               
                if detail['exon'] != None:
                    exn_no = detail['exon']

                    if detail['splice'] != None:
                        # Splice
                        don_acc = "Acceptor" if detail['splice']=='A' else "Donor"

                        row_type = "Splice"
                        row_typeval = don_acc + " (Exon"+str(exn_no)+")"
                        row_changeCDNA = mutation['cc']

                    elif detail['utr'] != None:
                        # UTR
                        num = str(detail['utr'])

                        row_type = "UTR"
                        row_typeval = num+"'UTR"
                    else:
                        # Coding
                        col_pr = mutation['co'].split('[')[1].split(']')[0].replace(' ','&rarr;') + "<br/>" + mutation['pr'].replace(' ','&rarr;')
                        # [TTG GTT] [Rhe Tre]
                        mu_typ = mutation['mu']                         # missense

                        row_type = "Exonic"
                        row_typeval = "Exon" + str(exn_no)
                        row_changeCDNA = mutation['cc']
                        row_changeProt = mutation['pc']
                        row_mutetype = mu_typ.lower()
                        row_mutetype_more = col_pr

                elif detail['promoterType']!=None:
                    #prom_type = ("Upstream (" if detail['promoterType'] == "up" else "Downstream (") + str(detail['promoterAmount']) + 'bp)'
                    prom_type = "(" + str(detail['promoterAmount']) + "bp)"  # upstream proms only

                    row_type = "Promoter"
                    row_typeval = prom_type
                    row_changeCDNA = mutation['cc']

                    camount = row_changeCDNA.split("c.")[1]
                    nums = ""
                    for con in range(len(camount)):
                        c = camount[con]
                        
                        if con == 0 and c == "-":
                            nums += c
                            continue

                        try:
                            int(c)
                            nums += c
                        except ValueError:
                            break

                    try:
                        nums = int(nums)
                    except ValueError:
                        import pdb;pdb.set_trace()
                    nums = nums if nums > 0 else -nums

                    diff = int(detail['promoterAmount'])
                    diff = diff if diff > 0 else -diff

                    if nums > diff:
                        continue
                    
                else:
                    print >> sys.stderr, "SOMETHING ELSE"
                    

                tmp_array.append( (row_gene, row_type, row_typeval,
                                   row_changeCDNA, row_changeProt,
                                   row_mutetype, row_mutetype_more)  )
                    
            formatDataLine( row_chrom, row_pos, row_rsid,
                            cases, controls,
                            tmp_array )

            #if pos == 9030444:
            #    import pdb
            #    pdb.set_trace()

            
    
    print "</tbody>"
    print "</table>"
    print "</div>"

    global exportable
    js_filename = "sortable.js"
    if exportable == "":
        print "<script src='%s'></script>" % js_filename
    else:
        js_file=open(exportable+"/"+js_filename,'r');jstext = js_file.read() ; js_file.close()
        print "<script>%s</script>" % jstext
        

    
                        
def printHTML():
    printHead()
    printTable()
    printTail()



                        
readInFiles( files )
printHTML()
