#!/usr/bin/env python2
import sys

def usage ():
	print >> sys.stderr, 'usage: %s <vcf_file>' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this script filters through variants by looking at the novelty of each variant. It keeps these variants that do not have a "rs" identifier'
	exit ()

if len (sys.argv) != 2:
        usage()

vcf = open (sys.argv [1], 'r')
#vcf_output = open (sys.argv [1]+ '.novelty-filter', 'w')

for line in vcf:
	new_line = line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		ID_card = guide_cards.index ('ID')

	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		ID = talkens [ID_card]

		if ID == '.':
			print line
