#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs novel filtering over all files" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Novel filter " green

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".novel.vcf"

#    currFileProgress $vcf
    novel_variant.py $vcf > $new_name
    finishProgress $new_name
done
