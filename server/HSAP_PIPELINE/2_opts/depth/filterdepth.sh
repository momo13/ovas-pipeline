#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs depth filtering over all files" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Depth filter $opt_read_depth_gt " green

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".depth_gt"$opt_read_depth_gt".vcf"

#    currFileProgress $vcf
    read_depth.py $vcf $opt_read_depth_gt > $new_name
    finishProgress $new_name $vcf
done

echo ""
