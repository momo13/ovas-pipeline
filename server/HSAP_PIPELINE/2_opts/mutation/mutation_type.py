#!/usr/bin/env python2
import sys

def usage ():
	print >> sys.stderr, 'usage: %s <vcf_file> [keywords]' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, '\nthis script filters trough variants by looking at the mutation type annotated by funcannot module. The user is able to choose mutation types\nIf "noncoding" is given it is changed to "*"'
	exit ()

if len (sys.argv) < 3:
	usage()

vcf = open (sys.argv [1], 'r')
#vcf_output = open (sys.argv [1]+ '.mut-type-filter', 'w')

key_words = sys.argv[2:]

# If 'all' is given, change it
try:
        noncode = key_words.index('noncoding')
        key_words[noncode] = '*'
        
except ValueError:
        pass;


for line in vcf:
	new_line = line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		format_card = guide_cards.index ('FORMAT')
		for item in guide_cards:
			if item.isdigit():
				annotation_card=guide_cards.index (item)

	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		format_line = talkens [format_card]
		format_line_splitted = format_line.split (':')
		mutation_type_card = format_line_splitted.index ('MUL')

		annotation_line = talkens[annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		mutation_type = annotation_line_splitted [mutation_type_card]

		for key_word in key_words:
			if mutation_type.find(key_word) != -1:
				print line
