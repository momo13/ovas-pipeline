#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs mutation type filtering over all files" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)


args=""
[ "$opt_mutation_missense"   = "Y" ] && args=$args" MISSENSE"
[ "$opt_mutation_nonsense"   = "Y" ] && args=$args" NONSENSE"
[ "$opt_mutation_synonymous" = "Y" ] && args=$args" SYNONYMOUS"
[ "$opt_mutation_noncoding"  = "Y" ] && args=$args" noncoding"

# convert to lower
small_args=`echo "$args" | tr '[:upper:]' '[:lower:]'`

printline " Mutation filter: $small_args " green

file_ext=`echo "$small_args" | sed 's/\s/_/g' | sed 's|*|all|g'`

#echo $file_ext
#exit

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".mut_"$file_ext".vcf"

#    currFileProgress $vcf
    logAndDo mutation_type.py $vcf $args > $new_name
    finishProgress $new_name $vcf

done

echo ""
