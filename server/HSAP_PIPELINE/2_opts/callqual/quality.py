#!/usr/bin/env python2
import sys

def usage ():
	print >> sys.stderr, 'usage: %s <vcf_file> <threshold>\n' % sys.argv[0].split('/')[-1]
	print >> sys.stderr, 'this script filters through variant quality score (GQ). The user is able to choose the minimum quality score'
	exit(-1)

if len (sys.argv) != 3:
	print usage()

vcf = open (sys.argv [1], 'r')
#vcf_output = open (sys.argv [1]+ '.quality-filter', 'w')
treshold = float(sys.argv [2])

for line in vcf:
	new_line = line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		format_card = guide_cards.index ('FORMAT')
		for item in guide_cards:
			if item.isdigit():
				annotation_card=guide_cards.index (item)

	new_line=line.strip()
	if new_line.startswith ('#'):
		print new_line

	if new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		format_line = talkens [format_card]
		format_line_splitted = format_line.split (':')
		quality_card = format_line_splitted.index ('GQ')

		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		quality = float(annotation_line_splitted [quality_card])

		if quality >= treshold:
			print line
