#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs call quality filtering over all files" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Call Quality filter $opt_call_qual_gt " green

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".callqual_gt"$opt_call_qual_gt".vcf"

    currFileProgress $vcf
    quality.py $vcf $opt_call_qual_gt > $new_name
    finishProgress $new_name $vcf
done

echo ""
