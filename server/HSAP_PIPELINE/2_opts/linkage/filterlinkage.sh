#!/bin/bash

[ $# != 3 ] && echo "`basename $0` <input folder> <output folder> <region file>

Performs linkage over all file" && exit -1

infold=$1
outfold=$2
regfile=$3

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " Linkage Filter " green

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`
    new_name=$new_file_prefix".linkage.vcf"

#    currFileProgress $vcf
    logAndDo chrome_comb.py $vcf $regfile > $new_name
    finishProgress $new_name
done
