#!/usr/bin/env python2
import sys

def usage ():
	print >> sys.stderr, 'usage: %s <vcf_file> <percentage>' % sys.argv[0]
	print >> sys.stderr, '\n\tthis script filters through AAF. The user is able to choose the minimum allele frequency percentage'
	exit(-1)

if len (sys.argv) != 3:
	usage()

vcf = open (sys.argv [1], 'r')
threshold = float(sys.argv [2]) / 100

for line in vcf:
	new_line = line.strip()
	if new_line.startswith ('#CHROM'):
		guide_cards = new_line.split ('\t')
		format_card = guide_cards.index ('FORMAT')
		for item in guide_cards:
			if item.isdigit():
				annotation_card=guide_cards.index (item)

	new_line=line.strip()

	if new_line.startswith ('#'):
		print new_line

	elif new_line.startswith ('chr'):
		line=new_line.splitlines ()[0]
		talkens = new_line.split ('\t')
		format_line = talkens [format_card]
		format_line_splitted = format_line.split (':')
		AAF_card= format_line_splitted.index ('AAF')

		annotation_line = talkens [annotation_card]
		annotation_line_splitted = annotation_line.split (':')
		AAF = float(annotation_line_splitted [AAF_card])

		if AAF <= threshold:
			print line

