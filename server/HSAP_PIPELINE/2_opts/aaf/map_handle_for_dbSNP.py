#!/usr/bin/env python2
import sys
import os
import cPickle as pickle
import gc

from glob import glob

from time import time

class chromN_dbSNPhandler:

	def __init__ (self, chromN_dbSNP_file):
		
		self.file_name = chromN_dbSNP_file
		self.chromN_dbSNPmap = {}		
		self.making_db_map ()



	def making_db_map (self):
		
		f = open (self.file_name, "r")
		count=0
		start_time = time() 						# start stopwatch

                rs_ID_card = -1
                allele_card = -1
                frequency_card = -1

                header_found = False
		
		for line in f:
			line=line.strip()
			if not header_found:
                                if line.index('chrom')!=-1:
				        guide_cards = line.split ('\t')
				        rs_ID_card = guide_cards.index ('name')
				        allele_card = guide_cards.index ('alleles')
				        frequency_card = guide_cards.index ('alleleFreqs')
                                        # Fail with indexing error
                                        header_found = True

				        continue

                                if rs_ID_card == 1:
                                        print >> sys.stderr, "Could not find header for %s" % self.file_name
                                        exit(-1)

			talkens = line.split ('\t')
			rs_ID = talkens [rs_ID_card]
                     
                        try:
                                allele_line = talkens [allele_card]

			        alleles = allele_line.split (',')
			        alt_allele = alleles [0]
			        ref_allele = alleles [1]

                           	frequency_line = talkens [frequency_card]
			        frequency = frequency_line.split (',')
			        alt_freq = float(frequency [0])
			        ref_freq = frequency [1]

                        except IndexError:
#                                print >> sys.stderr, "Nope"
 #                               exit(-1)
                                alt_freq=0.0

                        if rs_ID not in self.chromN_dbSNPmap:
				self.chromN_dbSNPmap [rs_ID] = alt_freq
#                               print >> sys.stderr, self.chromN_dbSNPmap



                                
			count+=1
			if (count%5999==0):print >> sys.stderr, "\r\t%d" % count,
			
		time_diff = time() - start_time				# end stopwatch
		print >> sys.stderr, "[", int(time_diff),"s]",



class dbSNP_handler:

	def __init__ (self, folder_name, dbsnp_ver, opt_chrom = []):
		self.folder_name = glob( "%s/%s*/" % (folder_name, dbsnp_ver) )[0]
                self.dbsnp = dbsnp_ver
		
		self.db_map = {}			# holds data for a single chromosome. 
		# When the vcf file reaches a new line with a new chromosome, this
		# gets replaced with a new db_map from a pickle file on the disk
		# (or generated if one doesn't yet exist)
		
		if opt_chrom == []:
			opt_chrom = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,"X","Y","MT"]
			
		self.chroms = opt_chrom
		

		# Create a folder to store pickle files
		self.pickle_folder = self.folder_name + "/pickles/"

		if not os.path.isdir(self.pickle_folder):
			print >> sys.stderr, "Pickle folder not found, creating new one in", self.pickle_folder
			os.mkdir(self.pickle_folder)
		
		

	def process_chrom (self, chrom_no):
		print >> sys.stderr, "Chr", chrom_no,
		
		if os.path.isfile(self.pickle_folder + "/" + str(chrom_no) + '.pkl'):
			self.load_chrom (chrom_no)
			
		else:
			chrom_file = glob( "%s/*chr%s" % (self.folder_name, str(chrom_no)))[0]
			c = chromN_dbSNPhandler (chrom_file)
			self.save_obj (c.chromN_dbSNPmap, str(chrom_no))
			self.db_map = c.chromN_dbSNPmap



	def save_obj(self, obj, name ):
		
		start_time = time()							# start stopwatch
		
		print >> sys.stderr, ":saving map in memory -> pickle file for later reuse:",
		
		with open(self.pickle_folder + "/" + str(name) + '.pkl', 'wb') as f:
                        print >> sys.stderr, " size = ", len(obj)
			pickle.dump(obj, f)
			
			time_diff = time() - start_time			# end stopwatch
			print >> sys.stderr, "[", int(time_diff),"s]"


	def load_obj(self, name ):
		
		start_time = time() 						# start stopwatch
		
		print >> sys.stderr, ":loading pickle from file -> map in memory:",
		
		with open(self.pickle_folder + "/" + str(name) + '.pkl', 'rb') as f:
			loaded = pickle.load(f)
			
			time_diff = time() - start_time			# end stopwatch
			print >> sys.stderr, "[", int(time_diff),"s]"
			
			return loaded



	def load_chrom(self, chrom_wanted):
		del self.db_map
		#gc.collect()
		
		try:
			# This is where it crashes
			self.db_map = self.load_obj(chrom_wanted)
		
			# It fails when the pickle file is bad (probably because it
			# was terminated early when it was still writing.)
		except EOFError,ValueError:
			print >> sys.stderr, "\nPickle file is corrupt! Regenerating..."
			
			# delete corrupt pickle
			os.remove( self.pickle_folder + "/" + str(chrom_wanted) + ".pkl" );
			
			# Reprocess chrom
			self.process_chrom( chrom_wanted )

