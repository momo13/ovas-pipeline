#!/usr/bin/env python2

from map_handle_for_dbSNP import chromN_dbSNPhandler, dbSNP_handler
import sys

class vcf_handler:

	def __init__ (self, vcf_file, superClassMap):
		self.file_name = vcf_file
		self.db_handler = superClassMap
		self.freq_map ={}
		self.process_vcf ()

	def process_vcf (self):
		last_line =""
		last_chrom = ""
		format_list =[]
		f=open (self.file_name, 'r')
		o=open (self.file_name + '.output', 'w')
		for line in f:
			line=line.splitlines () [0]
			if line.startswith ('#'):
				if line.startswith ('##') and not line.startswith ('##FORMAT'):
					print >> o,line
					continue

				if line.startswith ('##FORMAT'):
					format_list.append (line)
				else:
					if len (format_list)!=0:
						if last_line.startswith ('##FORMAT'):
							new_list_of_formats = self.handleFORMATlines (format_list)
							self.printFORMATlines(new_list_of_formats,o)
							format_list =[]
						else:
							print >> o,line
				last_line = line

				if line.startswith ('#CHROM'):
					print >> o,line
					guide_vcf = line.split ('\t')
					chr_vcf = guide_vcf.index ('#CHROM')
					name_vcf = guide_vcf.index ('ID')
					alt_vcf = guide_vcf.index ('ALT')
					format_vcf = guide_vcf.index ('FORMAT')
					for item in guide_vcf:
						if item.isdigit():
							annotation_vcf=guide_vcf.index (item)
				continue

			talkens = line.split ('\t')
			chrom = talkens [chr_vcf]
			ID = talkens [name_vcf]
			ALT = talkens [alt_vcf]
			format_line = talkens [format_vcf]
			format_line_splitted = format_line.split (":")
			annotation_line = talkens [annotation_vcf]
			annotation_line_items = annotation_line.split (":")
			if 'AAF' not in format_line_splitted:
				format_line_splitted.append ('AAF')
				new_format_line = ":".join (format_line_splitted)
				talkens [format_vcf] = new_format_line
				line = "\t".join (talkens)
			else:
				continue
				
				
			if chrom != last_chrom:
				self.db_handler.process_chrom (chrom [3:])

			freq_map = self.db_handler.db_map

                        ALT_freq = "0.000000"
                        if ID in freq_map:
                                ALT_freq = "%.6f" % freq_map [ID]

                        annotation_line_items.append (ALT_freq)
                        new_annotation_line = ":".join (annotation_line_items)
                        talkens [annotation_vcf] = new_annotation_line
                        line = '\t'.join (talkens)
                        print >> o, line                               
                                
                        last_chrom = chrom

		f.close()
		o.close()

	def handleFORMATlines(self,my_array):
		my_list_of_formats = my_array
		for format_line in my_list_of_formats:
			if '##FORMAT=<ID=AAF, Number=.,Type=String,Description="Allele frequency for alternative allele (ALT) is asigned">' in format_line:
				exit (-1)
		my_list_of_formats.append ('##FORMAT=<ID=ISO, Number=.,Type=String,Description="Allele frequency for alternative allele (ALT) is asigned">')
		return my_list_of_formats

	def printFORMATlines(self,array,output_file):
		for element in array:
			print >> output_file, element

