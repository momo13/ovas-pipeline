#!/usr/bin/env python2

from vcf_handler import *

if len(sys.argv)<1:
    print >> sys.stderr, "%s <[VCFs]>\n Appends allele frequency data to multiple VCF files" % sys.argv[0].split('/')[-1]

vcf_files = sys.argv[1:]
map_data = "/extra/static_data/dbSNPs/"
db_snp   = 'dbSNP142'  # dbSNP142

d = dbSNP_handler ( map_data , db_snp)

for vcf in vcf_files:
    print >> sys.stderr, vcf
    v = vcf_handler( vcf, d )
    del v
