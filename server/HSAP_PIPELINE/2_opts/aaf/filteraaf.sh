#!/bin/bash

[ $# != 2 ] && echo "`basename $0` <input folder> <output folder> 

Performs alternate allele filterin over all files" && exit -1

infold=$1
outfold=$2

mkdir -p $outfold

vcfs=$(grabCaseVCFs $infold)

printline " AAF filter < $opt_aaf_lt_perc " green

# Produces ".output" files in the input folder
# 
logAndDo append_aaf.py $vcfs

for vcf in $vcfs;do

    new_dir=$(setNewDir $vcf $infold $outfold)
    new_file_prefix=$new_dir/`basename $vcf .vcf`

    temp_name=$vcf".output"

    out_name=$new_file_prefix".aaf_"$opt_aaf_lt_perc".vcf"
    move_name=$(addPrefix $out_name "temp_")

    mv $temp_name $move_name

    # Performing filtering upon move name to out_name
    logAndDo filter_aaf.py $move_name $opt_aaf_lt_perc > $out_name   

    finishProgress $out_name $vcf
done
