#!/bin/bash

working_dir=../runs/working_dir

echo "remove soft files from $working_dir?"
read ans

[ "$ans" = "y" ] && rm -rf $working_dir/{02*,03*,04*,05*,06*,07*,final,last_processed,pedigrees,summary} && echo "done"

[ "$1" != "" ] && echo "remove core too?" && read hans && [ "$ans" = "y" ] && rm -rf $working_dir/01*
